<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 't2m');

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '6(-*A;>}V1ow}<gRq;@gG~YxsA<5Hod=4u@cCKNGsgXcSud(`/ Yz!-NC/6?%qu2' );
define( 'SECURE_AUTH_KEY',   '+gq9*e+-7_>ZF#8{?BLnOMDp2|AeY{k_#*qZo$6|DOf]fqkzQ;]qY0*:*EFLb*S^' );
define( 'LOGGED_IN_KEY',     'M7h>x9]~J+-ul&q$?kC!qank4D%Eg].o)0h|,4`Nbr4J[z/S!z3fkBcL#}Rvr/ED' );
define( 'NONCE_KEY',         'fU#r5 sjM`gBQ(PZY*0j4;>l~o5zKgb$YiWiN/.r&DA%%u{2Pp0S~o8=Qp+_g[AO' );
define( 'AUTH_SALT',         '0 U;Z_@u-/Ir}j_Sv>;Ud>?=v:{H<<06kC4y9J_0vy33w[^{J5T)#E*h#14AgQ;(' );
define( 'SECURE_AUTH_SALT',  'dms(6#gcZ](2k2#(vI<0a:|qe25zrcTtc h;0x1_4li&AIeIl5)J9${J,83^iNNH' );
define( 'LOGGED_IN_SALT',    '4,,Hf$<c,<6U*&t?ZtF/Ld:>T`Z`=`>c11BMALM9ie&Vv,P/?azkHHl|hUSr$CXR' );
define( 'NONCE_SALT',        'X)&acjxYR>5G*=*41?=*<LH2Z&x7J?R^DztXfMka!`m0[$#Ii`EUeA=:bzT51?yZ' );
define( 'WP_CACHE_KEY_SALT', 'F-Y:4wc,8pDai@!.W)kr;8xnBsCB/2wuA&5#;$ 7 mE28[N-$d9w05GawmQ[! :C' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', false );
define( 'WP_AUTO_UPDATE_CORE', false );
/** Memory Limit */
	define('WP_MEMORY_LIMIT', '256M');
	define( 'WP_MAX_MEMORY_LIMIT', '256M' );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
