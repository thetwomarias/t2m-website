<?php
/**
 * Footer template
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */
?>

</main>
<footer class="footer">
<div class="footer__container">
    <div class="footer__wrapper">
      <div class="footer__logo">
      	<?php
			$linkHome = apply_filters( 'wpml_home_url', get_option( 'home' ) );
		?>
		<a href="<?php echo $linkHome; ?>" class="logo__link"></a>
        <img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-white.svg" alt="The Two Marias" title="The Two Marias" />
      </div>
      <div class="footer__content">
        <div class="footer__col">
          <div class="footer__row">
            <p class="footer__title"><?php _e( 'Contact', 't2m'); ?></p>
            <p class="footer__subtitle"><a href="mailto:<?php echo the_field('email', 'option')?>" class="footer-link--cta"><span><?php echo the_field('email', 'option')?></span></a></p>
            <!-- <p class="footer__subtitle"><?php echo the_field('telephone', 'option')?></p> -->
          </div>
          <div class="footer__row">
            <p class="footer__title"><?php _e( 'Follow us', 't2m'); ?></p>
            <div class="footer__subtitle">
            	<ul class="social__list">
					<li class="social__item">
						<a class="social__link" href="<?php echo the_field('linkedin', 'option')?>" target="_blank"><span class="hide-sm show-md ">Lk</span><span class="show-sm ">Lk</span></a>
					</li>
					<li class="social__item">
						<a class="social__link" href="<?php echo the_field('instagram', 'option')?>" target="_blank"><span class="hide-sm show-md ">Ig</span><span class="show-sm">Ig</span></a>
					</li>
					<li class="social__item">
						<a class="social__link" href="<?php echo the_field('facebook', 'option')?>" target="_blank"><span class="hide-sm show-md ">Fb</span><span class="show-sm ">Fb</span></a>
					</li>
					<li class="social__item">
						<a class="social__link" href="<?php echo the_field('vimeo', 'option')?>" target="_blank"><span class="hide-sm show-md ">Vi</span><span class="show-sm ">Vi</span></a>
					</li>
		        </ul>
            </div>
          </div>
          <div class="footer__row footer__years">
          	<p class="footer__subtitle">
          		<?php
          			$years_image = get_field('years_image', 'option');
          			$years_image_w = $years_image['width'];
          			$years_image_h = $years_image['height'];
          			$years_image_url = $years_image['url'];

          			$years_text = get_field('years_text', 'option');
          			$years_link = get_field('years_link', 'option');
          			if(isset($years_image_url))
          			{
          				?>
          					<span class="years-img" >
          						<?php
          						if(!empty($years_link) && isset($years_link['url']) && $years_link['url']!='' )
          						{
          							?>
          								<a href="<?php echo $years_link['url']; ?>" target="_blank">
          							<?php

          						}
          						?>
          						<img src="<?php echo $years_image_url; ?>" alt="<?php echo $years_text; ?>" width="87" height="58" />
          						<?php
          						if(!empty($years_link) && isset($years_link['url']) && $years_link['url']!='' )
          						{
          							?>
          								</a>
          							<?php

          						}
          						?>
          					</span>
          				<?php
          			}
          		?>
          		<span class="years-text">
          			<?php
					if(!empty($years_link) && isset($years_link['url']) && $years_link['url']!='' )
					{
						?>
							<a href="<?php echo $years_link['url']; ?>" target="_blank">
						<?php

					}
					?>
          			<?php echo $years_text; ?>
          			<?php
					if(!empty($years_link) && isset($years_link['url']) && $years_link['url']!='' )
					{
						?>
							</a>
						<?php

					}
					?>
          		</span>
          	</p>
          </div>
        </div>
        <div class="footer__col footer__sitemap">
        	<?php
          		$legal_links = get_field('legal_links', 'option');
          	?>
            <p class="footer__title"><?php _e( 'RELEVANT LINKS', 't2m'); ?></p>
            <?php

            if(!empty($legal_links))
            {
            	?>
	            <div class="footer__subtitle">
		        	<ul class="social__list">
		            	<?php
		            	foreach ($legal_links as $link) {
		            		if(isset($link['link']) && isset($link['label']))
		            		{
		            			?>
									<li class="social__item">
										<a class="footer-link--cta" href="<?php echo $link['link']['url']; ?>" target="<?php echo $link['link']['target']; ?>" ><span><?php echo $link['label']; ?></span></a>
									</li>
		            			<?php
		            		}
		            	}
		            	?>
					</ul>
		        </div>
		       <?php
		   }
		?>
        </div>
        <div class="footer__col footer__women">
        	<?php
          		$women_owned = get_field('women_owned_text', 'option');
          		$women_owned_link = get_field('women_owned_link', 'option');
          		$women_owned_image = get_field('women_owned_image', 'option');
          		if(isset($women_owned_image))
      			{
      				?>
			            <div class="footer__subtitle">
			            	<?php echo $women_owned; ?>
			            </div>
			        <?php
      			}
      			if(isset($women_owned_image))
      			{
	      			if(isset($women_owned_link))
	      			{
	      				?><a href="<?php echo $women_owned_link; ?>" target="_blank"><?php
	      			}

      				?>
      					<span class="years-img" ><img src="<?php echo $women_owned_image['url']; ?>" alt="<?php echo $women_owned; ?>" /></span>
      				<?php

      				if(isset($women_owned_link))
	      			{
	      				?></a><?php
	      			}
      			}
          	?>
        </div>
        <?php
	        /*
	        <div class="footer__col footer__newsletter">
				<div class="footer__row">
					<p class="footer__title"><?php _e( 'Sign up to our T2M newsletter', 't2m'); ?></p>
					<p class="footer__subtitle"><?php _e( 'We’ll only send it when we have something interesting to tell you, promise.', 't2m'); ?></p>
					<div class="footer__form">
						<!-- Begin Mailchimp Signup Form -->
						<div id="mc_embed_signup">
							<form action="https://thetwomarias.us6.list-manage.com/subscribe/<?php if ( ICL_LANGUAGE_CODE=='en' ) : ?>post?u=c7c4d8c89541827f53d49532c&amp;id=103c5c5794<?php endif ?><?php if ( ICL_LANGUAGE_CODE=='ca' ) : ?>post?u=c7c4d8c89541827f53d49532c&amp;id=32a011fc51<?php endif ?><?php if ( ICL_LANGUAGE_CODE=='es' ) : ?>post?u=c7c4d8c89541827f53d49532c&amp;id=e33f4d5975<?php endif ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"  target="_blank">
								<div id="mc_embed_signup_scroll">
									<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" autocomplete="off" placeholder="<?php _e('Your email address','t2m');?>" required data-msg="<?php _e('Are you sure you typed in the email correctly?','t2m');?>">
									<div class="agreement">
										<input type="checkbox" value="1" name="subscribe" class="checkbox" required  data-msg="<?php _e('Guess you forgot to check the box?','t2m');?>">
										<label><?php _e('I agree to the terms in the','t2m');?> <a href="<?php if ( ICL_LANGUAGE_CODE=='es' ) : ?>/es<?php endif ?><?php if ( ICL_LANGUAGE_CODE=='ca' ) : ?>/ca<?php endif ?>/legal" class="footer-link--cta"><span><?php _e('Privacy Policy','t2m');?></span></a>, <?php _e('and that you will only use my data to send me the newsletter','t2m');?>.</label>
										<span class="error error-text"></span>
									</div>
									<?php if ( ICL_LANGUAGE_CODE=='en' ) : ?>
									<input id="group_1" style="display: none;" checked="checked" name="group[20421][1]" type="checkbox" value="1" />
									<?php endif ?>
									<?php if ( ICL_LANGUAGE_CODE=='ca' ) : ?>
									<input id="group_2" style="display: none;" checked="checked" name="group[20421][2]" type="checkbox" value="1" />
									<?php endif ?>
									<?php if ( ICL_LANGUAGE_CODE=='es' ) : ?>
									<input id="group_3" style="display: none;" checked="checked" name="group[20421][3]" type="checkbox" value="1" />
									<?php endif ?>
									<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c7c4d8c89541827f53d49532c_103c5c5794" tabindex="-1" value=""></div>
									<div class="clear"><i class="link--cta"><span><?php _e('Send','t2m');?></span><input type="submit" value="" name="Send" id="mc-embedded-subscribe" class="button"/></i></div>
								</div>
							</form>
						</div>
						<div class="footer__send">
							<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/send.svg" alt="Send" title="Send"/>
						</div>
						<!--End mc_embed_signup-->
					</div>
				</div>
			</div>
			*/
		?>
      </div>
    </div>
    <div class="scroll-btn scroll-up"><span><?php _e( 'Scroll up', 't2m'); ?></span></div>

  </div>
  <div class="copyright">
    <div class="copyright__text background--blue"><?php echo the_field('copyright', 'option')?></div>
    <div class="copyright__space"></div>
  </div>

  </div>
</footer>

<?php require_once get_stylesheet_directory().'/templates/cookie-message.php'; ?>

<?php wp_footer(); ?>
<script>
   var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
    // ... more custom settings?
    });

   var domainURL = "<?php echo get_site_url(); ?>";
</script>
</body>
</html>
