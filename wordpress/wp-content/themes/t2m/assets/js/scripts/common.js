const common = (($) => {
    'use strict';

    // Global variables
    var c, currentScrollTop = 0;
    var navbar = $('header');
    var navbarSticky = $('.nav-sticky');
    var scrollTop = $(window).scrollTop();

    const homeSlider = $('.home-projects__slider');
    const sliderMariismos = $('.home-mariismos__slider');
    const sliderProjects = $('.project__slider');
    const sliderTestimonials = $('.testimonials__slider');
    const sliderCaseStudies = $(".case-study__slider");

    const languageMenu = $('.wpml-ls-current-language')
    const languageMenuLink = $('.wpml-ls-current-language > a')
    const languageSubmenu = $('.wpml-ls-current-language .sub-menu');


    const someFunction = () => {

    // Adding touch class for devices

    if ("ontouchstart" in window || "ontouch" in window) {
        $('body').addClass('touch');
    }

    // Language Switcher

    if ($(window).width() > 768) {
        languageMenuLink.click(function(e){
            e.preventDefault();
            languageMenu.toggleClass('opened');
            languageMenuLink.toggleClass('rotated');
            languageSubmenu.slideToggle();

        });
    }



    // Bodymovin logo integration

    lottie.loadAnimation({
        container: document.getElementById('bm'), // the dom element that will contain the animation
        renderer: 'svg',
        loop: false,
        autoplay: true,
        path: '/wp-content/themes/t2m/assets/js/vendors/bodymovin/data.json' // the path to the animation json  // Pre Logo 15Y 2023 (base)
        // path: '/wp-content/themes/t2m/assets/js/vendors/bodymovin/T2M15.json' // Logo 15Y 2023
    });

    homeSlider.slick({
        autoplay: false,
        speed: 800,
        centerMode: true,
        centerPadding: '10%',
        fade: false,
        dots: true,
        arrows: false,
        adaptiveHeight: false,
        infinite: false,
        focusOnSelect: true,
        lazyLoad: 'ondemand',
        responsive: [
          {
            breakpoint: 767,
            settings: "unslick"
          }
        ],
        customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data();
        return '<a class="home--slider-dot">0'+(i+1)+'</a>';

        }
    });

    homeSlider.on('init', function() {
        $('.slick-current').prev().addClass('prev');
        $('.slick-current').next().addClass('next');
    });

    homeSlider.on('beforeChange', function(){
        $('.slick-slide').removeClass('prev next');
    });

    homeSlider.on('afterChange', function() {
        $('.slick-current').prev().addClass('prev');
        $('.slick-current').next().addClass('next');
    });

    /* Show / Hide home testimonials */

    $('#testimonials-home').click(function(){
    	if($('#testimonials').css('display')!='block')
    	{
	    	$('#testimonials').slideDown();
	    	initTestimonials();
	    	/* Display random testimonial */
		    	var totalTestimonials = $('.testimonials__quote').length;
		    	var randTestimonials = Math.floor( Math.random() * totalTestimonials );
		    	sliderTestimonials.slick('slickGoTo',randTestimonials);
		}

		$('html,body').animate({scrollTop:$('#testimonials').offset().top},1000);

    });



    sliderMariismos.on('init', function(e, slick) {
        $('div.mariismos__slide:first-child').find('.mariismos__heading').addClass("animated");
        $('div.mariismos__slide:first-child').find('.mariismos_desc').addClass("animated");
    });

    /* Display random mantra */
	    $(document).ready(function(){
	    	var totalMariismos = $('.mariismos__image').length;
			var randMariismos = Math.floor( Math.random() * totalMariismos );
	    	sliderMariismos.slick('slickGoTo',randMariismos);
	    });


    sliderMariismos.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        $('div.mariismos__slide:not(".slide-current")').find('.mariismos__image, .mariismos__heading, .mariismos_desc').removeClass("animated");
        var $animatingElements = $('div.mariismos__slide[data-slick-index="' + nextSlide + '"]').find('.mariismos_desc, .mariismos__heading, .mariismos__image').addClass("animated");
    });

    sliderMariismos.slick({
      autoplay: false,
      speed: 800,
      fade: true,
      dots: true,
      arrows: false,
      adaptiveHeight: false,
      slide: '.mariismos__slide',
      lazyLoad: 'ondemand',
      responsive: [{
        breakpoint: 768,
        settings: {
          adaptiveHeight: true
        }
      }],
      customPaging : function(slider, i) {
      var thumb = $(slider.$slides[i]).data();
      return '<a class="mantras--slider-dot">0'+(i+1)+'</a>';
      }
    });

    sliderProjects.on('init', function(event, slick){
        $(this).find('.slick-dots').appendTo($(this).parent().find('.project__dots'));
      });

    // sliderProjects.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
    //     myPlayerAccelya1.pause();
    //     myPlayerAccelya2.pause();
    //     myPlayerAccelya3.pause();
    // });

    sliderProjects.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        // myPlayerAccelya1.pause();
        // myPlayerAccelya2.pause();
        // myPlayerAccelya3.pause();
        myPlayerMotion3.pause();
    });

    sliderProjects.slick({
        autoplay: false,
        speed: 800,
        fade: false,
        dots: true,
        arrows: false,
        adaptiveHeight: false,
        slide: '.project__slide',
        lazyLoad: 'ondemand',
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data();
            var sliderId = $(slider.$slides).parent().parent().parent().parent().attr('id');
            return '<a class="'+sliderId+'"> 0'+(i+1)+'</a>';
        }
    });

    if(!$('body').hasClass('home'))
    {
    	initTestimonials();


	    /* Display random testimonial */
		    	var totalTestimonials = $('.testimonials__quote').length;
		    	var randTestimonials = Math.floor( Math.random() * totalTestimonials );
		    	sliderTestimonials.slick('slickGoTo',randTestimonials);

    }

    function initTestimonials()
    {
	    sliderTestimonials.slick({
	        autoplay: false,
	        speed: 800,
	        fade: true,
	        dots: true,
	        arrows: false,
	        adaptiveHeight: true,
	        slide: '.testimonials__slide',
	        customPaging : function(slider, i) {
	            var thumb = $(slider.$slides[i]).data();
	            if (i >= 9) {
	                return '<a class="testimonial-num">'+(i+1)+'</a>';
	            } else {
	                return '<a class="testimonial-num">0'+(i+1)+'</a>';
	            }
	        }
	    });

	    sliderTestimonials.on('init', function(e, slick) {
	        $('div.testimonials__slide:first-child').find('.testimonials__quote, .testimonials__credits').addClass("animated");
	    });


	    sliderTestimonials.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
	    	$('div.testimonials__slide:not(".slide-current")').find('.testimonials__quote, .testimonials__credits').removeClass("animated");
	        $('div.testimonials__slide[data-slick-index="' + nextSlide + '"]').find('.testimonials__quote, .testimonials__credits').addClass("animated");
	    });
    }

    $(function() {

	    sliderCaseStudies.slick({
	        autoplay: false,
	        speed: 800,
	        fade: false,
	        dots: true,
	        arrows: false,
	        adaptiveHeight: false,
	        mobileFirst: true,
	        responsive: [
	        {
	            breakpoint: 768,
	            settings: 'unslick'
	        }
	    ],
	    customPaging : function(slider, i) {
	        var thumb = $(slider.$slides[i]).data();
	        if (i >= 9) {
	            return '<a>'+(i+1)+'</a>';
	        } else {
	            return '<a>0'+(i+1)+'</a>';
	        }
		}});

	});

    // Viewport Checker

    $('.home-projects__item.slick-current .link--cta').viewportChecker({});

    if ($(window).width() <= 768 && ( $('body').hasClass('services') || $('body').hasClass('servicios') || $('body').hasClass('serveis') ) ) {

        $('.service__image').viewportChecker({
            classToAdd: 'show-animation',
            offset: 20
        });

        $('.page-header__title.appear').viewportChecker();

    } else if ($(window).width() > 768 && ( $('body').hasClass('services') || $('body').hasClass('servicios') || $('body').hasClass('serveis') ) ) {

        $('#service-websites .service__image').viewportChecker({
            offset: 200
        });
        $('#service-videos .service__image, #service-emails .service__image, #service-campaigns .service__image').viewportChecker({
            offset: 420
        });

        $('.page-header__title').viewportChecker({
            classToAdd: 'visible',
            offset: 0
        });

    } else {
        $('.appear').viewportChecker();
    }

    $('.mariismos-likes .mariismos__image, .mariismos-likes .mariismos__text').viewportChecker({
        //classToAdd: 'animated', // Class to add to the elements when they are visible,
    });

    // Video JS

    if ( $("body").hasClass('projects') || $("body").hasClass('projectes') || $("body").hasClass('proyectos') || $("body").hasClass('motion') ){

        var myPlayerMotion3 = videojs('video-motion-3');
        var myMotion3 = function(){};
        myPlayerMotion3.on("play", myMotion3);

        myPlayerMotion3.on("ended", function () {
        	myPlayer.currentTime(0);
        	myPlayer.bigPlayButton.show();
        });
    }

    // if ( $("body").hasClass('projects') || $("body").hasClass('projectes') || $("body").hasClass('proyectos') || $("body").hasClass('accelya') ){
    //     var myPlayerAccelya1 = videojs('video-accelya-1');
    //     var myPlayerAccelya2 = videojs('video-accelya-2');
    //     var myPlayerAccelya3 = videojs('video-accelya-3');

    //     var myFunc1 = function(){
    //     myPlayerAccelya2.pause();
    //     myPlayerAccelya3.pause();
    //     };

    //     var myFunc2 = function(){
    //     myPlayerAccelya1.pause();
    //     myPlayerAccelya3.pause();
    //     };

    //     var myFunc3 = function(){
    //     myPlayerAccelya1.pause();
    //     myPlayerAccelya2.pause();
    //     };

    //     myPlayerAccelya1.on("play", myFunc1);
    //     myPlayerAccelya2.on("play", myFunc2);
    //     myPlayerAccelya3.on("play", myFunc3);


    //     myPlayerAccelya1.on("ended", function () {
    //     myPlayer.currentTime(0);
    //     myPlayer.bigPlayButton.show();
    //     });

    //     myPlayerAccelya2.on("ended", function () {
    //     myPlayer.currentTime(0);
    //     myPlayer.bigPlayButton.show();
    //     });

    //     myPlayerAccelya3.on("ended", function () {
    //     myPlayer.currentTime(0);
    //     myPlayer.bigPlayButton.show();
    //     });
    // }

    if ( $("body").hasClass('honda') ){
        var myPlayerHonda = videojs('miimo-video');

        myPlayerHonda.on("ended", function () {
            myPlayer.currentTime(0);
            myPlayer.bigPlayButton.show();
        });

    }

    if ( $("body").hasClass('projects') || $("body").hasClass('projectes') || $("body").hasClass('proyectos') || $("body").hasClass('catalana') ){

        $('.page-header__brief .link--cta').click(function(e) {
            e.preventDefault();
            $('html,body').animate({scrollTop:$('#catalana-occidente-videos').offset().top}, 1500);
        });

        var myPlayerCatalanaOne = videojs('video-catalana-1');
        var myPlayerCatalanaTwo = videojs('video-catalana-2');

        myPlayerCatalanaOne.on("ended", function () {
            myPlayer.currentTime(0);
            myPlayer.bigPlayButton.show();
        });

        myPlayerCatalanaTwo.on("ended", function () {
            myPlayer.currentTime(0);
            myPlayer.bigPlayButton.show();
        });

    }

    if ( $("body").hasClass('projects') || $("body").hasClass('projectes') || $("body").hasClass('proyectos') ) {
        $('body').removeClass('sticky-nav');
        setTimeout(function(){
            $('body').removeClass('sticky-nav');
          }, 250);
    }


    // Mobile Menu

    $('.hamburger').click(function(){
        $('.hamburger').toggleClass('open');
        $('body').toggleClass('has-nav');
    });

    // Cookies management

    	// Show cookies settings

			$('#cookieSettings').on("click", function(){
				$('.cookie-message').addClass('cookie-settings');
				$(this).hide();
				$('.cookie-message .base').hide();
				$('.cookie-message .buttons').hide();
				$('.cookie-message .settings').show();
				$('.cookie-message #saveCookies').css('display','inline-block');
			});

			$('.cookieSettingsText').on("click", function(){
				$('.cookie-message').addClass('cookie-settings');
				$('.cookie-message #cookieSettings').hide();
				$('.cookie-message .base').hide();
				$('.cookie-message .buttons').hide();
				$('.cookie-message .settings').show();
				$('.cookie-message #saveCookies').css('display','inline-block');

				// If the cookie GA is accepted, then check de checkbox by default
					if($.cookie("t2m-cookies-third-party")!=undefined && $.cookie("t2m-cookies-third-party")==1)
						$('#checkbox-party').attr('checked','checked');

				$('.cookie-message').addClass('show');
				$('.cookie-message').fadeIn('slow');
			});

		// Accept cookies

			$('#acceptCookies').on("click", function(){

				$.cookie("t2m-cookies-accept", "1", { expires: 365, path: '/' });
				$.cookie("t2m-cookies-reject", null, { expires: 0, path: '/' });
				$('.cookie-message').fadeOut('slow');

				// if($('#checkbox-party').is(":checked")) // If accept third party cookies
				// {
					$.cookie("t2m-cookies-third-party", "1", { expires: 365, path: '/' });
					appendThirdPartyScripts();
				// }
				// else{ // If does not accept third party cookies
					// $.cookie("t2m-cookies-third-party", "0", { expires: 365, path: '/' });
					// removeThirdPartyCookies(); // Remove third party old cookies
				// }
			});

		// Save selected cookies

			$('#saveCookies').on("click", function(){

				$.cookie("t2m-cookies-accept", "1", { expires: 365, path: '/' });
				$.cookie("t2m-cookies-reject", null, { expires: 0, path: '/' });
				$('.cookie-message').fadeOut('slow');

				if($('#checkbox-party').is(":checked")) // If accept third party cookies
				{
					$.cookie("t2m-cookies-third-party", "1", { expires: 365, path: '/' });
					appendThirdPartyScripts();
				}
				else{ // If does not accept third party cookies
					$.cookie("t2m-cookies-third-party", "0", { expires: 365, path: '/' });
					removeThirdPartyCookies(); // Remove third party old cookies
				}
			});

		// Reject cookies

			$('#rejectCookies').on("click", function(){

				$('.cookie-message').fadeOut('slow');
				$.cookie("t2m-cookies-reject", "1", { expires: 365, path: '/' });
				$.cookie("t2m-cookies-third-party", "0", { expires: 365, path: '/' });

				removeThirdPartyCookies(); // Remove third party old cookies
			});

		/** If Not accepted, show Cookies Info **/

			if($.cookie("t2m-cookies-accept")==undefined && $.cookie("t2m-cookies-reject")==undefined)
			{
				setTimeout(function(){
					$('.cookie-message').addClass('show');
				}, 100);
			}

		/** If Accepted Third party cookies */
			if($.cookie("t2m-cookies-accept")!=undefined && $.cookie("t2m-cookies-accept")==1 && $.cookie("t2m-cookies-third-party")!=undefined && $.cookie("t2m-cookies-third-party")==1)
			{
				appendThirdPartyScripts();
			}

		// Add third party scripts

		function appendThirdPartyScripts(){
			var config = {
			    'analytics_storage': "granted"
		  	};
		  	gtag('consent', 'update', config);
			// var str = "";
			// $('body').append(str);
		}

		// If third party cookies are rejected or refused

		function removeThirdPartyCookies(){
			var config = {
			    'analytics_storage': "denied"
		  	};
		  	gtag('consent', 'update', config);

			var DelCookies = Cookies.noConflict();
			var cookies    = DelCookies.get();


			// Third party cookies
				$.cookie("t2m-cookies-third-party", null, { path: '/' });

			// Google analytics

				// GA Domain URL - Whithout https / http and a "." at the begining
					// PROD
						var gaDomainURL = ".thetwomarias.com";
						var gaDomainURL2 = "www.thetwomarias.com";
					// DEV
						// var gaDomainURL = "dev.thetwomarias.com";
						// var gaDomainURL2 = ".thetwomarias.com";


				jQuery.each( cookies, function( index ) {
					if ( '_ga' === index || '_gid' === index || /^_gat_/.test(index) || /^_ga_/.test(index)) {
						$.cookie(index, null, { path: '/', domain: gaDomainURL });
						$.cookie(index, null, { path: '/', domain: gaDomainURL2 });
					}
				});
		}



    // Parallax

    if (!$("body").hasClass("home")){
        $('.project-block').paroller();
    	$('.service__block').paroller();
    	$('.location').paroller();
    }

    //custom function showing current slide
    var $status = $('.pagingInfo');
    var $slickElement = $('.home-mariismos__slider');

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 1) + 1;
        $status.text(i + '/' + slick.slideCount);
    });

    // Scroll Buttons
    $('.scroll-btn.scroll-up span').click(function(){
        $('html,body').animate({ scrollTop: 0 }, 1000 );
        return false;
    });

    if ($("body").hasClass("projects")){
        //$('#project-banners .project__slide--bg').data('vide').getVideoObject().play();
    }

    // Validation form Newsletter

    // if ($('#mc-embedded-subscribe-form').length) {
    	// $('#mc-embedded-subscribe-form').validate({
	        // errorElement : 'span',
	        // errorLabelContainer: '.error-text'
      	// });
	// }

    };

    $(window).scroll(function() {

        // Play Videos when visible

        $('.case-study.honda video').each(function(){
            if ($(window).width() > 992) {
            if ($(this).hasClass("visible")) {
                $(this)[0].play();
            } else {
                $(this)[0].pause();
            }
            }
        });

        // Sticky header
        const a = $(window).scrollTop();
        const b = navbar.height();

        currentScrollTop = a;

        if (c < currentScrollTop && a > b + b) {
            $('body').removeClass("sticky-nav");
        } else if (c > currentScrollTop && !(a <= b)) {
            $('body').addClass("sticky-nav");
        }
        c = currentScrollTop;

        // Hide Sticky Nav when header is visible

        if ($(window).scrollTop() > 200) {
            // The top header is not visible yet
            $('.scroll-btn.scroll').css("opacity","0");
            $('.scroll-btn.scroll').addClass('scrolled');
        } else {
            // The top header is visible - hide the sticky nav
            $('body').removeClass("sticky-nav");
            $('.scroll-btn.scroll').css("opacity","1");
            $('.scroll-btn.scroll').removeClass('scrolled');

        }

        var homeProjectItem = $('.home-projects__item');
        if (homeProjectItem.length) {
            if ($(window).width() > 768) {
            	var hT = $('.home-projects__item').offset().top - 200,
                hH = $('.home-projects__item').outerHeight(),
                wH = $(window).height(),
                wS = $(this).scrollTop();
                if (wS > (hT+hH-wH)){
                  $('.home-projects__item.next').addClass('appear-rtl');
                }
                if ($(window).scrollTop() >= 425 && $(window).scrollTop() <= 485) {
            	  $('.home-projects__item.next').addClass('now');
                  setTimeout(function(){ $('.home-projects__item.next').removeClass('now'); }, 1200);
                }
              }
        }


      });

    // Home - team section - Play vimeo video

	    $('#play-team-video').click(function(){

	    	$(this).fadeOut(300);
	    	$('#team-poster-image').fadeOut(300);
	    	$("#team-video").show();

	    	var player = new Vimeo.Player(jQuery("#team-video")[0]);

		    player.play();
		    player.on('ended', function() {
		        $('#play-team-video').fadeIn(300);
		    	$('#team-poster-image').fadeIn(300);
		    	$("#team-video").hide();
		    	player.stop();

		    });


	    	// $("#team-video").vimeo("play");

	    });


    /**
     * Some function
     *
     * @since   1.0.0
     */
    // const someFunction = () => {
    //     // do something
    // };

    /**
     * Fire events on document ready and bind other events
     *
     * @since   1.0.0
     */
    const ready = () => {
        someFunction();
    };

    /**
     * Only expose the ready function to the world
     */
    return {
        ready: ready
    }



})(jQuery);

jQuery(common.ready);

