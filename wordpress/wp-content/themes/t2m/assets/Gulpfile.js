'use strict';

const gulp = require('gulp');
const babel = require('gulp-babel');
// const sass = require('gulp-sass');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

/**
 * Here we set a prefix for our compiled and stylesheet and scripts.
 * Note that this should be the same as the `$themeHandlePrefix` in `func-script.php` and `func-style.php`.
 */
const themePrefix = 'theme-name';

/**
 * Asset paths.
 */
const scssSrc = 'scss/**/*.scss';
const jsSrcDir = 'js';
const jsSrcFiles = [
    // `${jsSrcDir}/vendors/some-library.js`,
    `${jsSrcDir}/scripts/common.js`,
];

/**
 * Task for styles.
 *
 * Scss files are compiled and sent over to `assets/css/`.
 */
gulp.task('css', function () {
	// Where is scss file
    return gulp.src(scssSrc)
    // Pass that file through sass compiler
        .pipe(sass().on('error', sass.logError))
    // Add cross-browser prefix
        .pipe(autoprefixer({
            Browserslist : ['last 3 versions', '> 5%', 'Explorer >= 10', 'Safari >= 8'],
            cascade : false
        }))
    // Rename file minified
        .pipe(rename(`${themePrefix}.min.css`))
        .pipe(cleancss())
    // where do I save the compiled CSS?
        .pipe(gulp.dest('./css/'));
});

/**
 * Task for scripts.
 *
 * Js files are uglified and sent over to `assets/js/scripts/`.
 */
gulp.task('js', function () {
    return gulp.src(jsSrcFiles)
        .pipe(babel({
            presets : ['es2015']
        }))
        .pipe(concat(`${themePrefix}.min.js`))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

/**
 * Task for watching styles and scripts.
 */
gulp.task('watch', function () {
    // gulp.watch(scssSrc, ['css']);
    gulp.watch(scssSrc, gulp.series('css'));
    // gulp.watch(jsSrcFiles, ['js']);
    gulp.watch(jsSrcFiles, gulp.series('js'));
});
