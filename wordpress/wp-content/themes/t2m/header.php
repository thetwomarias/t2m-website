<?php
/**
 * Header template
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title( '&ndash;', true, 'right' ); ?></title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- The initial config of Consent Mode -->
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('consent', 'default', {
			ad_storage: 'denied',
			analytics_storage: 'denied',
			wait_for_update: 1500,
		});
		gtag('set', 'ads_data_redaction', true);
	</script>
<!-- Google tag (gtag.js) --><script async src="https://www.googletagmanager.com/gtag/js?id=G-SK6WKX3ZXX"></script><script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		  gtag('config', 'G-SK6WKX3ZXX');
		</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>
	<?php
		$template_file = get_page_template_slug();
		if($template_file == 'templates/meta.php'){
			if(wp_is_mobile()){
				?>
					onload="metaInit();"
				<?php
			}
			else{
				?>
					onload="metaInit(); metaInit2();"
				<?php
			}

		}
	?> >
	<script type="text/javascript">

	</script>

	<!-- Google Tag Manager (noscript) -->
	<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KC9D7XZ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
	<!-- End Google Tag Manager (noscript) -->
	<header class="header">

		<!-- Logo Pre 2023 -->
			<div class="logo appear">
				<?php
				$linkHome = apply_filters( 'wpml_home_url', get_option( 'home' ) );
				?>
				<a href="<?php echo $linkHome; ?>" class="logo__link"></a>
				<div id="bm" class="logo-image logo-animated" style="display:none;"></div>
				<object class="logo-image logo-static" type="image/svg+xml" data="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-static.svg" preserveAspectRatio="xMidYMid meet">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-static.svg" alt="The Two Marias" title="The Two Marias"/>
				</object>
				<object class="logo-image logo-mobile" style="display:none" type="image/svg+xml" data="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-mobile.svg" preserveAspectRatio="xMidYMid meet">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-mobile.svg" alt="The Two Marias" title="The Two Marias"/>
				</object>
			</div>

		<!-- Hamburger Menu -->
		<div class="hamburger">
			<div class="hamburger-icon">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			</div>
		</div>

		<!-- Main Navigation -->

		<nav class="navigation" role="navigation">
			<?php
			$linkHome = apply_filters( 'wpml_home_url', get_option( 'home' ) );
			wp_nav_menu(array(
				'theme_location' => 'primary',
				'container' => false,
				'menu_id' => 'main__menu',
				'menu_class' => '',
				'link_before' => '<span>',
       			'link_after' => '</span>',
				'items_wrap' => '<ul class="nav">
					%3$s</ul>
					',
					'depth' => 2
				));
		 	?>

		</nav>

		<div class="nav--sticky">
			<div class="nav--sticky-content">
			<div class="logo-small">
				<a href="<?php echo $linkHome; ?>" class="logo__link"></a>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo-small.svg" alt="The Two Marias" title="The Two Marias"/>
			</div>

			<div class="hamburger">
				<div class="hamburger-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				</div>
			</div>

			<nav class="navigation" role="navigation">
				<?php
				$linkHome = apply_filters( 'wpml_home_url', get_option( 'home' ) );
				wp_nav_menu(array(
					'theme_location' => 'primary',
					'container' => false,
					'menu_id' => 'main__menu',
					'menu_class' => '',
					'items_wrap' => '%3$s
						',
						'depth' => 2
					));
				?>

			</nav>
			</div>
		</div>

	</header>


	<main class="main">
