<?php
/*
Template Name: Services Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed appear appear-only"><span><?php _e('Scroll','t2m'); ?></span></div>
    <div class="page-header__container">
    <h1 class="page-header__title appear"><?php the_field('page_header_title'); ?></h1>
  </div>
</section>

<?php
    $query = new WP_Query(array(
    'post_type' => 'service',
    'post_status' => 'publish'
  ));
  $counter = 1;
  while ($query->have_posts()) {
    $query->the_post();
    $post_id = get_the_ID();
    $serviceTitle = str_replace(array('<p>','</p>'),'', get_field('service_title'));
    $serviceDescription = str_replace(array('<p>','</p>'),'', get_field('service_description'));
    $serviceImage = get_field('service_image');
    $serviceImagePlaceholder = get_field('service_image_placeholder');
    $serviceSvg = get_field('service_svg');
    $serviceId = get_field('service_id');
    $serviceAlignment = get_field('service_alignment');
    $serviceLinkText = get_field('service_link_text');
    $serviceLinkUrl = get_field('service_link_url');
    $parollerFactor = get_field('paroller_factor');
  ?>

  <section class="service__block service__block--<?php echo $serviceAlignment; ?>" id="service-<?php echo $serviceId; ?>" data-paroller-factor="<?php echo $parollerFactor; ?>" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="service__image">
    <img class="service__image service__image--placeholder" src="<?php echo $serviceSvg['url']; ?>" alt="<?php echo $serviceSvg['alt']; ?>" title="<?php echo $serviceSvg['title']; ?>"/>
    <object type="image/svg+xml" data="<?php echo $serviceSvg['url']; ?>" preserveAspectRatio="xMidYMid meet">
      <img src="<?php echo $serviceImage['url']; ?>" alt="<?php echo $serviceImage['alt']; ?>" title="<?php echo $serviceImage['title']; ?>"/>
    </object>
    </div>
    <div class="service__text">
        <h2 class="service__title"><?php echo $serviceTitle; ?></h2>
        <div class="service__desc"><?php echo $serviceDescription; ?></div>
        <a class="link--cta" href="<?php echo $serviceLinkUrl; ?>"><span><?php echo $serviceLinkText; ?></span></a>
    </div>
  </section>

  <?php if ($counter % 2 == 0 && $counter != 4){echo '<div class="image--absolute show-sm hide-md hide-lg"><img src="/wp-content/themes/t2m/assets/images/backgrounds/services-newsletters.svg" alt="Services Newsletters background"></div>';} ?>

  <?php $counter++ ;
    }
    wp_reset_query();
  ?>

<section class="services-bottom bottom-block">


<div class="bottom-block__wrapper">

  <div class="bottom-block__content">

    <div class="bottom-block__text">
      <h6 class="bottom-block__heading"><?php the_field('bottom_title'); ?></h6>
      <div class="bottom-block__desc"><?php the_field('bottom_description'); ?></div>
    </div>

    <div class="bottom-block__image">

        <?php

          $bottomImageRight = get_field('bottom_image_right');
          $bottomSvgRight = get_field('bottom_svg_right');
          $bottomSvgRightMobile = get_field('bottom_svg_right_mobile');

          if( !empty($bottomSvgRightMobile) ): ?>

          <object type="image/svg+xml" data="<?php echo $bottomSvgRightMobile ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="show-sm show-md hide-lg">
            <img src="<?php echo $bottomSvgRightMobile ['url']; ?>" alt="<?php echo $bottomSvgRightMobile ['alt']; ?>" title="<?php echo $bottomSvgRightMobile ['title']; ?>" class="hide-mobile show-desktop" />
          </object>


          <?php endif; ?>

          <?php if( !empty($bottomSvgRight) ): ?>

          <object type="image/svg+xml" data="<?php echo $bottomSvgRight ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="hide-sm hide-md show-lg">
            <img src="<?php echo $bottomImageRight ['url']; ?>" alt="<?php echo $bottomImageRight ['alt']?>" title="<?php echo $bottomImageRight ['title']?>" class="hide-mobile show-desktop" />
          </object>

          <?php endif; ?>

        </div>

  </div>

</div>

</section>



<?php
get_footer();
?>
