<?php
/*
Template Name: Team Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed appear appear-only"><span><?php _e('Scroll','t2m'); ?></span></div>
    <div class="page-header__container">
    <h1 class="page-header__title appear"><?php the_field('page_header_title'); ?></h1>
    <h2 class="page-header__subheading appear"><?php the_field('page_header_subheading'); ?></h2>
  </div>
</section>

<section class="members members--two">

<?php
    $query = new WP_Query(array(
    'post_type' => 'team_member',
    'post_status' => 'publish',
    'meta_query' => array(
      array(
      'key' => 'member_type',
      'value' => 'director'
      )
    )
  ));

  while ($query->have_posts()) {
    $query->the_post();
    $post_id = get_the_ID();
    $memberName = get_field('member_name');
    $memberPosition = get_field('member_position');
    $memberImage = get_field('member_image');
    $memberType = get_field('member_type');
  ?>

  <div class="member appear">
  <img class="member__image" src="<?php echo $memberImage['url'];?>" alt="<?php echo $memberImage['url'];?>" title="<?php echo $memberImage['title'];?>" />
    <h3 class="member__name"><?php echo $memberName ;?></h3>
    <h4 class="member__role"><?php echo $memberPosition ;?></h4>
  </div>

  <div class="member__separator"></div>

  <?php
    }
    wp_reset_query();
  ?>


</section>

<section class="team-intro team-intro--first appear">
    <div class="team-intro--left"><?php the_field('team_intro_left'); ?></div>
    <div class="team-intro--right"><?php the_field('team_intro_right'); ?></div>
</section>

<!-- <section class="members members--five appear"> -->
<section class="members members--six appear">

<?php
    $query = new WP_Query(array(
    'post_type' => 'team_member',
    'post_status' => 'publish',
    'showposts' => 6,
    'meta_query' => array(
      array(
      'key' => 'member_type',
      'value' => 'team'
      )
    )
  ));

  while ($query->have_posts()) {
    $query->the_post();
    $post_id = get_the_ID();
    $memberName = get_field('member_name');
    $memberPosition = get_field('member_position');
    $memberImage = get_field('member_image');
    $memberType = get_field('member_type');
  ?>

  <div class="member">
    <img class="member__image" src="<?php echo $memberImage['url'];?>" alt="<?php echo $memberImage['url'];?>" title="<?php echo $memberImage['title'];?>" />
    <h3 class="member__name"><?php echo $memberName ;?></h3>
    <h4 class="member__role"><?php echo $memberPosition ;?></h4>
  </div>

  <div class="member__separator"></div>

  <?php
    }
    wp_reset_query();
  ?>

  <img class="background--absolute left-0 bottom-20 minus-z show-sm hide-lg hide-md full" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-pink.svg" alt="Pink corner"/>

</section>

<!-- <section class="team-intro team-intro--freelancers background--pink appear"> -->
<section class="team-intro team-intro--freelancers appear">
    <div class="team-intro--left">
    <?php the_field('freelancers_intro'); ?>
    </div>
</section>

<!-- <section class="members members--four members--freelancers background--transparent"> -->
<section class="members members--six members--freelancers background--transparent">

  <div class="background--absolute left-0 top-10 h-400 full background--pink full" style="display:none;"></div>

  <div class="members__wrapper members__wrapper--first appear">

  <?php
    $query = new WP_Query(array(
    'post_type' => 'team_member',
    'post_status' => 'publish',
    'showposts' => 6,
    'order' => 'asc',
    'meta_query' => array(
      array(
      'key' => 'member_type',
      'value' => 'freelance'
      )
    )
  ));

  while ($query->have_posts()) {
    $query->the_post();
    $post_id = get_the_ID();
    $memberName = get_field('member_name');
    $memberPosition = get_field('member_position');
    $memberImage = get_field('member_image');
    $memberType = get_field('member_type');
  ?>

  <div class="member">
  <img class="member__image" src="<?php echo $memberImage['url'];?>" alt="<?php echo $memberImage['url'];?>" title="<?php echo $memberImage['title'];?>" />
    <h3 class="member__name"><?php echo $memberName ;?></h3>
    <h4 class="member__role"><?php echo $memberPosition ;?></h4>
  </div>

  <?php
    }
    wp_reset_query();
  ?>

  </div>
</section>

<section class="members members--six members--freelancers background--transparent">

  <!-- <div class="background--absolute left-0 top-10 h-400 full background--pink full" style="display:none;"></div>


  <div class="members__wrapper appear">

  <?php
    $query = new WP_Query(array(
    'post_type' => 'team_member',
    'post_status' => 'publish',
    'showposts' => 6,
    'offset' => 6,
    'order' => 'asc',
    'meta_query' => array(
      array(
      'key' => 'member_type',
      'value' => 'freelance'
      )
    )
  ));

  while ($query->have_posts()) {
    $query->the_post();
    $post_id = get_the_ID();
    $memberName = get_field('member_name');
    $memberPosition = get_field('member_position');
    $memberImage = get_field('member_image');
    $memberType = get_field('member_type');
  ?>

  <div class="member">
  <img class="member__image" src="<?php echo $memberImage['url'];?>" alt="<?php echo $memberImage['url'];?>" title="<?php echo $memberImage['title'];?>" />
    <h3 class="member__name"><?php echo $memberName ;?></h3>
    <h4 class="member__role"><?php echo $memberPosition ;?></h4>
  </div>

  <?php
    }
    wp_reset_query();
  ?>


  </div> -->


  <img class="background--absolute left-0 bottom-20 minus-z full" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-pink.svg" alt="Pink corner"/>

</section>

<section class="team-bottom bottom-block">

  <div class="bottom-block__wrapper"  style="background-color:<?php the_field('bottom_background_color'); ?>">

    <div class="bottom-block__content">

      <div class="bottom-block__text">
        <h6 class="bottom-block__heading"><?php the_field('bottom_title'); ?></h6>
        <div class="bottom-block__desc"><?php the_field('bottom_description', false, false); ?></div>
      </div>

      <div class="bottom-block__image">
      <?php

        $bottomImageRight = get_field('bottom_image_right');
        $bottomSvgRight = get_field('bottom_svg_right');
        $bottomSvgRightMobile = get_field('bottom_svg_right_mobile');

        if( !empty($bottomSvgRightMobile) ): ?>

        <object type="image/svg+xml" data="<?php echo $bottomSvgRightMobile ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="show-sm show-md hide-lg">
          <img src="<?php echo $bottomSvgRightMobile ['url']; ?>" alt="<?php echo $bottomSvgRightMobile ['alt']; ?>" title="<?php echo $bottomSvgRightMobile ['title']; ?>" class="hide-mobile show-desktop" />
        </object>


        <?php endif; ?>

        <?php if( !empty($bottomSvgRight) ): ?>

        <object type="image/svg+xml" data="<?php echo $bottomSvgRight ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="hide-sm hide-md show-lg">
          <img src="<?php echo $bottomImageRight ['url']; ?>" alt="<?php echo $bottomImageRight ['alt']?>" title="<?php echo $bottomImageRight ['title']?>" class="hide-mobile show-desktop" />
        </object>

        <?php endif; ?>

      </div>

    </div>

  </div>

</section>




<?php
get_footer();
?>
