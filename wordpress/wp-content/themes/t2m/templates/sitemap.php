<?php
/*
Template Name: Sitemap Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed"><span><?php _e('Scroll','t2m'); ?></span></div>
  <div class="page-header__container">
    <h1 class="page-header__title"><?php the_field('page_header_title'); ?></h1>
  </div>
</section>

<?php if( have_rows('sitemap_link') ): ?>
  <section class="sitemap-content appear">
  <?php while( have_rows('sitemap_link') ): the_row();
        $sitemapLinkText = get_sub_field('sitemap_link_text');
        $sitemapLinkUrlId = get_sub_field('sitemap_link_url');

        $sitemapLinkHasCaseStudy = get_field('has_case_study',$sitemapLinkUrlId);
		$sitemapLinkId = get_field('project_id',$sitemapLinkUrlId);

        $sitemapLinkUrl = get_the_permalink($sitemapLinkUrlId);
		if(!$sitemapLinkHasCaseStudy && isset($sitemapLinkId))
			$sitemapLinkUrl = get_the_permalink(wp_get_post_parent_id($sitemapLinkUrlId))."#".$sitemapLinkId;
      ?>
  <h2 class="sitemap__heading"><a class="sitemap__link" href="<?php echo $sitemapLinkUrl; ?>"><?php echo $sitemapLinkText; ?></a></h2>
  <?php endwhile; ?>

  </section>
<?php endif; ?>


<?php
get_footer();
