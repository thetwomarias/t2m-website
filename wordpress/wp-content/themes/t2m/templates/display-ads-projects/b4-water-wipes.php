<?php
	/**
	$b4_ww = get_field('b4_water_wipes');
	?>
		<section class="case-study__images background--purple">

				<?php
				$b4_brand = $b4_ww['brand'];
				$b4_client = $b4_ww['agency'];
				$b4_campaign = $b4_ww['campaign'];
				$b4_image1 = $b4_ww['image1'];
				$b4_image2 = $b4_ww['image2'];
				$b4_image3 = $b4_ww['image3'];
			?>

			<div class="campaign b4">
				<div class="campaign-specs appear">
					<div>
						<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b4_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b4_client; ?></h3>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b4_campaign; ?></h2>
					</div>
				</div>
				<div class="show-desktop campaign-img appear">
					<?php
						if($b4_image1['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b4_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b4_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b4_image1['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
				<div class="show-mobile campaign-img appear">
					<?php
						if($b4_image2['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b4_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b4_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b4_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
					<?php
						if($b4_image3['type']=="image")
						{
							?>
								<img class="b4-i3 left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b4_image3['image'],'full-size')[0]; ?>" alt="<?php echo $b4_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="b4-i3 left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b4_image3['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
			</div>
		</section>
	<?php
	**/
?>