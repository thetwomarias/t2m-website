<?php
	$b2_barclays = get_field('b2_barclays');
	?>
		<section class="case-study__images background--purple">

				<?php
				$b2_client = $b2_barclays['agency'];
				$b2_brand = $b2_barclays['brand'];
				$b2_campaign = $b2_barclays['campaign'];
				$b2_image1 = $b2_barclays['image1'];
				$b2_image2 = $b2_barclays['image2'];
				$b2_image3 = $b2_barclays['image3'];
			?>

			<div class="campaign b2">
				<div class="campaign-specs appear">
					<div>
						<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b2_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b2_client; ?></h3>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b2_campaign; ?></h2>
					</div>
				</div>
				<div class="campaign-img campaign-img1 appear">
					<?php
						if($b2_image1['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b2_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<div class="mobile-mask-container">
									<img class="mobile-mask" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/b2-mobile-mask-video.png" alt=""/>
									<div class="mobile-mask-video-container">
										<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b2_image1['video']); ?>" playsinline muted autoplay loop></video>
									</div>
								</div>
							<?php
						}
					?>
				</div>
				<div class="campaign-img campaign-img2 appear">

					<?php
						if($b2_image2['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b2_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b2_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
					<?php
						if($b2_image3['type']=="image")
						{
							?>
								<img class="left-0 bottom-0 b2-i3" src="<?php echo wp_get_attachment_image_src($b2_image3['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0 b2-i3" class="b2-img3" src="<?php echo wp_get_attachment_url($b2_image3['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>

				</div>

				<img class="show-mobile background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink-mobile.svg" alt=""/>
				<img class="show-desktop background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink.svg" alt=""/>
			</div>
		</section>