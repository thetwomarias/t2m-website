<?php
/**
	$b5_mg = get_field('b5_mg');
	?>
		<section class="case-study__images background--pink">

				<?php
				$b5_brand = $b5_mg['brand'];
				$b5_client = $b5_mg['agency'];
				$b5_campaign = $b5_mg['campaign'];
				$b5_image1 = $b5_mg['image1'];
				$b5_image2 = $b5_mg['image2'];
				$b5_image3 = $b5_mg['image3'];
				$b5_image4 = $b5_mg['image4'];
			?>

			<div class="campaign b5">
				<div class="campaign-specs appear">
					<div>
						<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b5_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b5_client; ?></h3>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b5_campaign; ?></h2>
					</div>
					<div class="campaign-img campaign-img1 show-desktop">
						<?php
							if($b5_image1['type']=="image")
							{
								?>
									<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b5_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b5_campaign; ?>"/>
								<?php
							}
							else
							{
								?>
									<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b5_image1['video']); ?>" playsinline muted autoplay loop></video>
								<?php
							}
						?>
					</div>
				</div>
				<div class="campaign-img campaign-img2 appear">
					<?php
						if($b5_image2['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b5_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b5_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b5_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
				<div class="campaign-img campaign-img3 show-mobile appear">
					<?php
						if($b5_image1['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b5_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b5_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b5_image1['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
				<div class="campaign-img campaign-img4 appear">
					<?php
						if($b5_image3['type']=="image")
						{
							?>
								<img class="show-desktop left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b5_image3['image'],'full-size')[0]; ?>" alt="<?php echo $b5_campaign; ?>"/>
								<img class="show-mobile left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b5_image3['image_mobile'],'full-size')[0]; ?>" alt="<?php echo $b5_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b5_image3['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>

				<img class="show-mobile background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-purple-mobile.svg" alt=""/>
				<img class="show-desktop background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-purple.svg" alt=""/>
			</div>
		</section>

	<?php
	**/
?>