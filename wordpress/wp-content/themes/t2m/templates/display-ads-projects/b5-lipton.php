<?php
	$b3_lipton = get_field('b3_lipton');
	?>
		<section class="case-study__images">

				<?php
				$b3_client = $b3_lipton['agency'];
				$b3_brand = $b3_lipton['brand'];
				$b3_campaign = $b3_lipton['campaign'];
				$b3_image1 = $b3_lipton['image1'];
				$b3_image1m = $b3_lipton['image1_mobile'];
				$b3_image2 = $b3_lipton['image2'];
				$b3_image2m = $b3_lipton['image2_mobile'];
				$b3_text = str_replace(['<p>', '</p>'], '', $b3_lipton['text']);
			?>

			<div class="campaign b3">
				<div class="campaign-specs appear">
					<div>
						<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b3_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b3_client; ?></h3>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b3_campaign; ?></h2>
					</div>
					<div class="show-mobile campaign-text">
						<?php echo $b3_text; ?>
					</div>
				</div>
				<div class="campaign-img campaign-img1 appear">
					<?php
						if($b3_image1['type']=="image")
						{
							?>
								<img class="show-desktop left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b3_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b3_campaign; ?>"/>
								<img class="show-mobile left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b3_image1['image_mobile'],'full-size')[0]; ?>" alt="<?php echo $b3_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b3_image1['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
				<div class="r2">
					<div class="campaign-img campaign-img2 appear">
						<?php
						if($b3_image1['type']=="image")
						{
							?>
								<img class="show-desktop left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b3_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b3_campaign; ?>"/>
								<img class="show-mobile left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b3_image2['image_mobile'],'full-size')[0]; ?>" alt="<?php echo $b3_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b3_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
					</div>
					<div class="show-desktop campaign-text appear">
						<?php echo $b3_text; ?>
					</div>
				</div>
				<img class="background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink-mobile-medium.svg" alt=""/>
			</div>
		</section>