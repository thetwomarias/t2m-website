<?php
	// B1 - Facebook (old B5 - M&G)

		/*

		$b1_facebook = get_field('b1_facebook');
		?>
			<section class="case-study__images">

					<?php
					$b1_brand = $b1_facebook['brand'];
					$b1_campaign = $b1_facebook['campaign'];
					$b1_description = $b1_facebook['description'];
					$b1_image1 = $b1_facebook['image1'];
					$b1_image2 = $b1_facebook['image2'];
					$b1_image3 = $b1_facebook['image3'];
				?>

				<div class="campaign b2 b1-facebook">
					<div class="campaign-specs appear">
						<div>
							<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
							<h2 class="page-header__specs--desc"><?php echo $b1_brand; ?></h2>
						</div>
						<div>
							<h4 class="page-header__specs--title"><?php _e('Campaigns','t2m'); ?></h4>
							<h2 class="page-header__specs--desc"><?php echo $b1_campaign; ?></h2>
						</div>
						<div>
							<h4 class="page-header__specs--title"><?php _e('Collaboration','t2m'); ?></h4>
							<h3 class="page-header__specs--desc"><?php echo $b1_description; ?></h3>
						</div>

					</div>
					<div class="campaign-img campaign-img1 appear">
						<?php
							if($b1_image1['type']=="image")
							{
								?>
									<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b1_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b1_campaign; ?>"/>
								<?php
							}
							else
							{
								?>
									<div class="mobile-mask-container">
										<img class="mobile-mask" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/b2-mobile-mask-video.png" alt=""/>
										<div class="mobile-mask-video-container">
											<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b1_image1['video']); ?>" playsinline muted autoplay loop></video>
										</div>
									</div>
								<?php
							}
						?>
					</div>
					<div class="campaign-img campaign-img2 appear">

						<?php
							if($b1_image2['type']=="image")
							{
								?>
									<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b1_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b1_campaign; ?>"/>
								<?php
							}
							else
							{
								?>
									<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b1_image2['video']); ?>" playsinline muted autoplay loop></video>
								<?php
							}
						?>
						<?php
							if($b1_image3['type']=="image")
							{
								?>
									<img class="left-0 bottom-0 b2-i3" src="<?php echo wp_get_attachment_image_src($b1_image3['image'],'full-size')[0]; ?>" alt="<?php echo $b1_campaign; ?>"/>
								<?php
							}
							else
							{
								?>
									<video class="left-0 bottom-0 b2-i3" class="b2-img3" src="<?php echo wp_get_attachment_url($b1_image3['video']); ?>" playsinline muted autoplay loop></video>
								<?php
							}
						?>

					</div>

					<img class="show-mobile background--absolute rotate-180 left-0 top-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-white.svg" alt=""/>
					<img class="show-desktop background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink.svg" alt=""/>
				</div>
			</section>

		<?php

	*/