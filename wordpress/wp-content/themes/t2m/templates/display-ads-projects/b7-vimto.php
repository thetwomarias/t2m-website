<?php
	$b7 = get_field('b7');
	?>
	<section class="case-study__images case-study case-study-v2 ">
			<?php
			$b7_brand = $b7['brand'];
			$b7_campaign = $b7['campaign'];
			$b7_description = $b7['description'];
			$b7_image1 = $b7['image1'];
			$b7_image2 = $b7['image2'];
			$b7_image3 = $b7['image3'];
			$b7_image4 = $b7['image4'];
		?>

		<div class="campaign b2 b6 b7">

			<img class="bg-image show-mobile background--absolute left-0 top-0 rotate-180" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink-mobile.svg" alt=""/>
			<img class="bg-image show-desktop background--absolute left-0 top-0 rotate-180" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink.svg" alt=""/>

			<div class="col-imgs">
				<div class="col-imgs-1">
					<div class="media-container text-center appear">
						<video class="appear left-0 bottom-0" src="<?php echo $b7_image1['video']; ?>" playsinline muted autoplay loop></video>
					</div>
				</div>
				<div class="col-imgs-2">
					<div class="media-container text-center appear">
						<img class=" appear" src="<?php echo $b7_image2['image']; ?>" alt="<?php echo $b7_campaign . "-". $b7_brand; ?>"/>
					</div>
					<div class="media-container text-center appear">
						<img class=" appear" src="<?php echo $b7_image3['image']; ?>" alt="<?php echo $b7_campaign . "-". $b7_brand; ?>"/>
					</div>
					<div class="media-container text-center appear">
						<img class=" appear" src="<?php echo $b7_image4['image']; ?>" alt="<?php echo $b7_campaign . "-". $b7_brand; ?>"/>
					</div>
				</div>
			</div><!--
			--><div class="campaign-specs appear">
				<div class="appear">
					<h4 class="page-header__specs--title"><?php _e('Brands','t2m'); ?></h4>
					<h2 class="page-header__specs--desc"><?php echo $b7_brand; ?></h2>
				</div>
				<div class="appear">
					<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
					<h2 class="page-header__specs--desc"><?php echo $b7_campaign; ?></h2>
				</div>
				<?php
				if(isset($b7_description) && $b7_description!=''){
					?>
						<div class="appear">
							<h4 class="page-header__specs--title"><?php _e('Campaigns','t2m'); ?></h4>
							<h2 class="page-header__specs--desc"><?php echo $b7_description; ?></h2>
						</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
