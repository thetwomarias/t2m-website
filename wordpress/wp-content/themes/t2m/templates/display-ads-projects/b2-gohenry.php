<?php
	$b2_gohenry = get_field('b2_gohenry');
	?>
		<section class="case-study__images background--pink">

				<?php
				$b2_agency = $b2_gohenry['agency'];
				$b2_brand = $b2_gohenry['brand'];
				$b2_campaign = $b2_gohenry['campaign'];
				$b2_image1 = $b2_gohenry['image1'];
				$b2_image2 = $b2_gohenry['image2'];
				$b2_image3 = $b2_gohenry['image3'];
			?>

			<div class="campaign b4 b2-gohenry">
				<div class="campaign-specs appear">
					<!-- <div>
						<h4 class="page-header__specs--title"><?php _e('Agency','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b2_agency; ?></h3>
					</div> -->
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b2_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b2_campaign; ?></h2>
					</div>
				</div>
				<div class="show-desktop campaign-img appear">
					<?php
						if($b2_image1['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b2_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b2_image1['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
				<div class="show-mobile campaign-img appear">
					<?php
						if($b2_image2['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b2_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b2_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
					<?php
						if($b2_image3['type']=="image")
						{
							?>
								<img class="b2-i3 left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b2_image3['image'],'full-size')[0]; ?>" alt="<?php echo $b2_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="b2-i3 left-0 bottom-0" class="b2-img3" src="<?php echo wp_get_attachment_url($b2_image3['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>
			</div>
		</section>