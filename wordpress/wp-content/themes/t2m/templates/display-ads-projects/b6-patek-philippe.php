<?php
	$b6 = get_field('b6');
	?>
	<section class="case-study__images case-study case-study-v2 ">
			<?php
			$b6_brand = $b6['brand'];
			$b6_campaign = $b6['campaign'];
			$b6_description = $b6['description'];
			$b6_image1 = $b6['image1'];
			$b6_image2 = $b6['image2'];
			$b6_image3 = $b6['image3'];
		?>

		<div class="campaign b2 b6">
			<div class="campaign-specs appear">
				<div>
					<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
					<h2 class="page-header__specs--desc"><?php echo $b6_brand; ?></h2>
				</div>
				<div>
					<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
					<h2 class="page-header__specs--desc"><?php echo $b6_campaign; ?></h2>
				</div>
				<div>
					<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
					<h3 class="page-header__specs--desc"><?php echo $b6_description; ?></h3>
				</div>
			</div><!--
			--><div class="col col-2 appear">
				<div class="media-container">
					<img class=" appear" src="<?php echo $b6_image1['image']; ?>" alt="<?php echo $b6_brand . "-". $b6_campaign; ?>"/>
				</div>
			</div><!--
			--><div class="col col-3 appear">
				<div class="media-container text-center">
					<video class="appear left-0 bottom-0" src="<?php echo $b6_image2['video']; ?>" playsinline muted autoplay loop></video>
				</div>
			</div><!--
			--><div class="col col-4 appear">
				<div class="media-container text-center">
					<img class=" appear" src="<?php echo $b6_image3['image']; ?>" alt="<?php echo $b6_brand . "-". $b6_campaign; ?>"/>
				</div>
			</div>
		</div>
	</section>
