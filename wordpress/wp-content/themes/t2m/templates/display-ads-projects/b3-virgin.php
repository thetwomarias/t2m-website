<?php
	$b1_virgin = get_field('b1_virgin');
	?>
		<section class="case-study__images">

				<?php
				$b1_brand = $b1_virgin['brand'];
				$b1_client = $b1_virgin['agency'];
				$b1_campaign = str_replace(['<p>', '</p>'], '', $b1_virgin['campaign']);
				$b1_image1 = $b1_virgin['image1'];
				$b1_image2 = $b1_virgin['image2'];
			?>

			<div class="campaign b1">
				<div class="campaign-specs appear">
					<div>
						<h4 class="page-header__specs--title"><?php _e('Brand','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b1_brand; ?></h2>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Client','t2m'); ?></h4>
						<h3 class="page-header__specs--desc"><?php echo $b1_client; ?></h3>
					</div>
					<div>
						<h4 class="page-header__specs--title"><?php _e('Campaign','t2m'); ?></h4>
						<h2 class="page-header__specs--desc"><?php echo $b1_campaign; ?></h2>
					</div>
				</div>
				<div class="campaign-img campaign-img1 appear">
					<?php
						if($b1_image1['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b1_image1['image'],'full-size')[0]; ?>" alt="<?php echo $b1_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<div class="mobile-mask-container">
									<img class="mobile-mask" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/b1-mobile-mask-video.png" alt=""/>
									<div class="mobile-mask-video-container">
										<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b1_image1['video']); ?>" playsinline muted autoplay loop></video>
									</div>
								</div>
							<?php
						}
					?>
				</div>
				<div class="campaign-img campaign-img2 appear">
					<?php
						if($b1_image2['type']=="image")
						{
							?>
								<img class="left-0 bottom-0" src="<?php echo wp_get_attachment_image_src($b1_image2['image'],'full-size')[0]; ?>" alt="<?php echo $b1_campaign; ?>"/>
							<?php
						}
						else
						{
							?>
								<video class="left-0 bottom-0" src="<?php echo wp_get_attachment_url($b1_image2['video']); ?>" playsinline muted autoplay loop></video>
							<?php
						}
					?>
				</div>

				<img class="show-mobile" style="width:100%!important; position:absolute!important; top:0px!important; left:0px!important;" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink-top.svg" alt=""/>
				<img class="show-desktop background--absolute left-0 bottom-0" style="width:100%" src="/wp-content/themes/t2m/assets/images/case-studies/display-ads/corner-pink.svg" alt=""/>
			</div>

		</section>