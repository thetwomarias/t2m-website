<?php
/*
Template Name: Contact Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed"><span><?php _e('Scroll','t2m'); ?></span></div>
  <div class="page-header__container">
    <h1 class="page-header__title"><?php the_field('page_header_title'); ?></h1>
    <p class="page-header__subheading appear"><?php the_field('page_header_subheading'); ?></p>
  </div>
</section>

<section class="contact-details">

  <div class="contact-details--left smallsize">
    <div class="contact-details__image appear">
    <?php

        $detailsLeftPlaceholder = get_field('details_left_image_placeholder');
        $detailsLeftImage = get_field('details_left_image');

        if( !empty($detailsLeftPlaceholder) ): ?>
          <img class="contact-details__image--placeholder" src="<?php echo $detailsLeftPlaceholder['url']; ?>" alt="<?php echo $detailsLeftPlaceholder['alt']; ?>" title="<?php echo $detailsLeftPlaceholder['title']; ?>"/>
        <?php endif; ?>

        <?php
        if( !empty($detailsLeftImage) ): ?>
          <object type="image/svg+xml" data="<?php echo $detailsLeftImage['url']; ?>" preserveAspectRatio="xMidYMid meet">
            <img src="<?php echo $detailsLeftImage['url']; ?>" alt="<?php echo $detailsLeftImage['alt']; ?>" description="<?php echo $detailsLeftImage['description']; ?>" />
          </object>
        <?php endif; ?>
    </div>

  </div>

  <div class="contact-details--right appear">
  	<?php while( have_rows('details_left_block') ): the_row();
        $detailsLeftHeading = get_sub_field('details_left_heading');
        $detailsLeftEmail = get_sub_field('details_left_email');
        $detailsLeftIntro = get_sub_field('details_left_info');
      ?>

        <h2 class="contact-details__heading"><?php echo $detailsLeftHeading; ?></h2>
        <p class="contact-details__info"><a href="mailto:<?php echo $detailsLeftEmail; ?>"><span><?php echo $detailsLeftEmail; ?></span></a><?php echo $detailsLeftIntro; ?></p>

      <?php endwhile; ?>

    </div>

</section>

<section class="contact-details">

 	<div class="contact-details--left fullsize">
	    <div class="contact-details__image appear">

	    <?php if( have_rows('details_left_block') ): ?>

	    <div class="contact-details__text">

		<?php
		  $contactMiddleText = get_field('contact_middle_text');
		  if( !empty($contactMiddleText) ): ?>

		    <section class="contact-details--center">

		      <div><?php echo $contactMiddleText; ?></div>

		    </section>

		    <?php endif; ?>

	    </div>

	    <?php endif; ?>

		</div>
	</div>

</section>

<?php if( have_rows('locations') ): ?>

<section class="contact-locations">

<?php

	while( have_rows('locations') ): the_row();
        $locationImage = get_sub_field('location_image');
        $locationImageMobile = get_sub_field('location_image_mobile');
        $locationName = get_sub_field('location_name');
        $locationAddress = get_sub_field('location_address');
        $locationLinkText = get_sub_field('location_link_text');
        $locationLinkUrl = get_sub_field('location_link_url');
        $parollerFactor = get_sub_field('paroller_factor');
        $locationAlignment = get_sub_field('alignment');
        $active = get_sub_field('active');

        if($active)
        {
		      ?>

		  <div class="location location--<?php if( $locationAlignment == 'left' ): ?>left<?php endif; ?><?php if( $locationAlignment == 'right' ): ?>right<?php endif; ?>" data-paroller-factor="<?php echo $parollerFactor; ?>" data-paroller-type="foreground" data-paroller-direction="vertical">
		    <div class="location__image">
		      <img class="hide-sm hide-md" src="<?php echo $locationImage['url']; ?>" alt="<?php echo $locationImage['alt']; ?>" title="<?php echo $locationImage['title']; ?>"/>
		      <img class="show-sm show-md hide-lg" src="<?php echo $locationImageMobile['url']; ?>" alt="<?php echo $locationImageMobile['alt']; ?>" title="<?php echo $locationImageMobile['title']; ?>"/>
		    </div>
		    <?php
		    $emptyText = 1;
		    if(!empty($locationName) || !empty($locationLinkText))
		    	$emptyText = 0;

		    	?>
			    <div class="location__text <?php if($emptyText)echo 'emptyText'; ?>">
			      <h3 class="location__name"><?php echo $locationName; ?></h3>
			      <address class="location__address"><?php echo $locationAddress; ?></address>
			      <?php if( !empty($locationLinkUrl) && !empty($locationLinkText) ): ?>
			      	<a class="link--cta" href="<?php echo $locationLinkUrl; ?>" target="_blank"><span><?php echo $locationLinkText; ?></span></a>
			      <?php endif; ?>
			    </div>
			   <?php
			?>
		  </div>

	  <?php
	  }

	endwhile;  ?>

</section>

<?php endif; ?>




<?php
get_footer();
