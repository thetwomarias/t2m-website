<?php
/*
Template Name: 404 Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed"><span><?php _e('Scroll','t2m'); ?></span></div>
  <div class="page-header__container">
    <h1 class="page-header__title"><?php the_field('page_header_404', false, true); ?></h1>
    <h2 class="page-header__subheading appear"><?php the_field('page_subheading_404', false, true); ?></h2>
    <?php if( !empty(the_field('404_link_text')) ): ?>
      <a class="link--cta" href="<?php the_field('404_link_url'); ?>"><span><?php the_field('404_link_text'); ?></span></a>
    <?php endif; ?>

  </div>
</section>


<?php
get_footer();
