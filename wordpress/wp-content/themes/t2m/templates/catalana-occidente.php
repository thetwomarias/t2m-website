<?php
/*
Template Name: Case Study Catalana Occidente
*/

	get_header();

      /* Case Study Info */
	      $caseStudyTitle = get_field('case_study_title');
	      $caseStudyHero = get_field('case_study_hero_image');

	      $caseStudyIndustry = get_field('case_study_industry');
	      $caseStudyTechnology = get_field('case_study_technology');
	      $caseStudyDevelopment = get_field('case_study_development');
	      $caseStudyBrief = get_field('case_study_brief');
	      $caseStudyLinkText = get_field('case_study_brief_link_text');
	      $caseStudyLinkUrl = get_field('case_study_brief_link_url');

	      $caseStudyVideoOne = get_field('case_study_video_one');
	      $caseStudyVideoOnePoster = get_field('case_study_video_one_poster');
	      $caseStudyVideoOneMobile = get_field('case_study_video_one_mobile');

	      $caseStudyVideoTwo = get_field('case_study_video_two');
	      $caseStudyVideoTwoPoster = get_field('case_study_video_two_poster');
	      $caseStudyVideoTwoMobile = get_field('case_study_video_two_mobile');

	      $caseMultipleImagesOne = get_field('case_study_multiple_images_one');
	      $caseMultipleImagesTwo = get_field('case_study_multiple_images_two');

	      $caseImageOne = get_field('case_study_image_one');
	      $caseImageTwo = get_field('case_study_image_two');
	      $caseImageThree = get_field('case_study_image_three');
	      $caseImageFour = get_field('case_study_image_four');

	      $quotes = get_field('quotes'); // Testimonials

      /* Next Case Study */
	      $nextCaseStudyId = get_field('case_study_next_case_study');
	      if(isset($nextCaseStudyId) && $nextCaseStudyId!=0)
	      {
		      $nextCaseTitle = get_field('next_case_study_title',$nextCaseStudyId);
		      $nextCaseDescription = get_field('next_case_study_description',$nextCaseStudyId);
		      $nextCaseImage = get_field('next_case_study_image',$nextCaseStudyId);
		      $nextCaseLinkText = get_field('next_case_study_link_text',$nextCaseStudyId);
		      $nextCaseLinkUrl = get_the_permalink($nextCaseStudyId);
		   }
?>

<?php

	$class = $classMobile = $background = "";

	// Header
		// $class = "background--purple";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/header.php';


	// Video One
		require_once get_stylesheet_directory() . '/templates/case-study-parts/catalana/video-one.php';

	// Image One
		// $class = "appear case-study__block case-study__image background--transparent visible";
		// require_once get_stylesheet_directory() . '/templates/case-study-parts/catalana/image-one.php';

	// Multiples Images One
		require_once get_stylesheet_directory() . '/templates/case-study-parts/catalana/multiples-img-one.php';

	// Testimonials
		$class = "testimonials--pink";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/testimonials.php';

	// Image Two
		$class = "appear case-study__image appear background--purple";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/image-two.php';

	// Image Three
		$class = "appear case-study__image appear background--purple";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/image-three.php';

	// Image Four
		// $class = "appear case-study__block case-study__image background--transparent";
		// require_once get_stylesheet_directory() . '/templates/case-study-parts/image-four.php';

	// Video Two
		require_once get_stylesheet_directory() . '/templates/case-study-parts/catalana/video-two.php';



	// Video block
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-block.php';

	// CTA
		require_once get_stylesheet_directory() . '/templates/case-study-parts/cta.php';

	// Next Case Study
		require_once get_stylesheet_directory() . '/templates/case-study-parts/next-case-study.php';

?>

<?php
	get_footer();
?>
