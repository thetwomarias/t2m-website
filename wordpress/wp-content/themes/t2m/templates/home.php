<?php
/*
Template Name: Home Template
*/

	get_header();

?>
<style type="text/css">
	<?php
		if( have_rows('projects') )
		{
			$i = 0;
			while( have_rows('projects') ): the_row();
				$idProject = get_sub_field('project');
				$projectId = get_field('project_id',$idProject);
				$projectImage = get_field('slider_project_image',$idProject);
				$projectImageMobile = get_field('slider_project_image_mobile',$idProject);
				// $projectBackground = get_field('slider_project_background',$idProject);
				$projectActive = get_sub_field('active');
				if( $projectActive ) {
					?>
						.home-project-<?php echo $projectId; ?>{background-image: url("<?php echo $projectImage['url']; ?>");}
						@media (max-width: 576px) {
							.home-project-<?php echo $projectId; ?>{background-image: url("<?php echo $projectImageMobile['url']; ?>");}
						}
					<?php
				}

			endwhile;
		}
	?>
</style>

<!-- Header -->
	<section class="page-header">
		<div class="scroll-btn scroll fixed appear appear-only"><span><?php _e('Scroll','t2m'); ?></span></div>
		<div class="page-header__container">
			<h1 class="page-header__title appear"><?php the_field('page_header_title'); ?></h1>
			<h2 class="page-header__subtitle appear">
				<?php

				$currentlyWorking = get_field('currently_working');
				$currentWorkingLinkId = get_field('currently_working_link');

				if(!empty($currentWorkingLinkId))
				{
		      		$currentWorkingHasCaseStudy = get_field('has_case_study',$currentWorkingLinkId);

					$currentWorkingLink = get_the_permalink($currentWorkingLinkId);
					if(!$currentWorkingHasCaseStudy)
					{
						$currentWorkingId = get_field('project_id',$currentWorkingLinkId);
						$currentWorkingLink = get_the_permalink(wp_get_post_parent_id($currentWorkingLinkId))."#".$currentWorkingId;
					}
				}

				$class = "";
				$strongVal = "";
				if(!isset($currentWorkingLink) || $currentWorkingLink=='')
				{
					$currentWorkingLink = "javascript:;";
					$class = "no-link";
				}
				if(isset($currentlyWorking) && $currentlyWorking!='')
					$strongVal = "<strong>".$currentlyWorking."</strong>";
				?>
				<a class="<?php echo $class; ?>" href="<?php echo $currentWorkingLink; ?>"><span class="icon-dash-line"></span><span class="currently appear appear-ltr"><?php the_field('page_header_subheading'); ?><?php echo $strongVal; ?></span></a>
			</h2>
		</div>
	</section>


<!-- Projects -->

	<section class="home-projects" id="home-projects">
		<?php if( have_rows('projects') ): ?>
			<div class="home-projects__slider">
				<?php
				$i = 0;
				while( have_rows('projects') ): the_row();
					$next = "";
					if($i==1) // Second slide
						$next = "next";

					$idProject = get_sub_field('project');
					$projectActive = get_sub_field('active');

					$projectId = get_field('project_id',$idProject);
	      			$projectHasCaseStudy = get_field('has_case_study',$idProject);

					$projectLinkUrl = get_the_permalink($idProject);
					if(!$projectHasCaseStudy)
						$projectLinkUrl = get_the_permalink(wp_get_post_parent_id($idProject))."#".$projectId;

					$projectName = get_field('slider_project_name',$idProject);
					$projectDescription = get_field('slider_project_description',$idProject);
					$projectLinkText = get_field('slider_project_link_text',$idProject);
					$projectImageType = get_field('slider_project_image_type',$idProject);
					$projectImage = get_field('slider_project_image',$idProject);
					$projectImageMobile = get_field('slider_project_image_mobile',$idProject);
					$projectTextColor = get_field('slider_project_text_color',$idProject);
					$projectBgColor = get_field('slider_project_background',$idProject);


					$imageType = "";
					if($projectImageType>0)
						$imageType = "fullsize";

					?>
					<?php if( $projectActive ): ?>
						<div <?php if( isset($projectBgColor) && $projectBgColor!=''){ echo 'style="background-color:'.$projectBgColor.';"'; } ?> class="home-projects__item home-projects__item--<?php if( $projectTextColor == 'blue' ): ?>blue<?php endif; ?><?php if( $projectTextColor == 'white' ): ?>white<?php endif; ?> appear home-project-<?php echo $projectId; ?> <?php echo $next; ?> <?php echo $imageType; ?>" id="home-project-<?php echo $projectId; ?>" data-vp-offset="10">
							<a class="link--invisible home-project-<?php echo $projectId; ?>-link" href="<?php echo $projectLinkUrl; ?>"></a>
							<h3 class="home-projects__title"><?php echo $projectName; ?></h3>
							<div class="home-projects__desc"><?php echo $projectDescription; ?></div>
							<div class="home-projects__link">
								<a href="<?php echo $projectLinkUrl; ?>" class="link--latest link--cta home-project-<?php echo $projectId; ?>-link"><span><?php echo $projectLinkText; ?></span></a>
							</div>
						</div>
					<?php endif; ?>
					<?php
					$i++;
				endwhile; ?>
			</div>
		<?php endif; ?>

		<div class="red-triangle-wrapper">
			<img class="red-triangle" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-red.svg" alt="Red Corner" />
		</div>
	</section>

<!-- Services -->
	<div id="home-services-wrapper">
		<section class="home-services appear">
			<div class="home-services__block">
				<?php if( have_rows('services') ): ?>
					<ul class="home-services__list">
						<?php while( have_rows('services') ): the_row();
							$serviceName = get_sub_field('service_name');
							$serviceLink = get_sub_field('service_link');
							?>
							<li class="home-services__item">
								<a href="<?php echo $serviceLink; ?>" class="home-services__link">
									<span><?php echo $serviceName; ?></span>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="home-services__block">
				<div class="home-services__text">
					<?php echo get_field('the_two_marias_description'); ?>
				</div>
			</div>
		</section>
	</div>

<!-- Clients -->

	<section class="clients appear">
		<h5 class="clients__heading"><?php _e('Clients','t2m'); ?></h5>
		<div class="clients__wrapper pad">
			<?php
			$query = new WP_Query(array(
				'post_type' => 'client',
				'post_status' => 'publish',
				'posts_per_page' => '20'
			));
			while ($query->have_posts()) {
				$query->the_post();
				$post_id = get_the_ID();
				$clientName = get_the_title();
				$clientImage = get_field('client_image');
				?>
				<div class="client">
					<img class="client__logo lazy" data-src="<?php echo $clientImage['url']; ?>" alt="<?php echo $clientName; ?>" title="<?php echo $clientName; ?>" width="<?php echo $clientImage['width']; ?>" height="<?php echo $clientImage['height']; ?>" />
				</div>
				<?php
			}
			wp_reset_query();
			?>
		</div>


		<!-- Testimonials -->

			<div class="testimonials-cta">
				<div class="home-testimonials__wrapper">
					<div class="home-testimonials__text">
						<p class="home-testimonials__heading"><?php the_field('testimonials_claim'); ?></p>
						<a id="testimonials-home" href="javascript:;" class="home-testimonials__link link--cta"><span><?php the_field('testimonials_link_text'); ?></span></a>
					</div>
				</div>
			</div>

			<section id="testimonials" class="testimonials-block bottom-block">

				<div class="bottom-block__wrapper">

					<img class="lazy" data-src="/wp-content/themes/t2m/assets/images/backgrounds/quotes.svg" alt="Testimonials" title="Testimonials" />

					<div class="testimonials__slider">

						<?php
						$query = new WP_Query(
							array(
								'post_type' => 'testimonial',
								'post_status' => 'publish',
								'posts_per_page' => 8,
								'meta_query' => array(
									array(
									      'key' => 'hide_in_projects',
									      'value' => 1,
									      'compare' => '!='
							      	),
								)
							)
						);

						while ($query->have_posts()) {
							$query->the_post();
							$post_id = get_the_ID();
							$testimonialQuote = get_field('testimonial_quote');
							$testimonialAuthor = get_field('testimonial_author');
							$testimonialRole = get_field('testimonial_role');
							$testimonialHide = get_field('hide_in_projects');

							// if(!$testimonialHide)
							// {
								?>

								<div class="testimonials__slide">
									<h5 class="testimonials__quote appear"><?php echo $testimonialQuote ;?></h5>
									<div class="testimonials__credits appear">
										<span class="testimonials__name"><?php echo $testimonialAuthor ;?></span>
										<span class="testimonials__position"><?php echo $testimonialRole ;?></span>
									</div>
								</div>

								<?php
							// }
						}

						wp_reset_query();
						?>

					</div>

				</div>

			</section>

	</section>

<!-- Mariismos / Mantras -->

	<section class="home-mariismos">
		<h4 class="mariismos__title hide-lg show-sm" style="text-align: center; letter-spacing: 2px; font-size: 14px; padding-top: 6%; font-weight:700;"><?php _e('Our mantras','t2m'); ?></h4>
		<?php if( have_rows('mantras') ): ?>
			<div class="home-mariismos__slider">
				<?php while( have_rows('mantras') ): the_row();
					$mantrasTitle = get_sub_field('mantras_title');
					$mantrasDescription = get_sub_field('mantras_description');
					$mantrasImage = get_sub_field('mantras_image');
					$mantrasName = get_sub_field('mantras_name');
					?>
					<div class="mariismos__slide mariismos-<?php echo $mantrasName; ?>">
						<div class="mariismos__image">
							<img class="lazy" data-src="<?php echo $mantrasImage['url']; ?>" title="<?php echo $mantrasImage['title']; ?>" alt="<?php echo $mantrasImage['alt']; ?>"/>
						</div>
						<div class="mariismos__text">
							<h5 class="mariismos__title hide-sm"><?php _e('Our mantras','t2m'); ?></h5>
							<h4 class="mariismos__heading"><?php echo $mantrasTitle; ?></h4>
							<p class="mariismos_desc"><?php echo $mantrasDescription; ?></p>
						</div>
					</div>
				<?php endwhile; ?>
				<span class="pagingInfo"></span>
			</div>
		<?php endif; ?>
	</section>

<!-- Team -->

	<section class="home-team">
		<div class="home-team__wrapper">

		<?php

			// if ( !wp_is_mobile() ) {
				?>
						<div class="home-team__text">
							<?php
							$gtnuTitle = get_field('get_to_know_us_title');
							$gtnuText = get_field('get_to_know_us_text');
							$gtnuLinkLabel = get_field('get_to_know_link_text');
							$gtnuLink = get_field('get_to_know_link_url');

							if(isset($gtnuTitle) && !empty($gtnuTitle)){
								?>
									<h4 cl ss="appear"><?php echo $gtnuTitle; ?></h4>
								<?php
							}
							if(isset($gtnuText) && !empty($gtnuText)){
								?>
									<p class="appear"><?php echo $gtnuText; ?></p>
								<?php
							}
							if(isset($gtnuLinkLabel) && $gtnuLinkLabel!='' && isset($gtnuLink) && $gtnuLink!='')
							{
								?>
									<a href="<?php echo $gtnuLink; ?>" class="link--cta appear"><span><?php echo $gtnuLinkLabel; ?></span></a>
								<?php
							}
							?>
						</div>
						<div class="home-team__image">
							<?php
							$getToKnowUsVimeo = get_field('get_to_know_us_vimeo'); // EN - 750244317?h=9fba662fd1 // ES - 751191822?h=ca43378956 // CA - 751190535?h=395d7bd99e
							$getToKnowUsImage = get_field('get_to_know_us_image');

							if( !empty($getToKnowUsVimeo) ):
								?>
									<div class='embed-container'>
										<?php
											$classVideo = "";
											if( !empty($getToKnowUsImage) ):
												$classVideo= "hidden-video";
												?>
													<a id="play-team-video" href="javascript:;" class="play"></a>
													<div class="poster-container">
														<img id="team-poster-image" class="lazy" data-src="<?php echo $getToKnowUsImage['url']; ?>" alt="<?php echo $getToKnowUsImage['alt']; ?>" title="<?php echo $getToKnowUsImage['title']; ?>" />
													</div>
												<?php
											endif;
										?>
										<iframe class="<?php echo $classVideo; ?>" id="team-video" src='https://player.vimeo.com/video/<?php echo $getToKnowUsVimeo; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen allow="autoplay" ></iframe>
									</div>
								<?php
							elseif( !empty($getToKnowUsImage) ):
								?>
									<img class="lazy" data-src="<?php echo $getToKnowUsImage['url']; ?>" alt="<?php echo $getToKnowUsImage['alt']; ?>" title="<?php echo $getToKnowUsImage['title']; ?>" />
								<?php
							endif;
							?>
						</div>
					<?php
				// }
			?>
		</div>
	</section>


<!-- CTA -->

	<section class="home-bottom bottom-block">
		<div class="bottom-block__wrapper"  style="background-color:<?php the_field('bottom_background_color'); ?>">
			<div class="bottom-block__content">
				<?php
				$bottomImageLeft = get_field('bottom_svg_left');

				if( !empty($bottomImageLeft) ): ?>
					<img class="lazy" data-src="<?php echo $bottomImageLeft['url']; ?>" alt="<?php echo $bottomImageLeft['alt']; ?>" title="<?php echo $bottomImageLeft['title']; ?>" />
				<?php endif; ?>

				<div class="bottom-block__text">
					<h6 class="bottom-block__heading"><?php the_field('bottom_title'); ?></h6>
					<div class="bottom-block__desc"><?php the_field('bottom_description'); ?></div>
				</div>

				<div class="bottom-block__image">
					<?php
					$bottomImageRight = get_field('bottom_image_right');
					$bottomSvgRight = get_field('bottom_svg_right');
					$bottomSvgRightMobile = get_field('bottom_svg_right_mobile');
					if( !empty($bottomSvgRightMobile) ): ?>
						<object type="image/svg+xml" data="<?php echo $bottomSvgRightMobile ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="show-sm show-md hide-lg">
							<img class="lazy" data-src="<?php echo $bottomSvgRightMobile ['url']; ?>" alt="<?php echo $bottomSvgRightMobile ['alt']; ?>" title="<?php echo $bottomSvgRightMobile ['title']; ?>" class="hide-mobile show-desktop" />
						</object>
					<?php endif; ?>

					<?php if( !empty($bottomSvgRight) ): ?>
						<object type="image/svg+xml" data="<?php echo $bottomSvgRight ['url']; ?>" preserveAspectRatio="xMidYMid meet" class="hide-sm hide-md">
							<img class="lazy" data-src="<?php echo $bottomImageRight ['url']; ?>" alt="<?php echo $bottomImageRight ['alt']?>" title="<?php echo $bottomImageRight ['title']?>" class="hide-mobile show-desktop" />
						</object>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>


<?php

	get_footer();
