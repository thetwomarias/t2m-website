<?php
/*
Template Name: Case Study Honda
*/

	get_header();

      /* Case Study Info */
	      $caseStudyTitle = get_field('case_study_title');
	      $caseStudyHero = get_field('case_study_hero_image');

	      $caseStudyIndustry = get_field('case_study_industry');
	      $caseStudyTechnology = get_field('case_study_technology');
	      $caseStudyDevelopment = get_field('case_study_development');
	      $caseStudyBrief = get_field('case_study_brief');
	      $caseStudyLinkText = get_field('case_study_brief_link_text');
	      $caseStudyLinkUrl = get_field('case_study_brief_link_url');

	      $caseStudyVideoOne = get_field('case_study_video_one');
	      $caseStudyVideoOnePoster = get_field('case_study_video_one_poster');
	      $caseStudyVideoOneMobile = get_field('case_study_video_one_mobile');

	      $caseMobileSlider = get_field('case_study_mobile_slider');

	      $caseMultipleImagesOne = get_field('case_study_multiple_images_one');
	      // $caseMultipleImagesTwo = get_field('case_study_multiple_images_two');

	      $caseIcons = get_field('case_study_icons');

	      $caseImageOne = get_field('case_study_image_one');
	      $caseImageTwo = get_field('case_study_image_two');
	      // $caseImageThree = get_field('case_study_image_three');

	      $quotes = get_field('quotes'); // Testimonials


      /* CTA */
	      // $caseCtaImage = get_field('case_study_cta_image');
	      // $caseCtaTitle = get_field('case_study_cta_title');
	      // $caseCtaDescription = get_field('case_study_cta_description');
	      // $caseCtaLinkText = get_field('case_study_cta_link_text');
	      // $caseCtaLinkUrl = get_field('case_study_cta_link_url');

      /* Next Case Study */
	      $nextCaseStudyId = get_field('case_study_next_case_study');

	      $nextCaseTitle = get_field('next_case_study_title',$nextCaseStudyId);
	      $nextCaseDescription = get_field('next_case_study_description',$nextCaseStudyId);
	      $nextCaseImage = get_field('next_case_study_image',$nextCaseStudyId);
	      $nextCaseLinkText = get_field('next_case_study_link_text',$nextCaseStudyId);
	      $nextCaseLinkUrl = get_the_permalink($nextCaseStudyId);
?>

<?php

	$class = "";

	// Header
		require_once get_stylesheet_directory() . '/templates/case-study-parts/header.php';



	// Video One
		require_once get_stylesheet_directory() . '/templates/case-study-parts/honda/video-one.php';

	// Multiples Images One
		require_once get_stylesheet_directory() . '/templates/case-study-parts/honda/multiples-img-one.php';

	// Testimonials
		$class = "";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/testimonials.php';

	// 2 Images - One over the other and with lateral margins
		$class = "case-study__image background--pink-white-triangle case-study__lateral-images";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/two-images-lateral-margin.php';




	// Video block
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-block.php';

	// CTA
		require_once get_stylesheet_directory() . '/templates/case-study-parts/cta.php';

	// Next Case Study
		require_once get_stylesheet_directory() . '/templates/case-study-parts/next-case-study.php';
 ?>

<?php
	get_footer();
?>
