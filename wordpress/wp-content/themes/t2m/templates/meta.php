<?php
/*
Template Name: Case Study Meta
*/
	get_header();

      /* Case Study Info */
	      $caseStudyTitle = get_field('case_study_title');
	      $caseStudyHero = get_field('case_study_hero_image');

	      $caseStudyIndustry = get_field('case_study_industry');
	      $caseStudyTechnology = get_field('case_study_technology');
	      $caseStudyDevelopment = get_field('case_study_development');
	      $caseStudyBrief = get_field('case_study_brief');
	      $caseStudyLinkText = get_field('case_study_brief_link_text');
	      $caseStudyLinkUrl = get_field('case_study_brief_link_url');

	      $caseLogos = get_field('case_study_description_logos');

	      $quotes = get_field('quotes'); // Testimonials

		 /* Next Case Study */
	      $nextCaseStudyId = get_field('case_study_next_case_study');
	      if(isset($nextCaseStudyId) && $nextCaseStudyId!=0)
	      {
		      $nextCaseTitle = get_field('next_case_study_title',$nextCaseStudyId);
		      $nextCaseDescription = get_field('next_case_study_description',$nextCaseStudyId);
		      $nextCaseImage = get_field('next_case_study_image',$nextCaseStudyId);
		      $nextCaseLinkText = get_field('next_case_study_link_text',$nextCaseStudyId);
		      $nextCaseLinkUrl = get_the_permalink($nextCaseStudyId);
		   }
?>
<?php

	$class = $classMobile = $background = "";

	// Header
		require_once get_stylesheet_directory() . '/templates/case-study-parts/header.php';

	// Projects Blocks

		// B0 - Hero
			require_once get_stylesheet_directory() . '/templates/meta-projects/b0-hero.php';
		// B5 - Holiday
			require_once get_stylesheet_directory() . '/templates/meta-projects/b5-holiday.php';
		// B1 - Ramadan
			require_once get_stylesheet_directory() . '/templates/meta-projects/b1-ramadan.php';
		// B2 - 100 Days of mobile campaign
			require_once get_stylesheet_directory() . '/templates/meta-projects/b2-100-days-of-mobile.php';
		// B3 - Workplace
			require_once get_stylesheet_directory() . '/templates/meta-projects/b3-workplace.php';
		// B4 - Disco Holiday
			// require_once get_stylesheet_directory() . '/templates/meta-projects/b4-discoholiday.php';


	// Testimonials
		$class = "testimonials--pink quotes-purple";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/testimonials.php';

	// Video block
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-block.php';

	// CTA
		require_once get_stylesheet_directory() . '/templates/case-study-parts/cta.php';

	// Next Case Study
		require_once get_stylesheet_directory() . '/templates/case-study-parts/next-case-study.php';

?>

<?php
	get_footer();
?>