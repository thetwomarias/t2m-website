<?php
/*
Template Name: Case Study SC
*/

	get_header();

      /* Case Study Info */
	      $caseStudyTitle = get_field('case_study_title');
	      $caseStudyHero = get_field('case_study_hero_image');

	      $caseStudyIndustry = get_field('case_study_industry');
	      $caseStudyTechnology = get_field('case_study_technology');
	      $caseStudyDevelopment = get_field('case_study_development');
	      $caseStudyBrief = get_field('case_study_brief');
	      $caseStudyLinkText = get_field('case_study_brief_link_text');
	      $caseStudyLinkUrl = get_field('case_study_brief_link_url');

	      $caseStudyVideoOne = get_field('case_study_video_one');
	      $caseStudyVideoOnePoster = get_field('case_study_video_one_poster');
	      $caseStudyVideoOneMobile = get_field('case_study_video_one_mobile');

	      $caseMobileSlider = get_field('case_study_mobile_slider');

	      $caseMultipleImagesOne = get_field('case_study_multiple_images_one');
	      $caseMultipleImagesTwo = get_field('case_study_multiple_images_two');

	      $caseIcons = get_field('case_study_icons');

	      $caseImageOne = get_field('case_study_image_one');
	      // $caseImageTwo = get_field('case_study_image_two');
	      // $caseImageThree = get_field('case_study_image_three');

	      $quotes = get_field('quotes'); // Testimonials


      /* CTA */
	      // $caseCtaImage = get_field('case_study_cta_image');
	      // $caseCtaTitle = get_field('case_study_cta_title');
	      // $caseCtaDescription = get_field('case_study_cta_description');
	      // $caseCtaLinkText = get_field('case_study_cta_link_text');
	      // $caseCtaLinkUrl = get_field('case_study_cta_link_url');

      /* Next Case Study */
	      $nextCaseStudyId = get_field('case_study_next_case_study');

	      $nextCaseTitle = get_field('next_case_study_title',$nextCaseStudyId);
	      $nextCaseDescription = get_field('next_case_study_description',$nextCaseStudyId);
	      $nextCaseImage = get_field('next_case_study_image',$nextCaseStudyId);
	      $nextCaseLinkText = get_field('next_case_study_link_text',$nextCaseStudyId);
	      $nextCaseLinkUrl = get_the_permalink($nextCaseStudyId);
?>

<?php

	$class = $classMobile = $background = "";

	// Header
		require_once get_stylesheet_directory() . '/templates/case-study-parts/header.php';



	// Video One
		$class = "case-study__video background--purple appear hide-sm hide-md";
		$classMobile = "case-study__video background--purple appear show-sm show-md hide-lg padding-sm left-35 right-35";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-one.php';

	// Slider
		require_once get_stylesheet_directory() . '/templates/case-study-parts/slider.php';

	// Testimonials
		$class = "";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/testimonials.php';

	// Multiples Images One
		$class = "case-study__block background--pink bottom-20";
		$background = "corner-purple.svg";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/multiples-img-one.php';

	// Icons
		require_once get_stylesheet_directory() . '/templates/case-study-parts/icons.php';

	// Image One
		$class = "case-study__image";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/image-one.php';

	// Multiples Images Two
		require_once get_stylesheet_directory() . '/templates/case-study-parts/multiples-img-two.php';



	// Video block
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-block.php';

	// CTA
		require_once get_stylesheet_directory() . '/templates/case-study-parts/cta.php';

	// Next Case Study
		require_once get_stylesheet_directory() . '/templates/case-study-parts/next-case-study.php';
?>

<?php
	get_footer();
?>
