<?php
/*
Template Name: Case Study Display Ads
*/
	get_header();

      /* Case Study Info */
	      $caseStudyTitle = get_field('case_study_title');
	      $caseStudyHero = get_field('case_study_hero_image');

	      $caseStudyIndustry = get_field('case_study_industry');
	      $caseStudyTechnology = get_field('case_study_technology');
	      $caseStudyDevelopment = get_field('case_study_development');
	      $caseStudyBrief = get_field('case_study_brief');
	      $caseStudyLinkText = get_field('case_study_brief_link_text');
	      $caseStudyLinkUrl = get_field('case_study_brief_link_url');

	      $caseLogos = get_field('case_study_description_logos');

	      $quotes = get_field('quotes'); // Testimonials

 /* Next Case Study */
	      $nextCaseStudyId = get_field('case_study_next_case_study');
	      if(isset($nextCaseStudyId) && $nextCaseStudyId!=0)
	      {
		      $nextCaseTitle = get_field('next_case_study_title',$nextCaseStudyId);
		      $nextCaseDescription = get_field('next_case_study_description',$nextCaseStudyId);
		      $nextCaseImage = get_field('next_case_study_image',$nextCaseStudyId);
		      $nextCaseLinkText = get_field('next_case_study_link_text',$nextCaseStudyId);
		      $nextCaseLinkUrl = get_the_permalink($nextCaseStudyId);
		   }
?>
<?php

	$class = $classMobile = $background = "";

	// Header
		require_once get_stylesheet_directory() . '/templates/case-study-parts/header.php';



	// Projects Blocks

		// B7 - Vimto, Kettle Chips
			require_once get_stylesheet_directory() . '/templates/display-ads-projects/b7-vimto.php';
		// B6 - Patek Philippe
			require_once get_stylesheet_directory() . '/templates/display-ads-projects/b6-patek-philippe.php';
		// B3 - Virgin
			require_once get_stylesheet_directory() . '/templates/display-ads-projects/b3-virgin.php';
		// B4 - Barclays
			require_once get_stylesheet_directory() . '/templates/display-ads-projects/b4-barclays.php';
		// B2 - Gohenry
			require_once get_stylesheet_directory() . '/templates/display-ads-projects/b2-gohenry.php';

	// Old Projects Blocks

		// B5 - Lipton
			// require_once get_stylesheet_directory() . '/templates/display-ads-projects/b5-lipton.php';
		// B4 - Water Wipes
			// require_once get_stylesheet_directory() . '/templates/display-ads-projects/b4-water-wipes.php';
		// B5 - M&G
			// require_once get_stylesheet_directory() . '/templates/display-ads-projects/b5-mg.php';
		// B1 - Facebook (old B5 - M&G)
			// require_once get_stylesheet_directory() . '/templates/display-ads-projects/b1-facebook.php';

	// Testimonials
		// $class = "testimonials--no-tp";
		require_once get_stylesheet_directory() . '/templates/case-study-parts/testimonials.php';

	// Video block
		require_once get_stylesheet_directory() . '/templates/case-study-parts/video-block.php';

	// CTA
		require_once get_stylesheet_directory() . '/templates/case-study-parts/cta.php';

	// Next Case Study
		require_once get_stylesheet_directory() . '/templates/case-study-parts/next-case-study.php';

?>

<?php
	get_footer();
?>
