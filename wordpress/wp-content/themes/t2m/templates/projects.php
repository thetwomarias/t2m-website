<?php
/*
Template Name: Projects Template
*/

get_header();

?>

<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/scripts/projects-videos.js'></script>

<section class="page-header">
  <div class="scroll-btn scroll fixed appear appear-only"><span><?php _e('Scroll','t2m'); ?></span></div>
    <div class="page-header__container">
    <h1 class="page-header__title appear"><?php the_field('page_header_title'); ?></h1>
  </div>
</section>

<!-- Projects -->

	<?php
	    $args = array(
		    'post_parent'    => $post->ID,
		    'post_type'      => 'page',
		    'post_status'	 => 'publish',
		    'order'          => 'ASC',
		    'orderby'        => 'menu_order',
		    'posts_per_page' => -1,
		    'suppress_filters' => false
		 );
	    $query = new WP_Query($args);

	    while ($query->have_posts()) {

		    $query->the_post();
		    $post_id = get_the_ID();
		    $projectId = get_field('project_id');
		    $projectHasCaseStudy = get_field('has_case_study');
		    $projectTitle = str_replace(array('<p>','</p>'),'', get_field('list_project_title'));
		    $projectLinkT = get_the_permalink();
		    $projectSubtitle = str_replace(array('<p>','</p>'),'', get_field('list_project_subtitle'));
		    $projectDescription = str_replace(array('<p>','</p>'),'', get_field('list_project_description'));
		    $projectLinkText = get_field('list_project_link_text');
		    $projectLinkUrl = get_the_permalink(	);
		    $projectAlign = get_field('list_project_alignment');
		    $projectType = get_field('list_project_type');
		    $parollerFactor = get_field('list_paroller_factor');
		    $list_project_videos = get_field('list_project_videos');
		    $list_project_images = get_field('list_project_images');

			?>

			<section class="project-block project-block--<?php echo $projectAlign;?> project-block--<?php echo $projectType ?>" id="<?php echo $projectId ;?>" data-paroller-factor="<?php echo $parollerFactor; ?>" data-paroller-type="foreground" data-paroller-direction="vertical">

				<div class="project__text appear">
					<h4 class="project__title"><?php echo $projectTitle; ?></h4>
					<h2 class="project__heading"><?php echo $projectSubtitle; ?></h2>
					<div class="project__desc"><?php echo $projectDescription; ?></div>
					<div class="project__dots"></div>
					<?php if( $projectHasCaseStudy && $projectLinkText ) { ?>
						<a class="link--cta" href="<?php echo $projectLinkUrl; ?>"><span><?php echo $projectLinkText; ?></span></a>
					<?php } ?>
				</div>

				<?php if( !empty($list_project_images) && count($list_project_images)>0 ): ?>

					<div class="project__slider appear">

						<?php

						foreach( $list_project_images as $current) {
							$projectImage = $current['project_image'];
							$projectImageMobile = $current['project_image_mobile'];
							?>

							<div class="project__slide project__slide--boxed">
								<?php if( $projectHasCaseStudy && $projectLinkText ) { ?>
									<a href="<?php echo $projectLinkUrl; ?>" class="link--inner"></a>
								<?php } ?>
								<picture>
									<source srcset="<?php echo $projectImage['url']; ?>" alt="<?php echo $projectImage['alt']; ?>" media="(min-width: 991px)" />
									<?php if( $projectImageMobile ) { ?>
										<source srcset="<?php echo $projectImageMobile['url']; ?>" media="(max-width: 990px)" />
									<?php } ?>
									<img src="<?php echo $projectImage['url']; ?>" alt="<?php echo $projectImage['alt']; ?>" title="<?php echo $projectImage['title']; ?>"/>
								</picture>
							</div>
							<?php
						}
						?>

					</div>
				<?php endif; ?>

				<?php if( !empty($list_project_videos) && count($list_project_videos)>0 ):
					$num = 1;
					?>

					<div class="project__slider appear">

						<?php foreach( $list_project_videos as $current) {
							$projecHasVideo = $current['has_video'];
							$projectVideo = $current['project_video'];
							$projectVideoMobile = $current['project_video_mobile'];
							$projectPoster = $current['project_poster'];
							$projectImageVideo = $current['project_image_video'];
							?>

							<div class="project__slide project__slide--boxed project__slide--video">
								<div class="video__slide--wrapper">
									<?php
										if($projecHasVideo) {
											if($projectPoster) {
												?>
												<video id="video-<?php echo $projectId ?>-<?php echo $num ?>" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause lazy" muted width="100%" height="100%" controls preload="none" data-src="<?php echo $projectVideo['url']; ?>" poster='<?php echo $projectPoster['url']; ?>'>
													<?php if($projectVideoMobile) {
														?>
															<source src="<?php echo $projectVideoMobile['url']; ?>" media="(min-width: 1025px)" type='video/mp4' />
														<?php
													}
													?>
													<source src="<?php echo $projectVideo['url']; ?>" type='video/mp4' />
												</video>
												<?php
											}
											else {
												?>
													<video id="video-<?php echo $projectId ?>-<?php echo $num ?>" data-src="<?php echo $projectVideo['url']; ?>" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline muted autoplay loop>
														<?php if($projectVideoMobile) {
															?>
																<source data-src="<?php echo $projectVideoMobile['url']; ?>" data-mw="767" />
															<?php
														}
														?>
														<!-- <source src="<?php echo $projectVideo['url']; ?>" type='video/mp4' data-mw="-Infinity" /> -->
													</video>
													<script type="text/javascript">
														new VideoResponser("#video-<?php echo $projectId ?>-<?php echo $num; ?>");
													</script>
												<?php
											}
											?>
												<picture></picture>
											<?php

											$num++;
										}
										else
										{
											?>
												<picture>
													<source data-srcset="<?php echo $projectImageVideo['url']; ?>" alt="<?php echo $projectImageVideo['alt']; ?>" media="(min-width: 991px)" />
													<?php if( $projectImageVideoMobile ) { ?>
														<source data-srcset="<?php echo $projectImageVideoMobile['url']; ?>" media="(max-width: 990px)" />
													<?php } ?>
													<img class="lazy" data-src="<?php echo $projectImageVideo['url']; ?>" alt="<?php echo $projectImageVideo['alt']; ?>" title="<?php echo $projectImageVideo['title']; ?>"/>
												</picture>
											<?php
										}
									?>

								</div>
							</div>
							<?php
						}
						?>

					</div>

				<?php endif; ?>

			</section>

			<?php

		}

		wp_reset_query();

	?>


<!-- Clients -->

	<section class="clients appear">

		<h3 class="clients__heading"><?php _e('Clients','t2m'); ?></h3>

		<div class="clients__wrapper pad">
		<?php
			$query = new WP_Query(array(
				'post_type' => 'client',
				'post_status' => 'publish',
				'posts_per_page' => '20'
			));
			while ($query->have_posts()) {
				$query->the_post();
				$post_id = get_the_ID();
				$clientName = get_the_title();
				$clientImage = get_field('client_image');
				?>
				<div class="client"><img class="client__logo lazy" data-src="<?php echo $clientImage['url']; ?>" alt="<?php echo $clientName; ?>" title="<?php echo $clientName; ?>" /></div>
			<?php
			}

			wp_reset_query();
		?>
		</div>

	</section>


<!-- Testimonials -->

	<section id="testimonials" class="testimonials-block bottom-block">

		<div class="bottom-block__wrapper">

			<img class="lazy" data-src="/wp-content/themes/t2m/assets/images/backgrounds/quotes.svg" alt="Testimonials" title="Testimonials" />

			<div class="testimonials__slider">

				<?php
				$query = new WP_Query(
					array(
						'post_type' => 'testimonial',
						'post_status' => 'publish',
						'posts_per_page' => 8,
						'meta_query' => array(
							array(
							      'key' => 'hide_in_projects',
							      'value' => 1,
							      'compare' => '!='
					      	),
						)
					)
				);

				while ($query->have_posts()) {
					$query->the_post();
					$post_id = get_the_ID();
					$testimonialQuote = get_field('testimonial_quote');
					$testimonialAuthor = get_field('testimonial_author');
					$testimonialRole = get_field('testimonial_role');
					$testimonialHide = get_field('hide_in_projects');

					// if(!$testimonialHide)
					// {
						?>

						<div class="testimonials__slide">
							<h5 class="testimonials__quote appear"><?php echo $testimonialQuote ;?></h5>
							<div class="testimonials__credits appear">
								<span class="testimonials__name"><?php echo $testimonialAuthor ;?></span>
								<span class="testimonials__position"><?php echo $testimonialRole ;?></span>
							</div>
						</div>

						<?php
					// }
				}

				wp_reset_query();
				?>

			</div>

		</div>

	</section>


<?php
	get_footer();

	/* Inner anchors */

		if (isset($_GET['v'])) {
			$v = $_GET['v'];
			?>
				<script type="text/javascript">
					var v = '<?php echo $v; ?>';
					if(jQuery('#'+v).length>0)
					{
						var aTag = jQuery("#"+v);
						setTimeout(function(){
	    					jQuery('html,body').animate({scrollTop: aTag.offset().top},1000);
						},500);
					}
				</script>
			<?php
		}
?>


