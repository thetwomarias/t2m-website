	<section class="case-study__images case-study case-study-v2 b4">
		<?php
			$b4_brand = get_field('b4_brand');
			$b4_campaign = get_field('b4_campaign');
			$b4_item_1 = get_field('b4_item_1');
			$b4_item_2 = get_field('b4_item_2');
			$b4_item_3 = get_field('b4_item_3');
			$b4_item_4 = get_field('b4_item_4');
		?>
			<div class="col col-1 appear">
				<div class="campaign-specs ">
					<h2 class="page-header__specs--desc title"><?php echo $b4_brand; ?></h2>
					<h2 class="page-header__specs--desc"><?php echo $b4_campaign; ?></h2>
				</div>
				<div class="media-container media-container-3 show-mobile">
					<video class="" src="<?php echo $b4_item_3; ?>" playsinline muted autoplay loop></video>
				</div><!--
				--><div class="media-container media-container-1">
					<img class="" src="<?php echo $b4_item_1; ?>" alt="<?php echo $b4_brand . "-". $b4_campaign; ?>"/>
				</div><!--
				--><div class="media-container media-container-2">
					<img class="" src="<?php echo $b4_item_2; ?>" alt="<?php echo $b4_brand . "-". $b4_campaign; ?>"/>
				</div>
			</div><!--
			--><div class="col col-2 appear">
				<div class="media-container text-right show-desktop">
					<video class="" src="<?php echo $b4_item_3; ?>" playsinline muted autoplay loop></video>
				</div><!--
				--><div class="media-container media-container-2 text-right">
					<img class="" src="<?php echo $b4_item_4; ?>" alt="<?php echo $b4_brand . "-". $b4_campaign; ?>"/>
				</div>
			</div>

	</section>