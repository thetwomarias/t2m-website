<meta name="ad.size" content="width=300,height=600">
			<script type="text/javascript">
				var clickTag = "";
			</script>
			<style>
				#animation_container {position:static;display: inline-block; margin:auto; left:0;right:0; top:0;bottom:0; }
				#animation_container_2 {position:static;display: inline-block; margin:auto; left:0;right:0; top:0;bottom:0; }
			</style>
			<script>
				// Animation 1
					var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
					function metaInit() {
						canvas = document.getElementById("canvas");
						anim_container = document.getElementById("animation_container");
						dom_overlay_container = document.getElementById("dom_overlay_container");
						var comp=AdobeAn.getComposition("BAE491CF8ED14B9AB1A4AAACEE401205");
						var lib=comp.getLibrary();
						handleComplete({},comp);
					}
					function handleComplete(evt,comp) {
						//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
						var lib=comp.getLibrary();
						var ss=comp.getSpriteSheet();
						exportRoot = new lib.index();
						stage = new lib.Stage(canvas);
						stage.enableMouseOver();
						//Registers the "tick" event listener.
						fnStartAnimation = function() {
							stage.addChild(exportRoot);
							createjs.Ticker.framerate = lib.properties.fps;
							createjs.Ticker.addEventListener("tick", stage);
						}
						//Code to support hidpi screens and responsive scaling.
						AdobeAn.makeResponsive(false,'both',false,1,[canvas,anim_container,dom_overlay_container]);
						AdobeAn.compositionLoaded(lib.properties.id);
						fnStartAnimation();
					}

				// Animation 2
					var canvas2, stage, exportRoot2, anim_container2, dom_overlay_container2, fnStartAnimation2;


					function metaInit2() {
						canvas2 = document.getElementById("canvas_2");
						anim_container2 = document.getElementById("animation_container_2");
						dom_overlay_container2 = document.getElementById("dom_overlay_container_2");
						var comp=AdobeAn.getComposition("A7816C00CDE3436D8970788C2154EC2D");
						var lib=comp.getLibrary();
						var loader = new createjs.LoadQueue(false);
						loader.addEventListener("fileload", function(evt){handleFileLoad2(evt,comp)});
						loader.addEventListener("complete", function(evt){handleComplete2(evt,comp)});
						var lib=comp.getLibrary();
						loader.loadManifest(lib.properties.manifest);
					}
					function handleFileLoad2(evt, comp) {
						var images=comp.getImages();
						if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
					}
					function handleComplete2(evt,comp) {
						//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
						var lib=comp.getLibrary();
						var ss=comp.getSpriteSheet();
						var queue = evt.target;
						var ssMetadata = lib.ssMetadata;
						for(i=0; i<ssMetadata.length; i++) {
							ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
						}
						exportRoot2 = new lib.index();
						stage = new lib.Stage(canvas2);
						stage.enableMouseOver();
						//Registers the "tick" event listener.
						fnStartAnimation2 = function() {
							stage.addChild(exportRoot2);
							createjs.Ticker.framerate = lib.properties.fps;
							createjs.Ticker.addEventListener("tick", stage);
						}
						//Code to support hidpi screens and responsive scaling.
						AdobeAn.makeResponsive(false,'both',false,1,[canvas_2,anim_container2,dom_overlay_container2]);
						AdobeAn.compositionLoaded(lib.properties.id);
						fnStartAnimation2();
					}
			</script>

			<section class="case-study__images case-study case-study-v2 b3">
				<?php
					$b3_brand = get_field('b3_brand');
					$b3_campaign = get_field('b3_campaign');
					$b3_item_1 = get_field('b3_item_1');
					$b3_item_2 = get_field('b3_item_2');
				?>
					<div class="col col-1 appear">
						<div class="campaign-specs ">
							<h2 class="page-header__specs--desc title"><?php echo $b3_brand; ?></h2>
							<h2 class="page-header__specs--desc"><?php echo $b3_campaign; ?></h2>
						</div>
						<div class="media-container show-desktop ">
							<div id="animation_container_2" style="background-color:rgba(222, 228, 233, 1.00); width:300px; height:600px">
								<canvas id="canvas_2" width="300" height="600" style="position: absolute; display: block; background-color:rgba(222, 228, 233, 1.00);"></canvas>
								<div id="dom_overlay_container_2" style="pointer-events:none; overflow:hidden; width:300px; height:600px; position: absolute; left: 0px; top: 0px; display: block;">
								</div>
							</div>
						</div>
					</div><!--
					--><div class="col col-2 appear">
						<div class="media-container text-center">
							<div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:300px; height:600px">
								<canvas id="canvas" width="300" height="600" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
								<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:300px; height:600px; position: absolute; left: 0px; top: 0px; display: block;">
								</div>
							</div>
						</div>
					</div><!--
					--><div class="col col-3 appear">
						<div class="media-container text-center ">
							<img class="" src="<?php echo $b3_item_1; ?>" alt="<?php echo $b3_brand . "-". $b3_campaign; ?>"/>
						</div>
					</div><!--
					--><div class="col col-4-mobile show-mobile appear">
						<div class="media-container text-center">
							<img class="" src="<?php echo $b3_item_2; ?>" alt="<?php echo $b3_brand . "-". $b3_campaign; ?>"/>
						</div>
					</div>

			</section>