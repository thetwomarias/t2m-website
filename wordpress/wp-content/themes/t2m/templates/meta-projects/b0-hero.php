	<section class="case-study case-study-v2 b0 appear">
		<?php
			$b0_desktop = get_field('b0_desktop');
			$b0_mobile = get_field('b0_mobile');
		?>
		<div class="show-desktop campaign-img">
			<img class="" src="<?php echo $b0_desktop; ?>" alt="<?php echo $caseStudyTitle; ?>"/>
		</div>
		<div class="show-mobile campaign-img">
			<img class="" src="<?php echo $b0_mobile; ?>" alt="<?php echo $caseStudyTitle; ?>"/>
		</div>
	</section>