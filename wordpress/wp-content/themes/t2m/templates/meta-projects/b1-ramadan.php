	<section class="case-study__images case-study case-study-v2 b1">
		<?php
			$b1_brand = get_field('b1_brand');
			$b1_campaign = get_field('b1_campaign');
			$b1_item_1 = get_field('b1_item_1');
			$b1_item_2 = get_field('b1_item_2');
			$b1_item_3 = get_field('b1_item_3');
			$b1_item_4 = get_field('b1_item_4');
		?>
		<div class="show-desktop">
			<div class="col col-1 appear">
				<div class="campaign-specs ">
					<h2 class="page-header__specs--desc title"><?php echo $b1_brand; ?></h2>
					<h2 class="page-header__specs--desc"><?php echo $b1_campaign; ?></h2>
				</div>
				<div class="media-container">
					<img class="" src="<?php echo $b1_item_1; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div>
			</div><!--
			--><div class="col col-2 appear">
				<div class="media-container">
					<div class="mobile-mask-container">
						<div class="mask">
							<img src="/wp-content/themes/t2m/assets/images/case-studies/meta/b1/mobile-mask-video.png" alt="" />
							<div class="media-content">
								<video class="" src="<?php echo $b1_item_2; ?>" playsinline muted autoplay loop></video>
							</div>
						</div>
					</div>
				</div>
			</div><!--
			--><div class="col col-3 appear">
				<div class="media-container text-center">
					<img class="" src="<?php echo $b1_item_3; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div><!--
				--><div class="media-container media-container-2 text-center">
					<img class="" src="<?php echo $b1_item_4; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div>
			</div>
		</div>


		<div class="show-mobile">
			<div class="row row-1 appear">
				<div class="campaign-specs ">
					<h2 class="page-header__specs--desc title"><?php echo $b1_brand; ?></h2>
					<h2 class="page-header__specs--desc"><?php echo $b1_campaign; ?></h2>
				</div>
			</div><!--
			--><div class="row row-2 appear">
				<div class="media-container">
					<div class="mobile-mask-container">
						<div class="mask">
							<img src="/wp-content/themes/t2m/assets/images/case-studies/meta/b1/mobile-mask-video.png" alt="" />
							<div class="media-content">
								<video class="" src="<?php echo $b1_item_2; ?>" playsinline muted autoplay loop></video>
							</div>
						</div>
					</div>
				</div>
			</div><!--
			--><div class="row row-3 appear">
				<div class="media-container text-center">
					<img class="" src="<?php echo $b1_item_3; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div>
			</div><!--
			--><div class="row row-4 appear">
				<div class="media-container media-container-1 text-center">
					<img width="50%" class="" src="<?php echo $b1_item_4; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div>
				<div class="media-container media-container-2 text-center">
					<img class="" src="<?php echo $b1_item_1; ?>" alt="<?php echo $b1_brand . "-". $b1_campaign; ?>"/>
				</div>
			</div>
		</div>
	</section>