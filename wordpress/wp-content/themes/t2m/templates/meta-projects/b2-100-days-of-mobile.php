	<section class="case-study__images case-study case-study-v2 b2">
		<?php
			$b2_brand = get_field('b2_brand');
			$b2_campaign = get_field('b2_campaign');
			$b2_item_1 = get_field('b2_item_1');
			$b2_item_2 = get_field('b2_item_2');
			$b2_item_3 = get_field('b2_item_3');
		?>
		<div class="show-desktop">
			<div class="col col-1 appear">
				<div class="campaign-specs ">
					<h2 class="page-header__specs--desc title"><?php echo $b2_brand; ?></h2>
					<h2 class="page-header__specs--desc"><?php echo $b2_campaign; ?></h2>
				</div>
				<div class="media-container">
					<img class="" src="<?php echo $b2_item_1; ?>" alt="<?php echo $b2_brand . "-". $b2_campaign; ?>"/>
				</div>
			</div><!--
			--><div class="col col-2 appear">
				<div class="media-container">
					<div class="mobile-mask-container">
						<div class="mask">
							<img class="active-mask" src="/wp-content/themes/t2m/assets/images/case-studies/meta/b2/mobile-mask-video.png" alt="" />
							<img class="vHidden" src="/wp-content/themes/t2m/assets/images/case-studies/meta/b2/mobile-mask-video.png" alt="" />
							<div class="media-content">
								<video class="" src="<?php echo $b2_item_2; ?>" playsinline muted autoplay loop></video>
							</div>
						</div>
					</div>
				</div>
			</div><!--
			--><div class="col col-3 appear">
				<div class="media-container">
					<div class="mobile-mask-container">
						<div class="mask">
							<img src="/wp-content/themes/t2m/assets/images/case-studies/meta/b2/mobile-mask-video.png" alt="" />
							<div class="media-content">
								<video class="" src="<?php echo $b2_item_3; ?>" playsinline muted autoplay loop></video>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="show-mobile">
			<div class="row row-1 appear">
				<div class="campaign-specs ">
					<h2 class="page-header__specs--desc title"><?php echo $b2_brand; ?></h2>
					<h2 class="page-header__specs--desc"><?php echo $b2_campaign; ?></h2>
				</div>
			</div><!--
			--><div class="row row-2 appear">
				<div class="media-container">
					<div class="mobile-mask-container">
						<div class="mask">
							<img class="active-mask" src="/wp-content/themes/t2m/assets/images/case-studies/meta/b2/mobile-mask-video.png" alt="" />
							<img class="vHidden" src="/wp-content/themes/t2m/assets/images/case-studies/meta/b2/mobile-mask-video.png" alt="" />
							<div class="media-content">
								<video class="" src="<?php echo $b2_item_2; ?>" playsinline muted autoplay loop></video>
							</div>
						</div>
					</div>
				</div>
			</div><!--
			--><div class="row row-3 appear">
				<div class="media-container media-container-1 text-center">
					<img class="" src="<?php echo $b2_item_1; ?>" alt="<?php echo $b2_brand . "-". $b2_campaign; ?>"/>
				</div><!--
				--><div class="media-container media-container-2 text-center">
					<video class="" src="<?php echo $b2_item_3; ?>" playsinline muted autoplay loop></video>
				</div>
			</div>
		</div>
	</section>