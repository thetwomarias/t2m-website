<?php if( !empty($caseImageOne) || !empty($caseImageTwo) ): ?>

	<section class="<?php echo $class; ?>">
		<?php if( !empty($caseImageOne) ): ?>
			<img class="appear" src="<?php echo $caseImageOne['url'] ?>" alt="<?php echo $caseImageOne['alt'] ?>" />
		<?php endif; ?>

		<?php if( !empty($caseImageTwo) ): ?>
			<img class="appear right" src="<?php echo $caseImageTwo['url'] ?>" alt="<?php echo $caseImageTwo['alt'] ?>" />
		<?php endif; ?>

	</section>

<?php endif; ?>