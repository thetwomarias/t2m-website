<?php
if(isset($nextCaseStudyId) && $nextCaseStudyId!=0)
{
	?>
		<section class="case-study-next">
			<div class="case-study-next__text">
				<h4 class="case-study-next__title"><?php _e('Next Case Study','t2m'); ?></h4>
				<h5 class="case-study-next__heading"><?php echo $nextCaseTitle; ?></h5>
				<div class="case-study-next__desc"><?php echo $nextCaseDescription; ?></div>
				<a href="<?php echo $nextCaseLinkUrl;?>" class="link--cta next-case-study-cta"><span><?php _e('Read more','t2m'); ?></span></a>
			</div>
			<div class="case-study-next__image case-study-next__image--boxed">
				<a href="<?php echo $nextCaseLinkUrl;?>" class="link--inner next-case-study-cta"></a>
				<img src="<?php echo $nextCaseImage ['url'] ;?>" alt="<?php echo $nextCaseImage ['alt'] ;?>" />
			</div>
		</section>
	<?php
}
?>