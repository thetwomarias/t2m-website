<?php if( !empty($caseMultipleImagesTwo) ): ?>
	<?php if( have_rows('case_study_multiple_images_two') ): ?>

		<section class="case-study__images case-study__images--two case-study__images--diff background--pink">
			<?php while( have_rows('case_study_multiple_images_two') ): the_row();
				$caseMultipleImage = get_sub_field('multiple_image');
				$caseMultipleImageLevel = get_sub_field('multiple_image_level');
				?>
				<img class="appear case-study__image--<?php if( $caseMultipleImageLevel == 'left' ): ?>left<?php endif; ?><?php if( $caseMultipleImageLevel == 'right' ): ?>right<?php endif; ?>" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>
			<?php endwhile; ?>
		</section>

	<?php endif; ?>
<?php endif; ?>