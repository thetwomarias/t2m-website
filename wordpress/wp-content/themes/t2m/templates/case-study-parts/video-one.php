<?php if( !empty($caseStudyVideoOne) ): ?>

    <section class="<?php echo $class; ?>">

      <div class="appear video__background" data-vide-bg="mp4: <?php echo $caseStudyVideoOne ['url'] ?>, webm: <?php echo $caseStudyVideoOne ['url'] ?>,  ogv: <?php echo $caseStudyVideoOne ['url'] ?>, poster: <?php echo $caseStudyVideoOnePoster ?>'" data-vide-options="posterType: 'jpg', loop: true, muted: true, volume: 0, autoplay: true, position: '0% 0%', resizing: true"></div>

    </section>

  <?php endif; ?>

  <?php if( !empty($caseStudyVideoOneMobile) ): ?>

    <section class="appear <?php echo $classMobile; ?>">

      <video src="<?php echo $caseStudyVideoOneMobile ['url'] ?>" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video>

    </section>

  <?php endif; ?>
