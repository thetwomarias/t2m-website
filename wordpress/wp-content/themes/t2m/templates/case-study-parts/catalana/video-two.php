
<?php if( !empty($caseStudyVideoTwo) || !empty($caseStudyVideoTwo) ): ?>

	<section class="case-study__video catalana-occidente-videos-2" id="catalana-occidente-videos">

		<?php if( !empty($caseImageFour) ): ?>
			<img class="appear" src="<?php echo $caseImageFour['url'] ?>" alt="<?php echo $caseImageFour['alt'] ?>" />
		<?php endif; ?>

		<!-- <img class="rotate-180 top-0 background--absolute left-0 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-white-cs.svg" alt=""/> -->

		<?php if( !empty($caseStudyVideoTwo) ): ?>
			<div class="case-study__video--macbook catalana-video-2 appear">
				<video id="video-catalana-2" src="<?php echo $caseStudyVideoTwoMobile ['url'] ?>" width="100%" height="100%" style="width:100%; display:block; margin-bottom:4.75%" poster='<?php echo $caseStudyVideoTwoPoster ['url'] ?>' class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" controls preload="none" muted>
					<source src="<?php echo $caseStudyVideoTwo ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
					<source src="<?php echo $caseStudyVideoTwo ['url'] ?>" type='video/mp4' />
					<source src="<?php echo $caseStudyVideoTwo ['url'] ?>" type='video/webm' />
				</video>
			</div>

			<img class="hide-md background--absolute left-0 bottom-0 minus-z" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple.svg" alt="">

		<?php endif; ?>

	</section>

<?php endif; ?>