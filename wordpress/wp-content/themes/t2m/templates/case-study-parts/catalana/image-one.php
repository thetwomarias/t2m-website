<?php if( !empty($caseImageOne) ): ?>

	<section class="<?php echo $class; ?>">
		<img class="appear" src="<?php echo $caseImageOne['url'] ?>" alt="<?php echo $caseImageOne['alt'] ?>" />
		<img class="background--absolute left-0 bottom-0 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple-catalana.svg" alt=""/>
	</section>

<?php endif; ?>