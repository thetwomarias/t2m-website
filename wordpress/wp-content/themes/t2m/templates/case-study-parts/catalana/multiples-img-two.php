<?php if( !empty($caseMultipleImagesTwo) ): ?>

	<section class="padding-0">

		<div class="case-study__images case-study__images--two case-study__images--two-two">

			<?php if( have_rows('case_study_multiple_images_one') ): ?>

				<?php
					foreach ($caseMultipleImagesTwo as $current) {
						$caseMultipleImage = $current['multiple_image'];
						$caseMultipleImageLevel = $current['multiple_image_level'];
					?>
					<img class="case-study__image--<?php if( $caseMultipleImageLevel == 'left' ): ?>left<?php endif; ?><?php if( $caseMultipleImageLevel == 'right' ): ?>right<?php endif; ?> appear" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>

				<?php } ?>
				<img class="background--absolute left-0 top-35 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple-inverted.svg" alt=""/>

			<?php endif; ?>

		</div>

	</section>

<?php endif; ?>