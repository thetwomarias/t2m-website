<?php if( !empty($caseImageOne) || !empty($caseMultipleImagesOne) ): ?>

	<section class="appear case-study__block visible background--white">

		<?php if( !empty($caseImageOne)): ?>
			<img class="appear" src="<?php echo $caseImageOne['url'] ?>" alt="<?php echo $caseImageOne['alt'] ?>" />
		<?php endif; ?>

		<!-- <img class="rotate-180 top-0 background--absolute left-0 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-white-cs.svg" alt=""/> -->

		<div class="case-study__images case-study__images--two">

			<?php if( have_rows('case_study_multiple_images_one') ): ?>

				<?php
					foreach ($caseMultipleImagesOne as $current) {
						$caseMultipleImage = $current['multiple_image'];
						$caseMultipleImageLevel = $current['multiple_image_level'];
						?>
						<img class="case-study__image--<?php if( $caseMultipleImageLevel == 'left' ): ?>left<?php endif; ?><?php if( $caseMultipleImageLevel == 'right' ): ?>right<?php endif; ?> appear" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>
					<?php } ?>

			<?php endif; ?>

		</div>

		<img class="bottom-0 background--absolute left-0 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple-catalana.svg" alt=""/>

	</section>

<?php endif; ?>