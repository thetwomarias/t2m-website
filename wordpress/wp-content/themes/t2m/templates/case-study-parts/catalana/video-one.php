<?php if( !empty($caseStudyVideoOne) ): ?>

	<section class="case-study__video background--purple" id="catalana-occidente-videos">

		<div class="case-study__video--macbook appear">
			<video id="video-catalana-1" src="<?php echo $caseStudyVideoOneMobile ['url'] ?>" width="100%" height="100%" style="width:100%; display:block; margin-bottom:4.75%;" poster='<?php echo $caseStudyVideoOnePoster ['url'] ?>' class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" controls preload="none" muted>
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/mp4' />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/webm' />
			</video>
		</div>

		<!-- <img class="background--absolute left-0 bottom-0 minus-z" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple.svg" alt=""> -->

	</section>

<?php endif; ?>