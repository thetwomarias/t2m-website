<?php if( !empty($caseMobileSlider) ): ?>

	<section class="case-study__block background--white" >

		<?php if( have_rows('case_study_mobile_slider') ): ?>

			<div class="case-study__images case-study__images--three case-study__slider">

				<?php
				foreach($caseMobileSlider as $current) {
					$caseMobileSliderImage = $current['mobile_slide'];
					$caseMobileSlideAlignment = $current['mobile_slide_alignment'];
					?>
					<img src="<?php echo $caseMobileSliderImage['url'] ?>" class="<?php if( $caseMobileSlideAlignment == 'top' ): ?>move-up<?php endif; ?>" alt="<?php echo $caseMobileSliderImage ['alt'] ?>"/>
				<?php } ?>

			</div>

		<?php endif; ?>

	</section>

<?php endif; ?>

