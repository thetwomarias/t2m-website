<?php if( !empty($caseStudyVideoThree) ): ?>

	<section class="case-study__video" id="accelya-motion">
		<img class="background--absolute right-0 top-0 minus-z" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple.svg" alt="" />
		<div class="case-study__video--macbook">
			<video id="video-accelya-3" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='<?php echo $caseStudyVideoThreePoster ['url'] ?>' muted>
				<source src="<?php echo $caseStudyVideoThreeMobile ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
				<source src="<?php echo $caseStudyVideoThree ['url'] ?>" type='video/mp4' />
				<source src="<?php echo $caseStudyVideoThree ['url'] ?>" type='video/webm' />
			</video>
		</div>
	</section>

<?php endif; ?>