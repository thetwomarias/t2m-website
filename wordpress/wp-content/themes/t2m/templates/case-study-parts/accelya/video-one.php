<?php if( !empty($caseStudyVideoOne) ): ?>

	<section class="case-study__video background--purple">

		<div class="case-study__video--macbook">
			<video id="video-accelya-1" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='<?php echo $caseStudyVideoOnePoster ['url'] ?>' muted>
				<source src="<?php echo $caseStudyVideoOneMobile ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/mp4' />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/webm' />
			</video>
		</div>

	</section>

<?php endif; ?>