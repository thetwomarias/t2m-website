<?php if( !empty($caseStudyVideoTwo) ): ?>

	<section class="case-study__video case-study__video--full background--purple">
		<video id="video-accelya-2" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='<?php echo $caseStudyVideoTwoPoster ['url'] ?>' muted>
			<source src="<?php echo $caseStudyVideoTwoMobile ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
			<source src="<?php echo $caseStudyVideoTwo ['url'] ?>" type='video/mp4' />
			<source src="<?php echo $caseStudyVideoTwo ['url'] ?>" type='video/webm' />
		</video>
	</section>

<?php endif; ?>