<?php if( !empty($caseMultipleImagesOne) ): ?>

	<section class="case-study__block case-study__images background--purple">

		<?php if( have_rows('case_study_multiple_images_one') ): ?>

			<?php
				foreach ($caseMultipleImagesOne as $current) {
					$caseMultipleImage = $current['multiple_image'];
					$caseMultipleImageLevel = $current['multiple_image_level'];
					?>
					<img class="image--<?php if( $caseMultipleImageLevel == 'left' ): ?>left front-z<?php endif; ?><?php if( $caseMultipleImageLevel == 'right' ): ?>right back-z<?php endif; ?>" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>
			<?php } ?>

		<?php endif; ?>

	</section>

<?php endif; ?>