<?php if( !empty($caseImageTwo) ): ?>

	<section class="<?php echo $class; ?>">
		<img class="appear" src="<?php echo $caseImageTwo['url'] ?>" alt="<?php echo $caseImageTwo['alt'] ?>" />
	</section>

<?php endif; ?>