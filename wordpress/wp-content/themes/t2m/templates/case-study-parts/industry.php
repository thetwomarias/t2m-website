<?php
	if(isset($caseStudyIndustry) && $caseStudyIndustry!=''){
		?>
			<div>
				<h4 class="page-header__specs--title">
					<?php
						if ( is_page_template( 'templates/display-ads.php' ))
							_e('Brands','t2m');
						else
							_e('Industry','t2m');
					?>
				</h4>
				<h3 class="page-header__specs--desc"><?php echo $caseStudyIndustry ?></h3>
			</div>
		<?php
	}
?>