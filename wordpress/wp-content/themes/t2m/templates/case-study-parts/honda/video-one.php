<?php if( !empty($caseStudyVideoOne) ): ?>

	<section class="case-study__video background--purple">

		<div class="appear case-study__video--macbook appear hide-sm hide-md show-lg">
			<video src="<?php echo $caseStudyVideoOne ['url'] ?>" width="100%" style="width:100%; display:block; margin-bottom:4.75%;" class="appear" playsinline loop muted autoplay></video>
		</div>

		<div class="appear case-study__video--macbook appear hide-lg show-sm show-md ">
			<video id="miimo-video" src="<?php echo $caseStudyVideoOneMobile ['url'] ?>" width="100%" height="100%" style="width:100%; display:block; margin-bottom:-1px; top:-0.3rem;" poster='<?php echo $caseStudyVideoOnePoster ['url'] ?>' class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" controls preload="none" muted>
				<source src="<?php echo $caseStudyVideoOneMobile ['url'] ?>"  type='video/mp4' media="all and (max-width:576px)" />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/mp4' />
				<source src="<?php echo $caseStudyVideoOne ['url'] ?>" type='video/webm' />
			</video>
		</div>

	</section>

<?php endif; ?>