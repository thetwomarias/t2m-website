<?php if( !empty($caseMultipleImagesOne) ): ?>

	<section class="case-study__images case-study__images--two">

		<?php if( have_rows('case_study_multiple_images_one') ): ?>

			<?php
				foreach ($caseMultipleImagesOne as $current) {
					$caseMultipleImage = $current['multiple_image'];
			    	$caseMultipleImageLevel = $current['multiple_image_level'];
			    	?>
			  		<img class="appear case-study__image--<?php if( $caseMultipleImageLevel == 'left' ): ?>left<?php endif; ?><?php if( $caseMultipleImageLevel == 'right' ): ?>right<?php endif; ?>" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>
			  	<?php } ?>

			<img class="background--absolute left-0 minus-z" style="width:100%" src="/wp-content/themes/t2m/assets/images/backgrounds/corner-purple-inverted.svg" alt=""/>

		<?php endif; ?>

	</section>

<?php endif; ?>
