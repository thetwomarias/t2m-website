<?php if( !empty($caseIcons) ): ?>

	<section class="case-study__icons background--purple">
		<?php if( have_rows('case_study_icons') ): ?>

			<div class="case-study__icons--list">
			<?php while( have_rows('case_study_icons') ): the_row();
				$caseIcon = get_sub_field('icon');
				?>
				<div class="case-study__icons--item"><img src="<?php echo $caseIcon['url'] ?>" alt="<?php echo $caseIcon['alt'] ?>" title="<?php echo $caseIcon['title'] ?>"/></div>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</section>

<?php endif; ?>