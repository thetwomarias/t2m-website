<?php
	if(isset($caseStudyDevelopment) && !empty($caseStudyDevelopment)){
		$numDevelopment = count($caseStudyDevelopment);
		?>
			<div>
				<h4 class="page-header__specs--title">
				<?php
						if ( is_page_template( 'templates/display-ads.php' ) || is_page_template( 'templates/meta.php' ))
							_e('Tasks','t2m');
						else
							_e('Development','t2m');
					?>
				</h4>

				<ul class="page-header__specs--list">
					<?php
					foreach ($caseStudyDevelopment as $index => $value)
						echo '<li class="page-header__specs--item">'.$value['development_name'].'</li>';
					?>
				</ul>

			</div>
		<?php
	}
?>