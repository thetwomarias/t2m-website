<?php if( !empty($caseImageThree) ): ?>

	<section class="<?php echo $class; ?>">
		<img class="appear" src="<?php echo $caseImageThree['url'] ?>" alt="<?php echo $caseImageThree['alt'] ?>" />
	</section>

<?php endif; ?>