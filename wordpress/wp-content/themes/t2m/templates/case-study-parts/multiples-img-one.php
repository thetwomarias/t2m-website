
<?php if( !empty($caseMultipleImagesOne) ): ?>

	<section class="<?php echo $class; ?>">

		<?php if( have_rows('case_study_multiple_images_one') ): ?>

			<div class="appear case-study__images case-study__images--two">
				<?php
				foreach ($caseMultipleImagesOne as $current) {
					$caseMultipleImage = $current['multiple_image'];
			    	$caseMultipleImageLevel = $current['multiple_image_level'];
			    	?>
			  		<img class="appear <?php if( $caseMultipleImageLevel == 'front' ): ?>front-z<?php endif; ?> <?php if( $caseMultipleImageLevel == 'back' ): ?>back-z<?php endif; ?>" src="<?php echo $caseMultipleImage['url'] ?>" alt="<?php echo $caseMultipleImage['alt'] ?>"/>
			  	<?php } ?>
			</div>

			<img class="background--absolute left-0 bottom-0 hide-sm" src="/wp-content/themes/t2m/assets/images/backgrounds/<?php echo $background; ?>" alt=""/>

		<?php endif; ?>

	</section>

<?php endif; ?>