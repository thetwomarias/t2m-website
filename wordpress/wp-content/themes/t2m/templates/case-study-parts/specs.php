<div class="page-header__container">

	<div class="page-header__specs appear">

		<?php
		// Industry
			require_once get_stylesheet_directory() . '/templates/case-study-parts/industry.php';

		// Techonology
			require_once get_stylesheet_directory() . '/templates/case-study-parts/technology.php';

		// Development
			require_once get_stylesheet_directory() . '/templates/case-study-parts/development.php';

		?>

	</div>

	<div class="page-header__brief appear">
		<div class="brief__desc">
			<?php echo $caseStudyBrief ?>
			<?php if(isset($caseLogos) && count($caseLogos)>0)
			{
				?>
					<ul class="desc-logos">
						<?php
						foreach($caseLogos as $logo)
						{
							echo "<li>";
								if(isset($logo['link']) && $logo['link']!='')
									echo '<a href="'.$logo['link'].'" target="_blank">';
								echo '<img src="'.$logo['image']['url'].'" alt="'.$logo['image']['title'].'" />';
								if(isset($logo['image']['link']) && $logo['image']['link']!='')
									echo '</a>';
							echo "</li>";
						}
						?>
					</ul>
				<?php
			}
			?>
		</div>
		<?php if( !empty($caseStudyLinkUrl) ): ?>
			<a href="<?php echo $caseStudyLinkUrl; ?>" target="_blank" class="link--cta"><span><?php echo $caseStudyLinkText; ?></span></a>
		<?php endif; ?>
	</div>
</div>