<section class="page-header <?php echo $class; ?>">

	<?php
		// Title / Hero
	?>
		<div class="scroll-btn scroll fixed appear appear-only"><span><?php _e('Scroll','t2m'); ?></span></div>
		<div class="page-header__container">
			<h1 class="page-header__title"><?php the_field('case_study_title'); ?></h1>
			<img class="page-header__image" src="<?php echo $caseStudyHero ['url'] ?>" alt="<?php echo $caseStudyHero ['alt'] ?>"/>
		</div>

	<?php
		// Specs / Brief
			require_once get_stylesheet_directory() . '/templates/case-study-parts/specs.php';
	?>

</section>