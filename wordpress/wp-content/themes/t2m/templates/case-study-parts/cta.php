<section class="case-study-cta">
	<div class="bottom-block__wrapper">
		<div class="bottom-block__content">
			<div class="bottom-block__image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/case-studies/chat.svg" alt="" title="">
			</div>
			<div class="bottom-block__text">
				<h6 class="bottom-block__heading"><?php _e('Do you have a similar<br>project in mind?','t2m'); ?></h6>
				<!-- <div class="bottom-block__desc"></div> -->
				<?php $contactId = apply_filters( 'wpml_object_id', 189 ); // Say Hi ?>
				<a href="<?php echo get_the_permalink($contactId); ?>" class="link--cta"><span><?php _e('Let\'s talk!','t2m'); ?></span></a>
			</div>
		</div>
	</div>
</section>