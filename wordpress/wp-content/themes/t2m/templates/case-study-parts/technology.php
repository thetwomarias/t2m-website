<?php
	if(isset($caseStudyTechnology) && !empty($caseStudyTechnology)){
		$numTechnology = count($caseStudyTechnology);
		?>
		<div>
			<h4 class="page-header__specs--title">
			<?php
					if ( is_page_template( 'templates/display-ads.php' ))
						_e('Clients','t2m');
					else
						_e('Technology','t2m');
				?>
			</h4>

			<h2 class="page-header__specs--desc">
				<?php
				foreach ($caseStudyTechnology as $index => $value){
					$comma = ", ";
					if($index==$numTechnology-1)
					$comma = "";
					echo $value['technology_name'].$comma;
				}
				?>
			</h2>

		</div>
	<?php
}
?>