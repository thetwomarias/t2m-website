<?php if( !empty($caseImageOne) ): ?>

	<section class="<?php echo $class; ?>">
		<img class="appear" src="<?php echo $caseImageOne['url'] ?>" alt="<?php echo $caseImageOne['alt'] ?>" />
	</section>

<?php endif; ?>