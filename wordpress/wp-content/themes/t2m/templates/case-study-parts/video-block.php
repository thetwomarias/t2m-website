<?php
	$video_block_vimeo_id = get_field('video_block_vimeo_id');

	if(isset($video_block_vimeo_id) && $video_block_vimeo_id!='')
	{
		?>
			<section class="case-study-video-block home-team appear">
				<div class="home-team__wrapper">
					<div class="home-team__text appear">
						<?php
						$videoBlockTitle = get_field('video_block_title');
						$videoBlockText = get_field('video_block_text');
						$videoBlockLinkLabel = get_field('video_block_link_text');
						$videoBlockLinkUrl = get_field('video_block_link_url');
						if(isset($videoBlockTitle) && !empty($videoBlockTitle)){
							?>
								<h4 cl ss="appear"><?php echo $videoBlockTitle; ?></h4>
							<?php
						}
						if(isset($videoBlockText) && !empty($videoBlockText)){
							?>
								<p class="appear"><?php echo $videoBlockText; ?></p>
							<?php
						}
						if(isset($videoBlockLinkLabel) && $videoBlockLinkLabel!='' && isset($videoBlockLinkUrl) && $videoBlockLinkUrl!='')
						{
							?>
								<a href="<?php echo $videoBlockLinkUrl; ?>" class="link--cta appear"><span><?php echo $videoBlockLinkLabel; ?></span></a>
							<?php
						}
						?>
					</div>
					<div class="home-team__image">
						<?php
						$videoBlockVimeoId = get_field('video_block_vimeo_id');
						$videoBlockImage = get_field('video_block_poster_image');

						if( !empty($videoBlockVimeoId) ):
							?>
								<div class='embed-container appear'>
									<?php
										$classVideo = "";
										if( !empty($videoBlockImage) ):
											$classVideo= "hidden-video";
											?>
												<a id="play-team-video" href="javascript:;" class="play"></a>
												<div class="poster-container">
													<img id="team-poster-image" class="lazy" data-src="<?php echo $videoBlockImage['url']; ?>" alt="<?php echo $videoBlockImage['alt']; ?>" title="<?php echo $videoBlockImage['title']; ?>" />
												</div>
											<?php
										endif;
									?>
									<iframe class="<?php echo $classVideo; ?>" id="team-video" src='https://player.vimeo.com/video/<?php echo $videoBlockVimeoId; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen allow="autoplay" ></iframe>
								</div>
							<?php
						elseif( !empty($videoBlockImage) ):
							?>
								<img class="lazy" data-src="<?php echo $videoBlockImage['url']; ?>" alt="<?php echo $videoBlockImage['alt']; ?>" title="<?php echo $videoBlockImage['title']; ?>" />
							<?php
						endif;
						?>
					</div>
				</div>
			</section>
		<?php
	}
?>
