<?php if( !empty($quotes) ): ?>

	<section id="testimonials" class="testimonials-block bottom-block <?php echo $class; ?>">

		<div class="bottom-block__wrapper">

			<?php if($class=="case-study__image background--pink" || $class=='testimonials--pink'){ ?>
				<img class="lazy" data-src="/wp-content/themes/t2m/assets/images/backgrounds/quotes-white.svg" alt="Testimonials" title="Testimonials" />
			<?php } else if($class=='testimonials--pink quotes-purple'){ ?>
				<img class="lazy" data-src="/wp-content/themes/t2m/assets/images/backgrounds/quotes-purple.svg" alt="Testimonials" title="Testimonials" />
			<?php } else { ?>
				<img class="lazy" data-src="/wp-content/themes/t2m/assets/images/backgrounds/quotes.svg" alt="Testimonials" title="Testimonials" />
			<?php } ?>

			<div class="testimonials__slider">

				<?php
				foreach($quotes as $current) {

		      		$testimionialQuote = get_field('testimonial_quote',$current);
		      		$testimionialAuthor = get_field('testimonial_author',$current);
		      		$testimionialRole = get_field('testimonial_role',$current);

					?>
						<div class="testimonials__slide">
							<h5 class="testimonials__quote appear"><?php echo $testimionialQuote ;?></h5>
							<div class="testimonials__credits appear">
								<span class="testimonials__name"><?php echo $testimionialAuthor ;?></span>
								<span class="testimonials__position"><?php echo $testimionialRole ;?></span>
							</div>
						</div>
					<?php

				} ?>

			</div>

		</div>

	</section>

<?php endif; ?>