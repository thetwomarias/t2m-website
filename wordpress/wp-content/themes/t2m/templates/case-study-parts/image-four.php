<?php if( !empty($caseImageFour) ): ?>

	<section class="<?php echo $class; ?>">
		<img class="appear" src="<?php echo $caseImageFour['url'] ?>" alt="<?php echo $caseImageFour['alt'] ?>" />
	</section>

<?php endif; ?>