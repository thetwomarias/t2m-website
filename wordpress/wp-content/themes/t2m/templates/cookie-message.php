<div class="cookie-message background--blue">
	<section class="base">
		<p><?php echo get_field('cookies_cookies_text', 'option'); ?></p>
	</section>
	<section class="settings">
		<p><?php echo get_field('cookies_cookies_strict_text', 'option'); ?>
		<div class="checkbox-container">
			<label class="form-control" for="checkbox-party">
				<input type="checkbox" id="checkbox-party">
				<span><a href="<?php echo get_field('cookies_cookies_link_third_party', 'option')['url']; ?>"><?php _e( 'Analytics', 't2m'); ?></a></span>
			</label>
			<a id="saveCookies" class="cookie-close link--cta link--white"><span><?php _e( 'Save', 't2m'); ?></span></a>
		</div>
	</section>
	<div class="buttons">
		<p><a id="acceptCookies" class="cookie-close base link--cta link--white"><span><?php _e( 'Accept', 't2m'); ?></span></a></p>
		<p><a id="rejectCookies" class="cookie-close base link--cta link--white"><span><?php _e( 'Reject', 't2m'); ?></span></a></p>
		<p class="cookie-settings-container"><a id="cookieSettings" class="cookie-settings link--cta link--white"><?php _e( 'Manage Settings', 't2m'); ?></a></p>
	</div>
</div>