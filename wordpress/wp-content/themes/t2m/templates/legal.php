<?php
/*
Template Name: Legal Template
*/

get_header();
?>

<section class="page-header">
  <div class="scroll-btn scroll fixed"><span><?php _e('Scroll','t2m'); ?></span></div>
  <div class="page-header__container">
    <h1 class="page-header__title"><?php the_field('page_header_title'); ?></h1>
  </div>
</section>

<?php if( have_rows('legal') ): ?>
  <section class="legal-content appear">
  <?php while( have_rows('legal') ): the_row();
      $legalHeading = get_sub_field('legal_heading');
      $legalContent = get_sub_field('legal_content', false, true);

    ?>
    <h2 class="legal__heading"><?php echo $legalHeading ?></h2>
  <?php echo $legalContent ?>
  <?php endwhile; ?>

  </section>
<?php endif; ?>


<?php
get_footer();
