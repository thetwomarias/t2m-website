<?php
/**
 * Menu functions
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */

/**
 * Register nav menus
 */
function gulp_wp_register_menus() {
	register_nav_menus(
		array(
			'primary' => __( 'Primary' ),
		)
	);
}
add_action( 'init', 'gulp_wp_register_menus' );

// Adding specific class to Menu links
function add_menuclass($ulclass) {
	return preg_replace('/<a/', '<a class="nav__link"', $ulclass, -1);
	}
add_filter('wp_nav_menu','add_menuclass');

define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
