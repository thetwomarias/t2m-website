<?php
/**
 * Script functions
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */

/**
 * Enqueue theme scripts
 */
function gulp_wp_theme_scripts() {

	/**
	 * Set a script handle prefix based on theme name.
	 * Note that this should be the same as the `themePrefix` var set in your Gulpfile.js.
	 */
	$theme_handle_prefix = 'theme-name';

	/**
	 * Enqueue common scripts.
	 */
	wp_enqueue_script( $theme_handle_prefix . '-scripts', get_template_directory_uri() . '/assets/js/' . $theme_handle_prefix . '.min.js?v=20230404', array( 'jquery' ), '1.0.0', true );

	// wp_enqueue_script( 'cookie', get_template_directory_uri() . '/assets/js/vendors/jquery.cookieBar.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'cookie', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.cookieBar/0.1.0/jquery.cookieBar.min.js', array ( 'jquery' ), 1.1, true);
	// wp_enqueue_script( 'cookie', get_template_directory_uri() . '/assets/js/vendors/js.cookie.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'jquery-cookie', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/3.0.1/js.cookie.min.js', array ( 'jquery' ), 1.1, true);

	// Get post template
		$template_file = get_page_template_slug();

		// If we have just one testimonial
		  	if($template_file == 'templates/amatller.php' ||
			  		$template_file == 'templates/honda.php' ||
			  		$template_file == 'templates/catalana-occidente.php' ||
			  		$template_file == 'templates/simon-coll.php' ||
			  		$template_file == 'templates/meta.php')
		  		wp_enqueue_script( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array ( 'jquery' ), 1.1, true);
		  	else // More than one testimonial
				wp_enqueue_script( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array ( 'jquery' ), 1.1, true);

		// Meta
		  	if($template_file == 'templates/meta.php'){
		  		wp_enqueue_script( 'meta', 'https://code.createjs.com/1.0.0/createjs.min.js', array ( 'jquery' ), 1.1, true);
		  		if(wp_is_mobile()){
					wp_enqueue_script( 'meta-1-mobile', get_template_directory_uri() . '/assets/js/case-studies/meta/index-mobile.min.js', array ( 'jquery' ), 1.1, true);
		  		}
		  		else{
					wp_enqueue_script( 'meta-1', get_template_directory_uri() . '/assets/js/case-studies/meta/index.min.js', array ( 'jquery' ), 1.1, true);
					wp_enqueue_script( 'meta-2', get_template_directory_uri() . '/assets/js/case-studies/meta/index2.min.js', array ( 'jquery' ), 1.1, true);
		  		}

				// if ( !wp_is_mobile() ) {
				// 	wp_enqueue_script( 'vimeo', 'https://player.vimeo.com/api/player.js', array ( 'jquery' ), 1.1, true);
		 	// 	}
		  	}

		// CS - Video Block
		  	if($template_file == 'templates/meta.php' || $template_file == 'templates/display-ads.php'){
		  		$video_block_vimeo_id = get_field('video_block_vimeo_id');
				if(isset($video_block_vimeo_id) && $video_block_vimeo_id!='')
				{
			  		if(!wp_is_mobile()){
			  			wp_enqueue_script( 'vimeo', 'https://player.vimeo.com/api/player.js', array ( 'jquery' ), 1.1, true);
			  		}
				}
		  	}

			// wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/vendors/slick.min.js', array ( 'jquery' ), 1.1, true);


	if(is_front_page()) {
		// wp_enqueue_script( 'vimeo', get_template_directory_uri() . '/assets/js/vendors/player.js', array ( 'jquery' ), 1.1, true);
		if ( !wp_is_mobile() ) {
		 	wp_enqueue_script( 'vimeo', 'https://player.vimeo.com/api/player.js', array ( 'jquery' ), 1.1, true);
		 }
		// wp_enqueue_script( 'vimeo-api', 'https://cdn.jsdelivr.net/npm/vimeo-jquery-api@0.10.3/dist/jquery.vimeo.api.min.js', array ( 'jquery, vimeo' ), 1.1, true);
	}
	else{
		wp_enqueue_script( 'paroller', 'https://cdnjs.cloudflare.com/ajax/libs/paroller.js/1.4.6/jquery.paroller.min.js', array ( 'jquery' ), 1.1, true);
		// wp_enqueue_script( 'paroller', get_template_directory_uri() . '/assets/js/vendors/jquery.paroller.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'videojs', 'https://cdnjs.cloudflare.com/ajax/libs/vide/0.5.1/jquery.vide.min.js', array ( 'jquery' ), 1.1, true);
		// wp_enqueue_script( 'videojs', get_template_directory_uri() . '/assets/js/vendors/jquery.vide.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'vide', 'https://cdnjs.cloudflare.com/ajax/libs/video.js/7.9.1/video.min.js', array ( 'jquery' ), 1.1, true);
		// wp_enqueue_script( 'vide', get_template_directory_uri() . '/assets/js/vendors/video.min.js', array ( 'jquery' ), 1.1, true);
	}

	// wp_enqueue_script( 'validate', get_template_directory_uri() . '/assets/js/vendors/jquery.validate.min.js', array ( 'jquery' ), 1.1, true);
	// wp_enqueue_script( 'validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js', array ( 'jquery' ), 1.1, true);
	// wp_enqueue_script( 'viewportchecker', get_template_directory_uri() . '/assets/js/vendors/jquery.viewportchecker.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'viewportchecker', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-viewport-checker/1.8.8/jquery.viewportchecker.min.js', array ( 'jquery' ), 1.1, true);
	// wp_enqueue_script( 'lottie', get_template_directory_uri() . '/assets/js/vendors/lottie.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'lottie', 'https://cdnjs.cloudflare.com/ajax/libs/lottie-web/5.7.0/lottie.min.js', array ( 'jquery' ), 1.1, true);
	// wp_enqueue_script( 'lazyload', get_template_directory_uri() . '/assets/js/vendors/lazyload.min.js', array ( 'jquery' ), 1.1, true);
	wp_enqueue_script( 'lazyload', 'https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/17.1.0/lazyload.min.js', array ( 'jquery' ), 1.1, true);

}
add_action( 'wp_enqueue_scripts', 'gulp_wp_theme_scripts' );

/*function to add async and defer attributes*/
function defer_js_async($tag){

	## 1: list of scripts to defer. (Edit with your script names)
	$scripts_to_defer = array('jquery.cookieBar.js', 'jquery.paroller.min.js', 'jquery.validate.min.js', 'jquery.vide.js', 'lottie.min.js');
	## 2: list of scripts to async. (Edit with your script names)
	$scripts_to_async = array('player.js');

	#defer scripts
	if (count($scripts_to_defer) > 0) {
		foreach($scripts_to_defer as $defer_script){
			if(!empty($tag) && !empty($defer_script))
			{
				if(true == strpos($tag, $defer_script ) )
				return str_replace( ' src', ' defer="defer" src', $tag );
			}
		}
	}
	#async scripts
	if (count($scripts_to_async) > 0) {
		foreach($scripts_to_async as $async_script){
			if(!empty($tag) && !empty($async_script))
			{
				if(true == strpos($tag, $async_script ) )
					return str_replace( ' src', ' async="async" src', $tag );
			}
		}
	}

	return $tag;
	}

add_filter( 'script_loader_tag', 'defer_js_async', 10 );


/* Disable external embeds */
	function disable_embeds_code_init() {

	 // Remove the REST API endpoint.
	 remove_action( 'rest_api_init', 'wp_oembed_register_route' );

	 // Turn off oEmbed auto discovery.
	 add_filter( 'embed_oembed_discover', '__return_false' );

	 // Don't filter oEmbed results.
	 remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

	 // Remove oEmbed discovery links.
	 remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

	 // Remove oEmbed-specific JavaScript from the front-end and back-end.
	 remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	 add_filter( 'tiny_mce_plugins', 'disable_embeds_tiny_mce_plugin' );

	 // Remove all embeds rewrite rules.
	 add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

	 // Remove filter of the oEmbed result before any HTTP requests are made.
	 remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
	}

	add_action( 'init', 'disable_embeds_code_init', 9999 );

	function disable_embeds_tiny_mce_plugin($plugins) {
	    return array_diff($plugins, array('wpembed'));
	}

	function disable_embeds_rewrites($rules) {
	    foreach($rules as $rule => $rewrite) {
	        if(false !== strpos($rewrite, 'embed=true')) {
	            unset($rules[$rule]);
	        }
	    }
	    return $rules;
	}
