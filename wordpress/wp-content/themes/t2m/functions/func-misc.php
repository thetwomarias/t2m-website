<?php

	//Page Slug Body Class
	function add_slug_body_class( $classes ) {
		global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_name;
		}

		return $classes;
	}

	add_filter( 'body_class', 'add_slug_body_class' );

	//Add Case Studies Body Class
	add_filter( 'body_class', 'custom_class' );
	function custom_class( $classes ) {
	  	if ( is_page_template( 'templates/meta.php' ) || is_page_template( 'templates/display-ads.php' ) || is_page_template( 'templates/simon-coll.php' ) || is_page_template( 'templates/honda.php' ) || is_page_template( 'templates/accelya.php' ) || is_page_template( 'templates/catalana-occidente.php' ) || is_page_template( 'templates/amatller.php' ) ) {

			$classes[] = 'case-study';
			$classes[] = get_field('project_id');
		}
		return $classes;
	}

	add_filter('body_class', 'append_language_class');
		function append_language_class($classes){
		$classes[] = ICL_LANGUAGE_CODE;  //or however you want to name your class based on the language code
		return $classes;
	}

	function wpassist_remove_block_library_css(){
		wp_dequeue_style( 'wp-block-library' );
	}
	add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );








