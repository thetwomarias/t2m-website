<?php
  // Variables
  $bodyClass = 'projects';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Proyectos que nos hacen felices</h1>
    </div>
  </section>

  <section class="project-block project-block--left project-block--web" id="project-simon-coll">
    <div class="project__text">
        <h4 class="project__title">Chocolates <br/>Simón Coll</h4>
        <h5 class="project__heading">Creación de website <br/>de E-commerce</h5>
        <p class="project__desc">Ayudamos a estos maestros del cacao a dar el salto al comercio digital desarrollando la UX, diseño, programación y publicando sus 400 referencias de chocolate. <br/>La boca agua.</p>
        <div class="project__dots project__dots--white"></div>
        <a class="link--cta" href="/case-studies/simon-coll.php"><span>Ver caso</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/simon-coll/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/simon-coll/slide-02.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/simon-coll/slide-03.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section>

  <section class="project-block project-block--right project-block--video" id="project-catalana-occidente" data-paroller-factor="-0.075" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Catalana Occidente</h4>
        <h5 class="project__heading">Creación de piezas de animación</h5>
        <p class="project__desc">Conceptualizamos y produjimos dos piezas de vídeo online para lanzar el Connected safety car, un nuevo servicio de apoyo al conductor. ¿Suena serio? <br/>Pues ya verás que no lo es.</p>
        <div class="project__dots project__dots--purple"></div>
        <a class="link--cta" href="/case-studies/catalana-occidente"><span>Ver caso</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed project__slide--video">
        <img src="/img/projects/catalana-occidente/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <img src="/img/projects/catalana-occidente/slide-02.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <img src="/img/projects/catalana-occidente/slide-03.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section>

  <section class="project-block project-block--left project-block--web" id="project-honda" data-paroller-factor="0.1" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Honda Motors <br/>Europe</h4>
        <h5 class="project__heading">Experiencia interactiva</h5>
        <p class="project__desc">¿Qué mejor manera de comprobar cómo funciona un cortador de césped que a vista de pájaro? Así lo hicimos en el Miimo Lawn Planner, un simulador de cortacésped interactivo para promocionar el de la vida real.</p>
        <div class="project__dots project__dots--purple"></div>
        <a class="link--cta" href="/case-studies/honda-miimo.php"><span>Ver caso</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/honda/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/honda/slide-02.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/honda/slide-03.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section>

  <section class="project-block project-block--right project-block--video" id="project-catalana-accelya">
    <div class="project__text">
        <h4 class="project__title">Accelya</h4>
        <h5 class="project__heading">Producción de vídeo online</h5>
        <p class="project__desc">Realizamos, desde la ideación hasta la edición, vídeos corporativos y de producto para este proveedor de servicios tecnológicos a las aerolíneas. Y el tiempo se nos pasa volando.</p>
        <div class="project__dots project__dots--red"></div>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed project__slide--video">
        <img src="/img/projects/accelya/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <img src="/img/projects/accelya/slide-02.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <img src="/img/projects/accelya/slide-03.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section>

  <section class="project-block project-block--left project-block--email" id="project-banners" data-paroller-factor="0.15" data-paroller-type="foreground" data-paroller-direction="vertical" >
    <div class="project__text">
        <h4 class="project__title">Banners</h4>
        <h5 class="project__heading">Para distintas marcas y agencias</h5>
        <p class="project__desc">Después de más de 1000 campañas online para marcas y agencias como Samsung, BMW, BBH o BMB, y de haber vivido la evolución desde el rich media hasta el social media, nos consideramos Licenciadísimos en el Arte del Banner.</p>
        <div class="project__dots project__dots--red"></div>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <div class="project__slide--bg" data-vide-bg="mp4: /videos/banners.mp4, poster:/img/case-studies/banners/slide-01.jpg" data-vide-options="poster-type: 'detect', loop: true, muted: true, position: 50% 50%, resizing: true;"></div>
      </div>
      <div class="project__slide project__slide--boxed">
        <div class="project__slide--bg" data-vide-bg="mp4: /videos/patek.mp4, poster:/img/case-studies/banners/slide-03.jpg" data-vide-options="poster-type: 'detect', loop: true, muted: true, position: 50% 50%, resizing: true;"></div>
      </div>
      <div class="project__slide project__slide--boxed">
        <div class="project__slide--bg" data-vide-bg="mp4: /videos/mcdonalds.mp4, poster:/img/case-studies/banners/slide-02.jpg" data-vide-options="poster-type: 'detect', loop: true, muted: true, position: 0% 0%, resizing: true;"></div>
      </div>
    </div>
  </section>

  <section class="project-block project-block--right project-block--email" id="project-newsletters" data-paroller-factor="-0.015" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Emailings</h4>
        <h5 class="project__heading">Para distintas marcas y agencias</h5>
        <p class="project__desc">El e-mail marketing ha pasado de patito feo a niña bonita de muchos negocios. Nosotros les ayudamos a que así sea creando plantillas desde cero, maquetando envíos e incluso enseñándoles a hacerlo ellos mismos.</p>
        <div class="project__dots project__dots--purple"></div>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/newsletters/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed">
        <img src="/img/projects/newsletters/slide-02.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section>



  <section class="clients appear">

    <h3 class="clients__heading">Clientes</h3>
    <div class="clients__wrapper pad">
      <div class="client"><img class="client__logo" src="/img/clients/simoncoll.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/redwood.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bbh.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/mc-saatchi.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/honda.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/vanquis.png" alt="" title="" /></div>

      <div class="client"><img class="client__logo" src="/img/clients/adam-eve.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/accelya.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/the-brooklyn-brothers.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/catalana-occidente.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bmb.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/facebook.png" alt="" title="" /></div>
    </div>

  </section>

  <section class="testimonials-block bottom-block">
    <div class="bottom-block__wrapper">

        <img src="/img/projects/quotes.svg" alt="Testimonials" title="Testimonials" />

        <div class="testimonials__slider">

          <div class="testimonials__slide">
            <h5 class="testimonials__quote appear">The Two Marias diseñaron y desarrollaron el e-commerce para nuestra marca de chocolate Simón Coll. Su experiencia y conocimientos técnicos hicieron que todo fuera muy fluido. También destacaría su estilo de gestión de proyectos, siempre profesional y organizado. El equipo estuvo también en todo momento accesible y en cada momento se adaptaron perfectamente a las necesidades específicas de nuestro negocio.</h5>
            <div class="testimonials__credits appear">
              <span class="testimonials__name">Maria Coll</span>
              <span class="testimonials__position">Directora de Marketing y Ventas, Simón Coll</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Realmente me ha encantado trabajar con The Two Marias en estos últimos años. Han sido capaces de hacer nuestras ideas realidad, con una gran cantidad de consejos prácticos sobre cómo lograr los objetivos del proyecto de manera eficiente y con un mínimo de estrés. No sé cómo lo hacen, pero se las arreglan para ser a la vez relajadas y agradables, así como precisas y eficientes, ¡todo al mismo tiempo! Es genial poder confiar en ellas como un Partner, especialmente cuando se trata de hacer malabares con varios proyectos a la vez.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">James Barter</span>
              <span class="testimonials__position">Product Manager, Honda Motor Europe - Power Products</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Trabajar con The Two Marias fue una experiencia muy positiva. Como cualquiera que haya creado su propia página web de agencia sabrá, hay muchas personas a las que gestionar y tener un Partner que comunica regularmente y cumple los plazos es de importancia crítica. El nuestro fue un concepto muy inusual en cuanto a producción y había una serie de retos que ellas lograron, con el mínimo esfuerzo y además aportando soluciones inteligentes. Para el CMS añadieron una función de ‘drag and drop’, lo que hace que actualizar el contenido sea una delicia. Puedo recomendar T2Ms como un Partner excelente a todos aquellos que buscan externalizar la producción de sus proyectos digitales.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Jez Dutton</span>
              <span class="testimonials__position">Redwood, Director de Innovación</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Es fantástico trabajar con The Two Marias. Desde el primer día nos encantó la manera como trabajan, tanto desde un punto de vista creativo, como a nivel de velocidad de entrega. Uno siempre sabe que va a obtener con ellas el mejor trabajo posible - la manera como se describen 'producción digital en buenas manos', ¡es totalmente cierta!</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Richard Atkins</span>
              <span class="testimonials__position">BBH London, Director de Producción Digital y Partner</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">The Two Marias han trabajado con BMB en varios proyectos con diferentes niveles de complejidad y urgencia. Su enfoque es siempre coherente - son muy profesionales, amables y sobre todo fiables. Siempre tenemos confianza absoluta cuando trabajamos en un proyecto con The Two Marias, ¡independientemente de los obstáculos que puedan aparecer por el camino! Realmente las vemos como una extensión del equipo de producción de BMB.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Melanie Lowings</span>
              <span class="testimonials__position">BMB, (Ex) Directora de Producción</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Hemos disfrutado plenamente trabajar con The Two Marias durantes los últimos 7 años. Son fiables, trabajadoras y siempre se las ingenian para dar la vuelta a un proyecto en un tiempo récord, a la vez manteniendo la calma y, sobretodo, ¡el buen humor!</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Lucy Hollingsworth</span>
              <span class="testimonials__position">Arthur London, Account Director</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">He trabajado con The Two Marias en unas cuantas agencias en los últimos ocho años. Son humildes, grandes en lo que hacen y están siempre disponibles para ofrecer un apoyo más allá de lo que una espera. Están enfocadas a detalle, trabajan duro y saben encontrar a los mejores profesionales para cada proyecto. Han sido un gran Partner para mí durante estos años, y lo continuarán siendo en los años venideros.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Sian Kelly Guy</span>
              <span class="testimonials__position">Leagas Delaney London, (Ex) Directora de Producción</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">The Two Marias se asociaron con nosotros en un proyecto complicado de desarrollo técnico, que se extendió por varios meses debido a su complejidad. Sus entregas cumplieron nuestros requerimientos a tiempo y dentro de presupuesto. Su estilo de gestión también nos fue muy útil, ya que hizo que los sintiéramos como una verdadera extensión de nuestro equipo.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Caroline Davison</span>
              <span class="testimonials__position">Elvis London, Directora General</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Trabajar con The Two Marias significa que uno no tiene que preocuparse de ningún aspecto del trabajo del que se estén encargando. Van a hacer siempre todo lo posible para entregar a tiempo y con todo bien testeado. También tienen toneladas de paciencia para hacer frente a los inevitables cambios de última hora, ¡que siempre llegan!</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Cara Bermingham</span>
              <span class="testimonials__position">The Co-Op, Directora de Producción Agile</span>
            </div>
          </div>

        </div>

    </div>
  </section>



</main>

<?php include 'templates/layout/footer.php'; ?>
