<?php
  // Variables
  $bodyClass = 'home';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Producción digital<br/>y paz mental para marcas<br/>y agencias desde 2008.</h1>
      <h2 class="page-header__subtitle appear"><span class="icon-dash-line"></span><span class="currently appear appear-ltr">Estamos trabajando con<strong>Honda Europe</strong></span></h2>
    </div>
  </section>

  <div id="js-wrapper">
    <section class="home-projects" id="home-projects">

        <div class="home-projects__item home-projects__item--blue appear" id="home-project-simoncoll" data-vp-offset="10">
          <a class="link--invisible" href="/"></a>
          <h2 class="home-projects__title">Chocolates<br/>Simon Coll</h2>
          <div class="home-projects__desc">Nuestro último (y delicioso) <br/>proyecto de e-commerce.</div>
          <div class="home-projects__link">
            <a href="/case-studies/simon-coll.php" class="link--cta"><span>Ver Caso</span></a>
          </div>
        </div>

        <div class="home-projects__item home-projects__item--white appear" id="home-project-catalana" data-vp-offset="10">
          <a class="link--invisible" href="/"></a>
          <h2 class="home-projects__title">Catalana <br/>Occidente</h2>
          <div class="home-projects__desc">Producción y puesta en el <br/>aire de una serie de vídeos <br/>corporativos.</div>
          <div class="home-projects__link">
            <a href="/case-studies/catalana-occidente.php" class="link--cta"><span>Ver Caso</span></a>
          </div>
        </div>

        <div class="home-projects__item home-projects__item--blue appear" id="home-project-banners" data-vp-offset="10">
          <a class="link--invisible" href="/"></a>
          <h2 class="home-projects__title">Banners</h2>
          <div class="home-projects__desc">De todas las formas, colores, <br/>tecnologías e idiomas que <br/>se te puedan ocurrir.</div>
          <div class="home-projects__link">
            <a href="/proyectos.php" class="link--cta"><span>Ver Más</span></a>
          </div>
        </div>

    </section>

    <div class="red-triangle-wrapper">
      <img class="red-triangle" src="/img/backgrounds/corner-red.svg" alt="" />
    </div>

  </div>


  <div id="home-services-wrapper">

    <section class="home-services appear">
      <div class="home-services__block">
        <ul class="home-services__list">
          <li class="home-services__item">Websites</li>
          <li class="home-services__item">Banners</li>
          <li class="home-services__item">E-mailings</li>
          <li class="home-services__item">Vídeo online</li>
          <li class="home-services__item">Animación</li>
        </ul>
      </div>
      <div class="home-services__block">
        <div class="home-services__text">
          <strong>The Two Marias</strong> somos una <br/>productora digital en la que diseñamos <br/>y desarrollamos experiencias,<br/>
          contenidos y campañas online para <br/>anunciantes y agencias que quieren <br/>el trabajo bien hecho, sin dolores<br/>de cabeza.
        </div>
      </div>
    </section>

  </div>

  <section class="clients appear">

    <h3 class="clients__heading">Clientes</h3>
    <div class="clients__wrapper pad">
      <div class="client"><img class="client__logo" src="/img/clients/simoncoll.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/redwood.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bbh.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/mc-saatchi.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/honda.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/vanquis.png" alt="" title="" /></div>

      <div class="client"><img class="client__logo" src="/img/clients/adam-eve.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/accelya.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/the-broklyn-brothers.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/catalana-occidente.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bmb.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/facebook.png" alt="" title="" /></div>
    </div>

  </section>

  <section class="home-mariismos">

    <div class="home-mariismos__slider">

      <div class="mariismos__slide mariismos-likes">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/likes.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title">The Two Mariismos</h4>
          <h5 class="mariismos__heading">Lo hemos hecho <br/>un millón de veces. <br/>Y nos sigue encantando.</h5>
          <p class="mariismos_desc">Hemos trabajado para clientes globales y locales en producciones digitales de distintos tipos. Tenemos la experiencia de toda una década y la energía del primer día.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-mamushka">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/mamushka.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title">The Two Mariismos</h4>
          <h5 class="mariismos__heading">El tamaño <br/>sí importa</h5>
          <p class="mariismos_desc">Y ser pequeños nos permite ser ágiles, flexibles y extra-cuidadosos con cada proyecto. <br/>Trabajamos mucho online, ahorrando reuniones, tiempo y dinero.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-mundo">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/mundo.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title">The Two Mariismos</h4>
          <h5 class="mariismos__heading">Con los pies en <br/>Barcelona y la cabeza <br/>en el mundo.</h5>
          <p class="mariismos_desc">Nuestra experiencia internacional nos ha dado las herramientas para poder manejarnos con cualquier país. Cogemos el teléfono en inglés, alemán, castellano, catalán y chino.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-bocas">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/bocas.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title">The Two Mariismos</h4>
          <h5 class="mariismos__heading">“Profesionales, <br/>eficientes y amables”</h5>
          <p class="mariismos_desc">Lo dicen nuestros clientes. ¿Será nuestra obsesión por los detalles? ¿El buen humor, incluso en lunes? ¿O que nunca entregamos nada tarde? Pues un poco de todo, suponemos :-)</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-karma">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/karma.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title">The Two Mariismos</h4>
          <h5 class="mariismos__heading">Aplicamos <br/>“metodologías” kármicas</h5>
          <p class="mariismos_desc">Hacer buen trabajo hoy nos traerá alegrías mañana. Así de simple y a-tecnológica es la metodología que aplicamos cada día.</p>
        </div>
      </div>

      <span class="pagingInfo"></span>

    </div>

  </section>

  <section class="home-team">
    <div class="home-team__wrapper">
      <div class="home-team__text">
        <p class="home-team__heading">Sip. Somos dos Marias.<br/>
          Pero también hay un Alex, <br/>una Paula o un Xavi.</p>

        <a href="#" class="link--cta"><span>Conócenos</span></a>
      </div>
      <div class="home-team__image appear">
        <img src="/img/home/home-team.jpg" alt="Equipo The Two Marias" title="Equipo The Two Marias" />
      </div>
    </div>
  </section>

  <section class="home-bottom bottom-block">
    <div class="bottom-block__wrapper">

      <div class="bottom-block__content">
        <img src="/img/home/home-pressu.png" alt="Presupuesto en menos de 24 horas" title="Presupuesto en menos de 24 horas" />

        <div class="bottom-block__text">
          <h6 class="bottom-block__heading">¿Te da buen feeling <br/>todo esto?</h6>
          <p class="bottom-block__desc">Escríbenos a <a href="mailto:proyectos@thetwomarias.com">proyectos@thetwomarias.com</a><br/>
            y en menos de 24 horas te mandamos presupuesto.</p>
        </div>

        <div class="bottom-block__image">
          <object type="image/svg+xml" data="/img/presupuesto.svg" preserveAspectRatio="xMidYMid meet">
            <img src="/img/home/home-bottom-bag.png" alt="Presupuesto en menos de 24 horas" title="Presupuesto en menos de 24 horas" />
          </object>

        </div>

      </div>

    </div>

  </section>

</main>

<?php include 'templates/layout/footer.php'; ?>
