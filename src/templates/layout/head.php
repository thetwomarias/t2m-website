<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>The Two Marias | New website</title>
  <meta name="description" content="">

  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="manifest" href="/img/site.webmanifest">
  <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#000000">
  <meta name="msapplication-TileColor" content="#ffca02">
  <meta name="theme-color" content="#ffca02">

  <link rel="stylesheet" href="/css/styles.css">

  <!-- Custom Fons -->
  <link href="https://file.myfontastic.com/vGbzw2M4rhvQkY2DPxVipa/icons.css" rel="stylesheet">

</head>
<body class="<?php echo $bodyClass ?>">
<div class="cookie-message background--blue" style="display:none;"><p>We know you’re probably not reading this, and heading directly to the Accept button (for which, thanks!). But let us tell you a little about how we use cookies anyway (because there is very little to say). In short: we use Google Analytics cookies to measure traffic to our website - nothing else. If you continue browsing, we’ll just assume your cool with that. But if you’re still reading, and you want to read more <a href="/legal.php"><strong>click here</strong></a>.</p><a class="cookie-close link--cta link--white"><span>Got it</span></a></div>
