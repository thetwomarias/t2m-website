<footer class="footer">
  <div class="footer__container">
    <div class="footer__wrapper">
      <div class="footer__logo">
      <a href="/" class="logo__link"></a>
        <img src="/img/logo-white.svg" alt="The Two Marias Logo" title="The Two Marias" />
      </div>
      <div class="footer__content">
        <div class="footer__col">
          <div class="footer__row">
            <p class="footer__title">Telephone</p>
            <p class="footer__subtitle">+34 934 150 893</p>
          </div>
          <div class="footer__row">
            <p class="footer__title">Address</p>
            <p class="footer__subtitle">Carretera BV-1274, Km1,<br/>08225 Terrassa,<br/>Barcelona</p>
          </div>
        </div>
        <div class="footer__col">
          <div class="footer__row">
            <p class="footer__title">e-mail</p>
            <p class="footer__subtitle"><a href="mailto:hola@thetwomarias.com" class="footer-link--cta footer-link--email"><span>hola@thetwomarias.com</span></a></p>
          </div>
          <div class="footer__row">
            <p class="footer__title">Follow us</p>
            <div class="footer__subtitle footer__social">
              <ul class="social__list">
              <li class="social__item">
                <a class="social__link" href="https://www.linkedin.com/company/thetwomarias/" target="_blank"><span>Lk</span></a>
              </li>
              <li class="social__item">
                <a class="social__link" href="https://www.instagram.com/wearethetwomarias/" target="_blank"><span>Ig</span></a>
              </li>
              <li class="social__item">
                  <a class="social__link" href="https://www.facebook.com/thetwomarias/" target="_blank"><span>Fb</span></a>
                </li>
                <li class="social__item">
                  <a class="social__link" href="https://twitter.com/TheTwoMarias" target="_blank"><span>Tw</span></a>
                </li>
                
                <!-- <li class="social__item">
                  <a class="social__link" href="https://www.instagram.com"><span>Ig</span></a>
                </li> -->
              </ul>
            </div>
          </div>
        </div>
        <div class="footer__col footer__newsletter">
          <div class="footer__row">
            <p class="footer__title">Sign up to our T2M newsletter</p>
            <p class="footer__subtitle">We’ll only send it when we have something interesting to tell you, promise.</p>
            <div class="footer__form">

              <!-- Begin Mailchimp Signup Form -->

              <div id="mc_embed_signup">
              <form action="https://thetwomarias.us6.list-manage.com/subscribe/post?u=c7c4d8c89541827f53d49532c&amp;id=103c5c5794" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"  target="_blank">
                <div id="mc_embed_signup_scroll">
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" autocomplete="off" placeholder="Your email address" required data-msg="Are you sure you typed in the email correctly?">
                  <div class="agreement">
                    <input type="checkbox" value="1" name="subscribe" class="checkbox" required  data-msg="Guess you forgot to check the box?">
                    <label>I agree to the terms in the <a href="/legal.php" class="footer-link--cta"><span>Privacy Policy</span></a>, and that you will only use my data to send me the newsletter.</label>
                    <span class="error error-text"></span>
                  </div>
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c7c4d8c89541827f53d49532c_103c5c5794" tabindex="-1" value=""></div>
                  <div class="clear"><i class="link--cta"><span>Send</span><input type="submit" value="" name="Send" id="mc-embedded-subscribe" class="button"/></i></div>
                </div>
              </form>
              </div>

              <div class="footer__send">
                <img src="/img/footer/send.svg" alt="Send" title="Send"/>
              </div>

              <!--End mc_embed_signup-->

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="scroll-btn scroll-up"><span>Scroll up</span></div>

  </div>
  <div class="copyright">
    <div class="copyright__text background--blue">© Copyright 2019 and beyond. <a href="/legal.php" class="footer-link--cta"><span>Privacy Policy</span></a> | <a href="/sitemap.php" class="footer-link--cta"><span>Sitemap</span></a></div>
    <div class="copyright__space"></div>
  </div>

  </div>

</footer>

<!-- jQuery -->
<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<!-- Vendor JS -->
<!-- <script src="/js/vendor/ScrollMagic/ScrollMagic.min.js"></script>
<script src="/js/vendor/ScrollMagic/debug.addIndicators.min.js"></script>
<script src="/js/vendor/TweenMax.min.js"></script>
<script src="/js/vendor/animation.gsap.js"></script> -->
<script src="/js/vendor/jquery.viewportchecker.min.js"></script>
<script src="/js/vendor/slick.min.js"></script>
<script src="/js/vendor/video.js"></script>
<script src="/js/vendor/jquery.vide.js"></script>
<script src="/js/vendor/jquery.paroller.min.js"></script>
<script src="/js/vendor/jquery.cookieBar.js"></script>
<script src="/js/vendor/jquery.validate.min.js"></script>
<script src="/js/vendor/lottie.js"></script>


<!-- Custom Scripts -->
<script src="/js/scripts.js"></script>

</body>
</html>
