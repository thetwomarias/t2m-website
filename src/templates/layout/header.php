<header>

  <div class="logo appear">
    <a href="/" class="logo__link"></a>
    <div id="bm" class="logo-image logo-animated" style="display:none;"></div>
    <!-- <object class="logo-image logo-animated" style="display:none" type="image/svg+xml" data="/img/logo-animated.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/logo-animated.svg" alt="The Two Marias" title="The Two Marias"/>
    </object> -->
    <object class="logo-image logo-static" type="image/svg+xml" data="/img/logo-static.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/logo-static.svg" alt="The Two Marias" title="The Two Marias"/>
    </object>
    <object class="logo-image logo-mobile" style="display:none" type="image/svg+xml" data="/img/logo-mobile.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/logo-mobile.svg" alt="The Two Marias" title="The Two Marias"/>
    </object>
  </div>

  <div class="hamburger">
    <div class="hamburger-icon">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <nav role="navigation">

    <ul class="nav">
      <li class="nav__item">
        <a class="nav__link nav__link--services" href="/services.php"><span>Services</span></a>
      </li>
      <li class="nav__item">
        <a class="nav__link nav__link--projects" href="/projects.php"><span>Projects</span></a>
      </li>
      <li class="nav__item">
        <a class="nav__link nav__link--team" href="/team.php"><span>Team</span></a>
      </li>
      <li class="nav__item nav__item--last">
        <a class="nav__link nav__link--contact" href="/contact.php"><span>Say hi!</span></a>
      </li>
    </ul>

    <!-- <ul class="lang-switcher__list">
      <li class="lang-switcher--current"><a href="" class="lang-switcher__link">Eng</a><i class="icon-arrow-down"></i></li>
    </ul> -->

  </nav>

  <div class="nav--sticky">
    <div class="nav--sticky-content">
      <div class="logo-small">
        <a href="/" class="logo__link"></a>
        <img src="/img/logo-small.svg" alt="The Two Marias" title="The Two Marias"/>
      </div>

      <div class="hamburger">
        <div class="hamburger-icon">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <nav class="nav">
        <li class="nav__item">
          <a class="nav__link nav__link--services" href="/services.php"><span>Services</span></a>
        </li>
        <li class="nav__item">
          <a class="nav__link nav__link--projects" href="/projects.php"><span>Projects</span></a>
        </li>
        <li class="nav__item">
          <a class="nav__link nav__link--team" href="/team.php"><span>Team</span></a>
        </li>
        <li class="nav__item">
          <a class="nav__link nav__link--contact" href="/contact.php"><span>Say hi!</span></a>
        </li>
        <!-- <ul class="lang-switcher__list">
          <li class="lang-switcher--current"><a href="" class="lang-switcher__link">Eng</a><i class="icon-arrow-down"></i></li>
        </ul> -->
      </nav>
    </div>
  </div>


</header>
