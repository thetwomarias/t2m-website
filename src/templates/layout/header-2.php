<header>

  <div class="logo appear">
    <object class="logo-image" type="image/svg+xml" data="/img/logo-animated.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/logo-animated.svg" alt="The Two Marias" title="The Two Marias"/>
    </object>
  </div>


  <nav role="navigation">

    <ul class="nav">
      <li class="nav__item">
        <a class="nav__link" href="">Servicios</a>
      </li>
      <li class="nav__item">
        <a class="nav__link" href="">Proyectos</a>
      </li>
      <li class="nav__item">
        <a class="nav__link" href="">Equipo</a>
      </li>
      <li class="nav__item">
        <a class="nav__link" href="">Contacto</a>
      </li>
    </ul>

    <ul class="lang-switcher__list">
      <li class="lang-switcher--current"><a href="" class="lang-switcher__link">Cas</a><i class="icon-arrow-down"></i></li>
    </ul>

  </nav>

  <div class="nav--sticky">
    <div class="nav--sticky-content">
      <div class="logo-small">
        <img src="/img/logo-small.svg" alt="The Two Marias" title="The Two Marias"/>
      </div>
      <nav class="nav">
        <li class="nav__item">
          <a class="nav__link" href="">Servicios</a>
        </li>
        <li class="nav__item">
          <a class="nav__link" href="">Proyectos</a>
        </li>
        <li class="nav__item">
          <a class="nav__link" href="">Equipo</a>
        </li>
        <li class="nav__item">
          <a class="nav__link" href="">Contacto</a>
        </li>
        <ul class="lang-switcher__list">
          <li class="lang-switcher--current"><a href="" class="lang-switcher__link">Cas</a><i class="icon-arrow-down"></i></li>
        </ul>
      </nav>
    </div>
  </div>


</header>
