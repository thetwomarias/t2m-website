<?php
  // Variables
  $bodyClass = 'projects';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Projects that make us proud.</h1>
    </div>
  </section>

  <section class="project-block project-block--left project-block--web" id="project-simon-coll">
    <div class="project__text">
        <h4 class="project__title">Simón Coll</h4>
        <h5 class="project__heading">E-commerce website</h5>
        <p class="project__desc">We helped these master chocolatiers make the leap to digital commerce by working on the UX, design and development of their new website. There are over 400 product detail pages, and every one made our mouth water.</p>
        <div class="project__dots"></div>
        <a class="link--cta" href="/case-studies/simon-coll.php"><span>View case study</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <a href="/case-studies/simon-coll.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/simon-coll/slide-01.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/simon-coll/slide-01-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/simon-coll/slide-01-sm.jpg" />
        </picture>
      </div>
      <div class="project__slide project__slide--boxed">
      <a href="/case-studies/simon-coll.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/simon-coll/slide-02.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/simon-coll/slide-02-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/simon-coll/slide-02-sm.jpg" />
        </picture>
      </div>
      <div class="project__slide project__slide--boxed">
      <a href="/case-studies/simon-coll.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/simon-coll/slide-03.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/simon-coll/slide-03-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/simon-coll/slide-03-sm.jpg" />
        </picture>
      </div>
    </div>
  </section>

  <!-- <section class="project-block project-block--right project-block--video" id="project-catalana-occidente" data-paroller-factor="-0.075" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Catalana Occidente</h4>
        <h5 class="project__heading">Animation</h5>
        <p class="project__desc">We conceptualised and produced two online videos designed to launch the Connected safety card, a new digital support service for drivers. Sounds a bit dry, doesn’t it? Worry not, we managed to find their funny…</p>
        <div class="project__dots project__dots--purple"></div>
        <a class="link--cta" href="/case-studies/catalana-occidente.php"><span>View case study</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed project__slide--video">
        <a href="/case-studies/catalana-occidente.php" class="link--inner"></a>
        <img src="/img/projects/catalana-occidente/slide-01.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
        <a href="/case-studies/catalana-occidente.php" class="link--inner"></a>
        <img src="/img/projects/catalana-occidente/slide-02.jpg" class="slide__image" alt=""/>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
        <a href="/case-studies/catalana-occidente.php" class="link--inner"></a>
        <img src="/img/projects/catalana-occidente/slide-03.jpg" class="slide__image" alt=""/>
      </div>
    </div>
  </section> -->

  <section class="project-block project-block--right project-block--web" id="project-honda" data-paroller-factor="0.1" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Honda Motors <br/>Europe</h4>
        <h5 class="project__heading">Interactive experience</h5>
        <p class="project__desc">What better way to see how a lawnmower works than from a bird’s eye view? We created an interactive lawnmower simulator to bring to life the incredible features of the new Miimo Lawn Planner. Put your feet up, and watch it ride…</p>
        <div class="project__dots project__dots--purple"></div>
        <a class="link--cta" href="/case-studies/honda-miimo.php"><span>View case study</span></a>
    </div>
    <div class="project__slider">
      <div class="project__slide project__slide--boxed">
        <a href="/case-studies/honda-miimo.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/honda/slide-01.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/honda/slide-01-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/honda/slide-01-sm.jpg" />
        </picture>
      </div>
      <div class="project__slide project__slide--boxed">
        <a href="/case-studies/honda-miimo.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/honda/slide-02.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/honda/slide-02-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/honda/slide-02-sm.jpg" />
        </picture>
      </div>
      <div class="project__slide project__slide--boxed">
        <a href="/case-studies/honda-miimo.php" class="link--inner"></a>
        <picture>
          <source srcset="/img/projects/honda/slide-03.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/honda/slide-03-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/honda/slide-03-sm.jpg" />
        </picture>
      </div>
    </div>
  </section>

  <section class="project-block project-block--left project-block--video" id="project-accelya" data-paroller-factor="0.02" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Accelya</h4>
        <h5 class="project__heading">Online video production</h5>
        <p class="project__desc">This leading provider of technological services to airlines asked us to produce a series of corporate and product videos to be showcased at international trade events in Dallas, Dubai and Monaco. We carried out the production of the videos from first idea to final edit. And the time just flew by!</p>
        <div class="project__dots project__dots--red"></div>
        <a class="link--cta" href="/case-studies/accelya.php"><span>View case study</span></a>
    </div>
    <div class="project__slider" id="accelya-slider">
      <div class="project__slide project__slide--boxed project__slide--video">
      <div class="video__slide--wrapper">
        <video id="video-accelya-one" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" muted width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-1.jpg'>
          <source src="/videos/accelya-one-mobile.mp4"  type='video/mp4' media="all and (max-width:576px)" />
          <source src="/videos/accelya-one.mp4" type='video/mp4' />
          <source src="/videos/accelya-one.webm" type='video/webm' />
        </video>
      </div>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <div class="video__slide--wrapper">
      <video id="video-accelya-two" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" muted width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-2.jpg'>
          <source src="/videos/accelya-two.mp4" type='video/mp4' />
          <source src="/videos/accelya-two.webm" type='video/webm' />
        </video>
      </div>
      </div>
      <div class="project__slide project__slide--boxed project__slide--video">
      <div class="video__slide--wrapper">
        <video id="video-accelya-three" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" muted width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-3.jpg'>
          <source src="/videos/accelya-three-mobile.mp4"  type='video/mp4' media="all and (max-width:576px)" />
          <source src="/videos/accelya-three.mp4" type='video/mp4' />
          <source src="/videos/accelya-three.webm" type='video/webm' />
        </video>
      </div>
      </div>
    </div>
  </section>

  <section class="project-block project-block--right project-block--email" id="project-banners" data-paroller-factor="0.15" data-paroller-type="foreground" data-paroller-direction="vertical" >
    <div class="project__text">
        <h4 class="project__title">Banners</h4>
        <h5 class="project__heading">For various brands & agencies</h5>
        <p class="project__desc">We’ve worked on over 1000 digital display campaigns, for brands such as Samsung and BMW, and an even wider array of agencies, from BBH to BMB. And we’ve survived the shift from rich media to social media along the way. So you’ll forgive us if we consider ourselves 10th-dan blacks belts in the way of the banner.</p>
        <div class="project__dots project__dots--red"></div>
    </div>
    <div class="project__slider" id="banners-slider">
      <div class="project__slide project__slide--boxed">
        <!-- <div class="project__slide--bg" data-vide-bg="mp4: /videos/banners-mobile.mp4, poster:/img/projects/banners/slide-01.jpg" data-vide-options="poster-type: 'jpg', loop: true, muted: true, autoplay: true, position: 50% 50%, resizing: true;"></div> -->
        <video src="/videos/banners-mobile.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video>
      </div>
      <div class="project__slide project__slide--boxed">
        <!-- <div class="project__slide--bg" data-vide-bg="mp4: /videos/patek.mp4, poster:/img/projects/banners/slide-03.jpg" data-vide-options="poster-type: 'jpg', loop: true, muted: true, autoplay: true, position: 50% 50%, resizing: true;"></div> -->
        <video src="/videos/patek.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video>
      </div>
      <div class="project__slide project__slide--boxed">
        <!-- <div class="project__slide--bg" data-vide-bg="mp4: /videos/mcdonalds-mobile.mp4, poster:/img/projects/banners/slide-02.jpg" data-vide-options="poster-type: 'jpg', loop: true, muted: true, autoplay: true, position: 0% 0%, resizing: true;"></div> -->
        <video src="/videos/mcdonalds-mobile.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video>
      </div>
      <div class="project__slide project__slide--boxed">
        <!-- <div class="project__slide--bg" data-vide-bg="mp4: /videos/barclays-mobile.mp4, poster:/img/projects/banners/slide-04.jpg" data-vide-options="poster-type: 'jpg', loop: true, muted: true, autoplay: true, position: 0% 0%, resizing: true;"></div> -->
        <video src="/videos/barclays-mobile.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video> 
      </div>
    </div>
  </section>

  <section class="project-block project-block--left project-block--email" id="project-newsletters" data-paroller-factor="-0.05" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="project__text">
        <h4 class="project__title">Newsletters</h4>
        <h5 class="project__heading">For various brands & agencies</h5>
        <p class="project__desc">E-mail marketing has gone from ugly duckling to beautiful swan in many marketing departments. We like to think we had in a hand in at least a few of those transformations…We help companies get there not just by creating flexible templates from scratch and designing best-in-class branded emails - we can also teach them how to do it for themselves.</p>
        <div class="project__dots"></div>
    </div>
    <div class="project__slider" id="newsletters-slider">
      <div class="project__slide project__slide--boxed">
      <picture>
          <source srcset="/img/projects/newsletters/slide-01.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/newsletters/slide-01-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/newsletters/slide-01-sm.jpg" />
        </picture>
      </div>
      <div class="project__slide project__slide--boxed">
      <source srcset="/img/projects/newsletters/slide-02.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/newsletters/slide-02-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/newsletters/slide-02-sm.jpg" />
      </div>
      <div class="project__slide project__slide--boxed">
        <source srcset="/img/projects/newsletters/slide-03.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/newsletters/slide-03-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/newsletters/slide-03-sm.jpg" />
      </div>

      <div class="project__slide project__slide--boxed">
        <source srcset="/img/projects/newsletters/slide-04.jpg" media="(min-width: 991px)" />
          <source srcset="/img/projects/newsletters/slide-04-md.jpg" media="(min-width: 576px)" />
          <img src="/img/projects/newsletters/slide-04-sm.jpg" />
      </div>

    </div>
  </section>



  <section class="clients appear">

    <h3 class="clients__heading">Clients</h3>
    <div class="clients__wrapper pad">
      <div class="client"><img class="client__logo" src="/img/clients/simoncoll.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/redwood.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bbh.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/mc-saatchi.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/honda.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/vanquis.png" alt="" title="" /></div>

      <div class="client"><img class="client__logo" src="/img/clients/adam-eve.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/accelya.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/the-brooklyn-brothers.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/catalana-occidente.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bmb.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/facebook.png" alt="" title="" /></div>
    </div>

  </section>

  <section class="testimonials-block bottom-block">
    <div class="bottom-block__wrapper">

        <img src="/img/projects/quotes.svg" alt="Testimonials" title="Testimonials" />

        <div class="testimonials__slider">

          <div class="testimonials__slide">
            <h5 class="testimonials__quote appear">The Two Marias designed and developed the e-commerce website for our chocolate company Simón Coll. Their seniority and technical expertise made the project very smooth. We were also impressed with their very professional and organised Project Management style. At the same time, their entire team was always approachable and adjusted effortlessly to the specific needs of our business.</h5>
            <div class="testimonials__credits appear">
              <span class="testimonials__name">Maria Coll</span>
              <span class="testimonials__position">Simón Coll, Marketing and Sales Director</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">I have really enjoyed working with The Two Marias in the last few years. They have managed to bring our ideas to life, with lots of practical guidance on how to achieve project objectives efficiently and with minimal stress. I don’t know how they do it, but they somehow manage to be both relaxed and friendly, as well as precise and efficient at the same time! It’s great to be able to rely on them as a partner, especially when trying to juggle various projects.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">James Barter</span>
              <span class="testimonials__position">Honda Motor Europe, Product Manager</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Working with The Two Marias was a really positive experience. As anyone who has created their own internal agency site will know, there are many people to keep on track and having a partner that communicates regularly and hits deadlines is critically important. This was a really unusual concept to deliver and there were a number of challenges that they managed with minimum fuss and smart solutions. The CMS even has a lovely drag and drop feature which makes updating the content a delight. I can fully recommend T2Ms as a great partner for anyone looking to outsource production of their digital projects.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Jez Dutton</span>
              <span class="testimonials__position">Redwood, Chief Innovation Officer</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">The Two Marias are great to work with.. From day one they knocked it out the park, both from a creative standpoint as well as speed of delivery. You always know you'll get the best work possible - a 'safe pair of hands' doesn't do them enough justice.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Richard Atkins</span>
              <span class="testimonials__position">BBH London, Head of Digital Production & Partner</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">The Two Marias have worked with BMB across projects of varying complexity and urgency. Their approach is always consistent – they’re professional, friendly and above all reliable. We always have absolute confidence when working on a project with the Two Marias, regardless of any curveballs that may come our way! We really do see the Two Marias as an extension of the BMB production team.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Melanie Lowings</span>
              <span class="testimonials__position">BMB, (Former) Production Director</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">We have thoroughly enjoyed working with The Two Marias for the past 7 years. They are reliable, hard-working and can always turn around a job in record time whilst still keeping calm and, importantly, friendly!</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Lucy Hollingsworth</span>
              <span class="testimonials__position">Arthur London, Account Director</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">It was an absolute pleasure working with The Two Marias - they are personable, efficient, and very open to ideas. Thanks to their hard work we were able to deliver an exceptional HTML5 build on time and on budget.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Stephen Mead</span>
              <span class="testimonials__position">adam&eveDDB, Integrated Producer</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">I’ve worked with The Two Marias at a selection of agencies now for the past eight years. They are straight forward, great at what they do and always on hand to offer support over and above. Detail orientated, hard working and strong at finding the best talent. They’ve been a great partner for me over the years, and will be for the years to come.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Sian Kelly Guy</span>
              <span class="testimonials__position">Leagas Delaney London, (Former) Operations Director</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">The Two Marias partnered with us on a complicated technical build project that spanned several months due to its complexity. They delivered against our requirements on time and on budget. Their style of management also stood us in good stead, they really did feel like a true extension of our team.</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Caroline Davison</span>
              <span class="testimonials__position">Elvis, Managing Partner</span>
            </div>
          </div>

          <div class="testimonials__slide">
            <h5 class="testimonials__quote">Working with The Two Marias means I don't have to worry about whatever aspect of the job they are taking care of. They'll do whatever it takes to deliver something on time and with everything tested. They also must have tons of patience to deal with the inevitable last minute changes!</h5>
            <div class="testimonials__credits">
              <span class="testimonials__name">Cara Bermingham</span>
              <span class="testimonials__position">The Co-op, Head of Agile Delivery.</span>
            </div>
          </div>

        </div>

    </div>
  </section>



</main>

<?php include 'templates/layout/footer.php'; ?>

<script>
    $(document).ready(function () {
            console.log("Projects!");
            if (window.location.href.indexOf('#accelya') > -1) {
              $('#accelya-slider').slick("slickNext");
              $('html, body').animate({
                  scrollTop: $("#project-accelya").offset().top - 150
              }, 2000);
            }
    });
</script>
