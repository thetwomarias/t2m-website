<?php
  // Variables
  $bodyClass = 'sitemap';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Sitemap</h1>
    </div>
  </section>

  <section class="sitemap-content appear">
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/">Home</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/services.php">Services</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/projects.php">Projects</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/case-studies/simon-coll.php">Projects / Simón Coll</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/case-studies/honda.php">Projects / Honda</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/case-studies/accelya.php">Projects / Accelya</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/projects.php/#project-banners">Projects / Banners</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/projects.php/#project-newsletters">Projects / Newsletters</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/team.php">Team</a></h2>
    <h2 class="sitemap__heading"><a class="sitemap__link" href="/contact.php">Say Hi!</a></h2>


  </section>

  


</main>

<?php include 'templates/layout/footer.php'; ?>
