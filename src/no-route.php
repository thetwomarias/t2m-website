<?php
  // Variables
  $bodyClass = 'no-route';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">We’re sorry, <br/>I’m afraid the page you’re <br/>looking for is not <br/>here anymore :(</h1>
    </div>
  </section>

  

  


</main>

<?php include 'templates/layout/footer.php'; ?>
