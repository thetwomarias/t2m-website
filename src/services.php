<?php
  // Variables
  $bodyClass = 'services';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">We care not just about what we do, but how we do it - with care and attention to detail. Because every project has a unique story to tell.</h1>
    </div>
  </section>

  <section class="service__block service__block--left" id="service-websites" data-paroller-factor="0.025" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="service__image">
    <img class="service__image service__image--placeholder" src="/img/services/websites-placeholder.svg" alt="Website design and development" title="Website design and development"/>
    <object type="image/svg+xml" data="/img/services/websites.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/services/websites.svg" alt="Creamos websites" title="Creamos websites"/>
    </object>
    </div>
    <div class="service__text">
        <h2 class="service__title">We create websites from top to bottom</h2>
        <p class="service__desc">From tactical landing pages to complex e-commerce websites (and everything in between), we conceptualise, wireframe, design and program. Responsive? CSS3? HTML5? SEO-friendly? A cherry on top? If you order it, it’s on the menu.</p>
    </div>
  </section>

  <section class="service__block service__block--right" id="service-videos" data-paroller-factor="-0.1" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="service__image">
    <img class="service__image service__image--placeholder" src="/img/services/videos-placeholder.svg" alt="Producción de vídeo y animación" title="Producción de vídeo y animación"/>
    <object type="image/svg+xml" data="/img/services/videos.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/services/videos.svg" alt="Producción de vídeo y animación" title="Producción de vídeo y animación"/>
    </object>
    </div>
    <div class="service__text">
        <h2 class="service__title">We produce video and motion graphics for big screens and small</h2>
        <p class="service__desc">We create everything from widescreen corporate videos that make a bang at your annual shareholder meeting, to social-friendly animations that are made especially for mobile. We believe that effectiveness doesn’t have to come with a big price tag - and we have a wide range of contacts and partners around the world should you need to shoot in locations that might otherwise be hard to reach.</p>
    </div>
  </section>

  <div class="image--absolute show--sm">
      <img src="/img/backgrounds/services-newsletters.svg" alt="" />
  </div> 

  <section class="service__block service__block--left" id="service-newsletters" data-paroller-factor="0.05" data-paroller-type="foreground" data-paroller-direction="vertical">
  
  <div class="service__image">
    <img class="service__image service__image--placeholder" src="/img/services/newsletters-placeholder.svg" alt="Generamos e-mailings y Newsletters" title="Generamos e-mailings y Newsletters"/>
    <object type="image/svg+xml" data="/img/services/newsletters.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/services/newsletters.svg" alt="Generamos e-mailings y Newsletters" title="Generamos e-mailings y Newsletters"/>
    </object>
    </div>
    <div class="service__text">
        <h2 class="service__title">We generate e-mails and newsletters at scale</h2>
        <p class="service__desc">We know that there’s more to e-mail marketing than "an e-mail”. We work with senior specialist creators to plan, design and program HTML e-mail campaigns that can run across any of the major eCRM platforms - and get them right first time.</p>
    </div>
  </section>

  <section class="service__block service__block--right" id="service-campaigns" data-paroller-factor="-0.1" data-paroller-type="foreground" data-paroller-direction="vertical">
    <div class="service__image">
    <img class="service__image service__image--placeholder" src="/img/services/campaigns-placeholder.svg" alt="Llevamos 1000 campañas online producidas" title="Producción de vídeo y animación"/>
    <object type="image/svg+xml" data="/img/services/campaigns.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/services/campaigns.svg" alt="Llevamos 1000 campañas online producidas" title="Producción de vídeo y animación"/>
    </object>
    </div>
    <div class="service__text">
        <h2 class="service__title">And we have delivered over 1000 online campaigns</h2>
        <p class="service__desc">We are old hands with the banner format. We have created multi-country, multi-language and multi-device campaigns for over 10 years, which means we know our platforms and formats inside out – which may be why we’re a Google DoubleClick Studio Qualified Partner, too.</p>
    </div>
  </section>

  <section class="services-bottom bottom-block">


    <div class="bottom-block__wrapper">

      <div class="bottom-block__content">

        <div class="bottom-block__text">
          <h6 class="bottom-block__heading">Put your next <br/>project in the <br/>safest of hands.</h6>
          <p class="bottom-block__desc">Send an email to <a href="mailto:projects@thetwomarias.com">projects@thetwomarias.com</a> <br/>and let’s get this party started.</p>
        </div>

        <div class="bottom-block__image">
          <object type="image/svg+xml" data="/img/services/manos.svg" preserveAspectRatio="xMidYMid meet">
            <img src="/img/services/manos.svg" alt="Pon el proyecto que tienes en mente en buenas manos" title="Pon el proyecto que tienes en mente en buenas manos" />
          </object>

        </div>

      </div>

    </div>

  </section>


</main>

<?php include 'templates/layout/footer.php'; ?>
