<?php
  // Variables
  $bodyClass = 'home';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Delivering digital goodness <br/>and peace of mind <br/>since 2008.</h1>
      <h2 class="page-header__subtitle appear"><a href="/case-studies/honda-miimo.php"><span class="icon-dash-line"></span><span class="currently appear appear-ltr">Currently working with<strong>Honda Europe</strong></span></a></h2>
    </div>
  </section>

  <section class="home-projects" id="home-projects">

    <div class="home-projects__slider">

      <div class="home-projects__item home-projects__item--blue appear home-project-simoncoll" id="home-project-simoncoll" data-vp-offset="10">
        <a class="link--invisible" href="/case-studies/simon-coll.php"></a>
        <h2 class="home-projects__title">Simón Coll</h2>
        <div class="home-projects__desc">Our most recent (and most delicious) <br/>e-commerce experience - this time <br/>for the renowned chocolatier.</div>
        <div class="home-projects__link">
          <a href="/case-studies/simon-coll.php" class="link--cta"><span>View case study</span></a>
        </div>
      </div>         

      <div class="home-projects__item home-projects__item--white home-project-accelya appear next" id="home-project-accelya" data-vp-offset="10">
        <a class="link--invisible" href="/case-studies/accelya.php"></a>
        <h2 class="home-projects__title">Accelya</h2>
        <div class="home-projects__desc">Production of three short videos <br/>for this leading service provider, <br/>supporting the airline industry <br/>from Dallas to Dubai.</div>
        <div class="home-projects__link">
          <a href="/case-studies/accelya.php" class="link--cta"><span>View videos</span></a>
        </div>
      </div>

      <div class="home-projects__item home-projects__item--blue home-project-banners" id="home-project-banners" data-vp-offset="10">
        <a class="link--invisible" href="/projects.php#project-banners"></a>
        <h2 class="home-projects__title">Banners</h2>
        <div class="home-projects__desc">Too many favourites to choose <br/>just one… We’ve got banners in every <br/>shape, size, colour and language.</div>
        <div class="home-projects__link">
          <a href="/projects.php#project-banners" class="link--cta"><span>View more</span></a>
        </div>
      </div>

    </div>

      <div class="red-triangle-wrapper">
        <img class="red-triangle" src="/img/backgrounds/corner-red.svg" alt="" />
      </div>

  </section>



  <div id="home-services-wrapper">

    <section class="home-services appear">
      <div class="home-services__block">
        <ul class="home-services__list">
          <li class="home-services__item"><a href="/projects.php#project-honda" class="home-services__link"><span>Websites</span></a></li>
          <li class="home-services__item"><a href="/projects.php#project-simon-coll" class="home-services__link"><span>E-commerce</span></a></li>
          <li class="home-services__item"><a href="/projects.php#project-accelya" class="home-services__link"><span>Online video</span></a></li>
          <li class="home-services__item"><a href="/projects.php#project-banners" class="home-services__link"><span>Banners</span></a></li>
          <li class="home-services__item"><a href="/projects.php#newsletters-slider" class="home-services__link"><span>E-mail</span></a></li>
          <li class="home-services__item"><a href="/projects.php#project-accelya" class="home-services__link"><span>Motion graphics</span></a></li>
        </ul>
      </div>
      <div class="home-services__block">
        <div class="home-services__text">
          <strong>The Two Marias</strong> is an independent <br/>production company based in Barcelona. We design and develop digital experiences, create content and craft campaigns — and we do it for brands and agencies around the world who want their projects delivered well (and without the headaches). <br><br><a href="/projects.php" class="link--cta" style="display:inline-block;"><span>Click here</span> to view all projects</a>.
        </div>
      </div>
    </section>

  </div>

  <section class="clients appear">

    <h3 class="clients__heading">Clients</h3>
    <div class="clients__wrapper pad">
      <div class="client"><img class="client__logo" src="/img/clients/simoncoll.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/redwood.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bbh.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/mc-saatchi.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/honda.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/vanquis.png" alt="" title="" /></div>

      <div class="client"><img class="client__logo" src="/img/clients/adam-eve.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/accelya.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/the-brooklyn-brothers.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/catalana-occidente.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/bmb.png" alt="" title="" /></div>
      <div class="client"><img class="client__logo" src="/img/clients/facebook.png" alt="" title="" /></div>
    </div>

  </section>

  <section class="home-mariismos">  
    <h4 class="mariismos__title hide-lg show-sm" style="text-align: center; letter-spacing: 2px; font-size: 14px; padding-top: 4%; font-weight:700;">Our mantras</h4>

    <div class="home-mariismos__slider">

      <div class="mariismos__slide mariismos-likes">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/likes.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title hide-sm">Our mantras</h4>
          <h5 class="mariismos__heading">Treat every project <br/>like it’s the first.</h5>
          <p class="mariismos_desc">We’ve worked with clients all over the world, and on a vast array of different digital production projects. And whilst we’ve been doing it for over a decade, we still bring the same energy that we brought on Day 1.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-mamushka">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/mamushka.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title hide-sm">Our mantras</h4>
          <h5 class="mariismos__heading">Small <br/>is beautiful.</h5>
          <p class="mariismos_desc">Because being small allows us to be agile, flexible, and focus on the details of each and every project. And it means we treat our clients’ money like it’s our own. We work and collaborate continuously online, saving time and making the most of your budget.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-mundo">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/mundo.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title hide-sm">Our mantras</h4>
          <h5 class="mariismos__heading">Keep your feet on the ground and your arms around the world.</h5>
          <p class="mariismos_desc">10 years of international experience has given us the skills to be able to handle different time zones and multiple work scenarios. You can pick up the phone, and we’ll answer in English, German, Spanish, Catalan or French, depending on your needs.</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-bocas">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/bocas.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title hide-sm">Our mantras</h4>
          <h5 class="mariismos__heading">“Professional, <br/>efficient and friendly”</h5>
          <p class="mariismos_desc">That’s what our clients say, anyway… Is it our belief that “god is in the details”? Our good humour (even on a Monday morning)? Or is it that we always deliver the goods (and always on time)? Maybe it’s a mix of all three, or maybe it’s something more…</p>
        </div>
      </div>

      <div class="mariismos__slide mariismos-karma">
        <div class="mariismos__image">
          <img src="/img/home/mariismos/karma.svg" title="" alt=""/>
        </div>
        <div class="mariismos__text">
          <h4 class="mariismos__title hide-sm">Our mantras</h4>
          <h5 class="mariismos__heading">Karma means <br/>keeping it simple</h5>
          <p class="mariismos_desc">Doing good work today brings joy tomorrow - both to us, and our clients. We might work with complex technologies, but we keep things human-scale and jargon-free. Maybe that’s why people keep coming back again and again.</p>
        </div>
      </div>

      <span class="pagingInfo"></span>

    </div>

  </section>

  <section class="home-team">
    <div class="home-team__wrapper">
      <div class="home-team__text">
        <p class="home-team__heading">There are only two Marias<br/>
        But there’s also one Paula, <br/>an Alex and a Xavi …</p>

        <a href="/team.php" class="link--cta"><span>Get to know us</span></a>
      </div>
      <div class="home-team__image appear">
        <img src="/img/home/home-team.jpg" alt="Equipo The Two Marias" title="Equipo The Two Marias" />
      </div>
    </div>
  </section>

  <section class="home-bottom bottom-block">
    <div class="bottom-block__wrapper">

      <div class="bottom-block__content">
        <img src="/img/home/express-quote.svg" alt="Express quote in 24 hours" title="Express quote in 24 hours" />

        <div class="bottom-block__text">
          <h6 class="bottom-block__heading">Want to know more?</h6>
          <p class="bottom-block__desc">Tell us what you need at <a href="mailto:projects@thetwomarias.com">projects@thetwomarias.com</a><br/>
          and we’ll send you a quote in less than 24 hours.</p>
        </div>

        <div class="bottom-block__image">

          <object type="image/svg+xml" data="/img/presupuesto-mobile.svg" preserveAspectRatio="xMidYMid meet" class="show-sm show-md hide-lg">
            <img src="/img/home/home-bottom-bag.png" alt="Express quote in 24 hours" title="Express quote in 24 hours" class="hide-mobile show-desktop" />
          </object>

          <object type="image/svg+xml" data="/img/presupuesto.svg" preserveAspectRatio="xMidYMid meet" class="hide-sm hide-md">
            <img src="/img/home/home-bottom-bag.png" alt="Express quote in 24 hours" title="Express quote in 24 hours" class="hide-mobile show-desktop" />
          </object>

        </div>

      </div>

    </div>

  </section>

</main>

<?php include 'templates/layout/footer.php'; ?>
