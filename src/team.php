<?php
  // Variables
  $bodyClass = 'team';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">We are family</h1>
      <p class="page-header__subheading appear">We’re more than just two Marias… there’s also one Paula, an Alex, a Xavi, and many more - in fact, right now, there’s about a dozen of us. But we can be more (or less), depending on the project. Our team expands and contracts to suit your needs, and achieve the perfect balance of agility, efficiency and expertise.</p>
    </div>
  </section>


  <section class="members members--two">
    <div class="member">
    <img class="member__image" src="/img/team/maria_blasi.jpg" alt="" />
      <h2 class="member__name">Maria Blasi</h2>
      <h3 class="member__role">Founder and Operations Director</h3>
    </div>

    <div class="member__separator"></div>

    <div class="member">
      <img class="member__image" src="/img/team/maria_sala.jpg" alt="" />
      <h2 class="member__name">Maria Sala</h2>
      <h3 class="member__role">Partner and Managing Director</h3>
    </div>
  </section>

  <section class="team-intro team-intro--first">
    <div class="team-intro--left"><strong>Two Marias </strong>met while having a not-so-great coffee in a meeting room in a London agency in 1999. Not only did they have a name and nationality in common, but also long experience of digital business – not to mention a shared work ethic. Moments like this don’t come around very often, and when they do, they generally mean something. And so, they decided to create The Two Marias and, over the course of a decade, they’ve brought 1,064 projects into the world (and counting).</div>
    <div class="team-intro--right">Over the years we have been fortunate enough to attract seasoned professionals from different disciplines, all of whom bring with them oceans of international experience. It’s their talent and energy that means we can ensure not just speed, but quality - and the Christmas parties are definitely more fun since they joined!</div>

  </section>

  <section class="members members--four">

  <div class="member">
      <img class="member__image" src="/img/team/paula_aguilar.jpg" alt="" />
      <h2 class="member__name">Paula</h2>
      <h3 class="member__role">Producer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/alex_lopez.jpg" alt="" />
      <h2 class="member__name">Alex</h2>
      <h3 class="member__role">Front-End Developer</h3>
    </div>

    <div class="member">
    <img class="member__image" src="/img/team/xavi_gort.jpg" alt="" />
      <h2 class="member__name">Xavi</h2>
      <h3 class="member__role">Back-End Developer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/ale_bica.jpg" alt="" />
      <h2 class="member__name">Ale</h2>
      <h3 class="member__role">Designer</h3>
    </div>

    <img class="background--absolute left-0 bottom-20 minus-z show--sm full" src="/img/backgrounds/corner-pink.svg" alt=""/>


  </section>

  <section class="team-intro team-intro--freelancers background--pink">
    <div class="team-intro--left">
    Every project that comes through the door is an opportunity for us (and you) to work with old friends and new talent. We cast every project according to your specific needs, and ensure that everyone brings with them the right levels of experience, seriousness and enthusiasm - the best of all possible worlds!
    </div>
</section>

  <section class="members members--four members--freelancers background--transparent">

  <div class="background--absolute left-0 top-10 h-400 full background--pink full" style="display:none;"></div>

  <div class="members__wrapper members__wrapper--first">

    <div class="member">
    <img class="member__image" src="/img/team/albert.jpg" alt="" />
      <h2 class="member__name">Albert</h2>
      <h3 class="member__role">Developer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/cecilia.jpg" alt="" />
      <h2 class="member__name">Cecilia</h2>
      <h3 class="member__role">Art Director</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/saul.jpg" alt="" />
      <h2 class="member__name">Saül</h2>
      <h3 class="member__role">Designer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/cata.jpg" alt="" />
      <h2 class="member__name">Cata</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

  </div>

  <div class="members__wrapper">


    <div class="member">
      <img class="member__image" src="/img/team/barbara.jpg" alt="" />
      <h2 class="member__name">Barbara</h2>
      <h3 class="member__role">UX/UI</h3>
    </div>

    <div class="member">
    <img class="member__image" src="/img/team/cesc.jpg" alt="" />
      <h2 class="member__name">Cesc</h2>
      <h3 class="member__role">Designer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/dirk.jpg" alt="" />
      <h2 class="member__name">Dirk</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/jonas.jpg" alt="" />
      <h2 class="member__name">Jonas</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

  </div>

  <img class="background--absolute left-0 bottom-20 minus-z full" src="/img/backgrounds/corner-pink.svg" alt=""/>

  </section>

  <section class="team-bottom bottom-block">


    <div class="bottom-block__wrapper">

      <div class="bottom-block__content">

        <div class="bottom-block__text">
          <h6 class="bottom-block__heading">Join the <span class="">family</span>!</h6>
          <p class="bottom-block__desc">Developers, designers, producers, technologists and assorted multi-hyphenates… we are always looking for new talent to collaborate with.<br/>Contact us at  <a href="mailto:jobs@thetwomarias.com">jobs@thetwomarias.com</a><br/></p>
        </div>

        <div class="bottom-block__image">
          <object type="image/svg+xml" data="/img/puzzle.svg" preserveAspectRatio="xMidYMid meet">
            <img src="/img/home/home-bottom-bag.png" alt="Presupuesto en menos de 24 horas" title="Presupuesto en menos de 24 horas" />
          </object>

        </div>

      </div>

    </div>

  </section>


</main>

<?php include 'templates/layout/footer.php'; ?>
