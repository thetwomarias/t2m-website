<?php
  // Variables
  $bodyClass = 'contact';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Bienvenid@ foraster@</h1>
      <p class="page-header__subheading appear">Nos gusta conocer gente nueva cada día. <br/>Nunca sabes de dónde vendrá la siguiente oportunidad...</p>
    </div>
  </section>

  <section class="contact-details">

    <div class="contact-details--left">
    <object type="image/svg+xml" data="/img/contact/buzon.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/contact/buzon.svg" alt="" description="" />
    </object>
    <div class="contact-details__text">
      <h2 class="contact-details__heading">¿Tienes un proyecto que contarnos? </h2>
      <p class="contact-details__info"><a href="mailto:maria.sala@thetwomarias.com">maria.sala@thetwomarias.com</a>te escuchará encantada.</p>
      <h2 class="contact-details__heading">¿Nos quieres enseñar tu portfolio?</h2>
      <p class="contact-details__info"><a href="mailto:maria.blasi@thetwomarias.com">maria.blasi@thetwomarias.com</a>lo está esperando.</p>
    </div>
    </div>
    <div class="contact-details--right">
    <object type="image/svg+xml" data="/img/contact/phone.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/contact/phone.svg" alt="" description="" />
    </object>
    <h2 class="contact-details__heading">¿Echas de menos llamar a un teléfono fijo?</h2>
    <p class="contact-details__info">Prueba con el nuestro.<a href="tel:+34934150893">+34 934 150 893</a></p>


    </div>

  </section>

  <section class="contact-details--center">
    <div>Si lo prefieres, nos encantará ponerte cara en una de nuestras bases; podemos tomar un café con vistas en la <strong>Soho House si estás en Barcelona</strong>, o recibirte en nuestra oficina en el <strong>Parc Audiovisual de Catalunya (Terrassa)</strong>.</div>
  </section>

  <section class="contact-locations">

    <div class="location location--right" data-paroller-factor="0.125" data-paroller-type="foreground" data-paroller-direction="vertical">
      <div class="location__image">
        <img src="/img/contact/parc-audiovisual.jpg" alt=""/>
      </div>
      <div class="location__text">
        <h3 class="location__name">Parc Audiovisual de Catalunya</h3>
        <address class="location__address">Carretera, BV-1274, Km1, 08225 Terrassa, Barcelona</address>
        <a class="link--cta" href="https://goo.gl/maps/qFbfArKzES2pnhiE9" target="_blank"><span>Ver mapa</span></a>
      </div>
    </div>

    <div class="location location--left" data-paroller-factor="-0.075" data-paroller-type="foreground" data-paroller-direction="vertical"> 
      <div class="location__image">
          <img src="img/contact/soho-house.jpg" alt=""/>
      </div>
      <div class="location__text">
          <h3 class="location__name">Soho House Barcelona</h3>
          <address class="location__address">Plaça del Duc de Medinaceli, 4, 08002 Barcelona</address>
          <a class="link--cta" href="https://goo.gl/maps/CZkUo3NpBiwris1M6" target="_blank"><span>Ver mapa</span></a>
      </div>
    </div>

  </section>

  


</main>

<?php include 'templates/layout/footer.php'; ?>
