
lottie.loadAnimation({
  container: document.getElementById('bm'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: false,
  autoplay: true,
  path: '/js/bodymovin/data.json' // the path to the animation json
});

// A $( document ).ready() block.
$( document ).ready(function() {

  // Global variables
  var c, currentScrollTop = 0,
  navbar = $('header');
  navbarSticky = $('.nav-sticky');

  var scrollTop = $(window).scrollTop();


  // Header Current link

  if ($('body').hasClass('services')) {
    $('.nav__link--services').addClass('nav__link--current');
  } else if ($('body').hasClass('projects')) {
    $('.nav__link--projects').addClass('nav__link--current');
  } else if ($('body').hasClass('team')) {
    $('.nav__link--team').addClass('nav__link--current');
  } else if ($('body').hasClass('contact')) {
    $('.nav__link--contact').addClass('nav__link--current');
  }

  // Projects Slider

  var homeSlider = $('.home-projects__slider');

  homeSlider.slick({
    autoplay: false,
    speed: 800,
    centerMode: true,
    centerPadding: '10%',
    fade: false,
    dots: true,
    arrows: false,
    adaptiveHeight: false,
    infinite: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 576,
        settings: "unslick"
      }
    ],
    customPaging : function(slider, i) {
    var thumb = $(slider.$slides[i]).data();
    return '<a>0'+(i+1)+'</a>';
    
    }
  });

  homeSlider.on('init', function() {
    $('.slick-current').prev().addClass('prev');
    $('.slick-current').next().addClass('next');
  });

  homeSlider.on('beforeChange', function(){
    $('.slick-slide').removeClass('prev next');
  });

  homeSlider.on('afterChange', function() {
    $('.slick-current').prev().addClass('prev');
    $('.slick-current').next().addClass('next');
  });

  // Mariismos Slider

  var sliderMariismos = $('.home-mariismos__slider');

  sliderMariismos.on('init', function(e, slick) {
      var $firstAnimatingElements = $('div.mariismos__slide:first-child').find('.mariismos__text').addClass("animated");
  });

  sliderMariismos.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
    $('div.mariismos__slide:not(".slide-current")').find('.mariismos__text, .mariismos__image').removeClass("animated");
      var $animatingElements = $('div.mariismos__slide[data-slick-index="' + nextSlide + '"]').find('.mariismos__text, .mariismos__image').addClass("animated");
  });

  sliderMariismos.slick({
    autoplay: false,
    speed: 800,
    fade: true,
    dots: true,
    arrows: false,
    adaptiveHeight: false,
    slide: '.mariismos__slide',
    responsive: [{
      breakpoint: 768,
      settings: {
        adaptiveHeight: true
      }  
    }],
    customPaging : function(slider, i) {
    var thumb = $(slider.$slides[i]).data();
    return '<a>0'+(i+1)+'</a>';
    }
  });

  // Projects Sliders

  var sliderProjects = $('.project__slider');

  sliderProjects.on('init', function(event, slick){
    //console.log("Slider inited!");
    $(this).find('.slick-dots').appendTo($(this).parent().find('.project__dots'));
  });

  sliderProjects.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
    myPlayerAccelya1.pause();
    myPlayerAccelya2.pause();
    myPlayerAccelya3.pause();
  });

  sliderProjects.slick({
    autoplay: false,
    speed: 800,
    fade: false,
    dots: true,
    arrows: false,
    adaptiveHeight: false,
    slide: '.project__slide',
    customPaging : function(slider, i) {
    var thumb = $(slider.$slides[i]).data();
    return '<a>0'+(i+1)+'</a>';
    }
  });

  // Projects Sliders

  var sliderTestimonials = $('.testimonials__slider');

  sliderTestimonials.slick({
    autoplay: false,
    speed: 800,
    fade: true,
    dots: true,
    arrows: false,
    adaptiveHeight: true,
    slide: '.testimonials__slide',
    customPaging : function(slider, i) {
    var thumb = $(slider.$slides[i]).data();
    if (i >= 9) {
      return '<a>'+(i+1)+'</a>';
    } else {
      return '<a>0'+(i+1)+'</a>';
    }
    }
  });

  sliderTestimonials.on('init', function(e, slick) {
    $('div.testimonials__slide:first-child').find('.testimonials__quote, .testimonials__credits').addClass("animated");
  });

  sliderTestimonials.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
    $('div.testimonials__slide:not(".slide-current")').find('.testimonials__quote, .testimonials__credits').removeClass("animated");
      $('div.testimonials__slide[data-slick-index="' + nextSlide + '"]').find('.testimonials__quote, .testimonials__credits').addClass("animated");
  });

  var sliderCaseStudies = $(".case-study__slider");

  sliderCaseStudies.slick({
    autoplay: false,
    speed: 800,
    fade: false,
    dots: true,
    arrows: false,
    adaptiveHeight: false,
    mobileFirst: true,
    responsive: [
      {
              breakpoint: 768,
              settings: 'unslick'
      }
    ],
    customPaging : function(slider, i) {
    var thumb = $(slider.$slides[i]).data();
    if (i >= 9) {
      return '<a>'+(i+1)+'</a>';
    } else {
      return '<a>0'+(i+1)+'</a>';
    }
    }
  });

  // Viewport Checker

    $('.home-projects__item.slick-current .link--cta').viewportChecker({
      
    });
   
  if ($(window).width() <= 768 && $('body').hasClass('services') ) {

    $('.service__image').viewportChecker({
      classToAdd: 'show-animation',
      offset: 20
    });

    $('.page-header__title.appear').viewportChecker();

  } else if ($(window).width() > 768 && $('body').hasClass('services') ) {
    
    $('#service-websites .service__image').viewportChecker({
      offset: 200
    });
    $('#service-videos .service__image, #service-newsletters .service__image, #service-campaigns .service__image').viewportChecker({
      offset: 420
    });

    $('.page-header__title').viewportChecker({
      classToAdd: 'visible',
      offset: 0
    });

  } else {
    $('.appear').viewportChecker();
  }
 
  $('.mariismos-likes .mariismos__image, .mariismos-likes .mariismos__text').viewportChecker({
    classToAdd: 'animated', // Class to add to the elements when they are visible,
  });

  // Video JS

  if ( $("body").hasClass('projects') || $("body").hasClass('accelya') ){
    var myPlayerAccelya1 = videojs('video-accelya-one');
    var myPlayerAccelya2 = videojs('video-accelya-two');
    var myPlayerAccelya3 = videojs('video-accelya-three');

    var myFunc1 = function(){
      myPlayerAccelya2.pause();
      myPlayerAccelya3.pause();
    };

    var myFunc2 = function(){
      myPlayerAccelya1.pause();
      myPlayerAccelya3.pause();
    };

    var myFunc3 = function(){
      myPlayerAccelya1.pause();
      myPlayerAccelya2.pause();
    };

    myPlayerAccelya1.on("play", myFunc1);
    myPlayerAccelya2.on("play", myFunc2);
    myPlayerAccelya3.on("play", myFunc3);


    myPlayerAccelya1.on("ended", function () {
      myPlayer.currentTime(0);
      myPlayer.bigPlayButton.show();
    });

    myPlayerAccelya2.on("ended", function () {
      myPlayer.currentTime(0);
      myPlayer.bigPlayButton.show();
    });

    myPlayerAccelya3.on("ended", function () {
      myPlayer.currentTime(0);
      myPlayer.bigPlayButton.show();
    });

  }

  if ( $("body").hasClass('honda') ){
    var myPlayerHonda = videojs('miimo-video');

    myPlayerHonda.on("ended", function () {
      myPlayer.currentTime(0);
      myPlayer.bigPlayButton.show();
    });

  }

  // Mobile Menu

  $('.hamburger').click(function(){
    $('.hamburger').toggleClass('open');
    $('body').toggleClass('has-nav');
  });

  // Cookie Bar

  $('.cookie-message').cookieBar({ closeButton : '.cookie-close', hideOnClose: false });
  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });

  // Parallax

  $('.project-block').paroller();
  $('.service__block').paroller();
  $('.location').paroller();

  //custom function showing current slide
  var $status = $('.pagingInfo');
  var $slickElement = $('.home-mariismos__slider');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 1) + 1;
    $status.text(i + '/' + slick.slideCount);
  });

  // Scroll Buttons
  $('.scroll-btn.scroll-up span').click(function(){
    $('html,body').animate({ scrollTop: 0 }, 1000 );
      return false;
  });

  if ($("body").hasClass("projects")){
    //$('#project-banners .project__slide--bg').data('vide').getVideoObject().play();
  }

  // Scroll Window functions

  $(window).scroll(function () {

    // Play Videos when visible

    $('.case-study.honda video').each(function(){
      if ($(window).width() > 992) {
        if ($(this).hasClass("visible")) {
            $(this)[0].play();
        } else {
            $(this)[0].pause();
        }
      }
    });

    // Sticky header
    const a = $(window).scrollTop();
    const b = navbar.height();

    currentScrollTop = a;

    if (c < currentScrollTop && a > b + b) {
      $('body').removeClass("sticky-nav");
    } else if (c > currentScrollTop && !(a <= b)) {
      $('body').addClass("sticky-nav");
    }
    c = currentScrollTop;

    // Hide Sticky Nav when header is visible

    if ($(window).scrollTop() > 200) {
      // The top header is not visible yet
      $('.scroll-btn.scroll').css("opacity","0");
      $('.scroll-btn.scroll').addClass('scrolled');
    } else {
      // The top header is visible - hide the sticky nav
      $('body').removeClass("sticky-nav");
      $('.scroll-btn.scroll').css("opacity","1");
      $('.scroll-btn.scroll').removeClass('scrolled');

    }

  });

  // Home Projects to appear when scroll

  $(window).scroll(function() {    
   
    if ($(window).width() > 768) {
      var hT = $('.home-projects__item').offset().top - 200,
      hH = $('.home-projects__item').outerHeight(),
      wH = $(window).height(),
      wS = $(this).scrollTop();
      if (wS > (hT+hH-wH)){
        $('.home-project-accelya').addClass('appear-rtl');
      }
      if ($(window).scrollTop() >= 450 && $(window).scrollTop() <= 465) {
        $('.home-project-accelya').addClass('now');
        setTimeout(function(){ $('.home-project-accelya').removeClass('now'); }, 1200);
      }
    }
    
  });

  $('#mc-embedded-subscribe-form').validate({
    errorElement : 'span',
    errorLabelContainer: '.error-text'
  });


});
