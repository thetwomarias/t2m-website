<?php
  // Variables
  $bodyClass = 'contact';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Hi there!</h1>
      <p class="page-header__subheading appear">We like to hear from old friends and new, every day. <br/>You never know where the next opportunity will come from, and it’s always good to talk…</p>
    </div>
  </section>

  <section class="contact-details">

    <div class="contact-details--left">
    
    <div class="contact-details__image appear">
      <img class="contact-details__image--placeholder" src="/img/contact/buzon-placeholder.svg" alt="Contact The Two Marias" title="Contact The Two Marias"/>
      <object type="image/svg+xml" data="/img/contact/buzon.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/contact/buzon.svg" alt="" description="" />
    </object>
    </div>
   
    <div class="contact-details__text">
      <h2 class="contact-details__heading">Already have a project in mind? Tell</h2>
      <p class="contact-details__info"><a href="mailto:maria.sala@thetwomarias.com"><span>maria.sala@thetwomarias.com</span></a>all about it.</p>
      <h2 class="contact-details__heading">Want to show us your portfolio? Right now,</h2>
      <p class="contact-details__info"><a href="mailto:maria.blasi@thetwomarias.com"><span>maria.blasi@thetwomarias.com</span></a>is literally hitting “send & receive” on repeat, waiting for it to arrive.</p>
    </div>
    </div>
    <div class="contact-details--right appear">
    <object type="image/svg+xml" data="/img/contact/phone.svg" preserveAspectRatio="xMidYMid meet">
      <img src="/img/contact/phone.svg" alt="" description="" />
    </object>
    <h2 class="contact-details__heading">Rather talk to a human being?</h2>
    <p class="contact-details__info">Pick up the phone, and we’ll answer in English, German, Spanish, Catalan or French (and maybe a few others if you speak slow enough). <a href="tel:+34934150893">+34 934 150 893</a></p>


    </div>

  </section>

  <section class="contact-details--center">
    <div>If face-to-face and IRL is more your thing, we’d love to meet up. If you’re ever in Barcelona, we can enjoy coffee with a view at <strong>Soho House</strong>; ​​and we’re always happy to play host at our home in the <strong>Parc Audiovisual de Catalunya</strong> (that’s in Terrassa, by the way, just 30 minutes from Barcelona city centre).</div>
  </section>

  <section class="contact-locations">

    <div class="location location--right" data-paroller-factor="0.125" data-paroller-type="foreground" data-paroller-direction="vertical">
      <div class="location__image">
        <img src="/img/contact/parc-audiovisual.jpg" alt=""/>
      </div>
      <div class="location__text">
        <h3 class="location__name">Parc Audiovisual <br/>de Catalunya</h3>
        <address class="location__address">Carretera, BV-1274, Km1, 08225 Terrassa, Barcelona</address>
        <a class="link--cta" href="https://goo.gl/maps/qFbfArKzES2pnhiE9" target="_blank"><span>View map</span></a>
      </div>
    </div>

    <div class="location location--left" data-paroller-factor="-0.075" data-paroller-type="foreground" data-paroller-direction="vertical"> 
      <div class="location__image">
          <img src="img/contact/soho-house.jpg" alt=""/>
      </div>
      <div class="location__text">
          <h3 class="location__name">Soho House Barcelona</h3>
          <address class="location__address">Plaça del Duc de Medinaceli, 4, 08002 Barcelona</address>
          <a class="link--cta" href="https://goo.gl/maps/CZkUo3NpBiwris1M6" target="_blank"><span>View map</span></a>
      </div>
    </div>

  </section>

  


</main>

<?php include 'templates/layout/footer.php'; ?>
