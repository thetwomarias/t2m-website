<?php
  // Variables
  $bodyClass = 'team';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Somos una familia</h1>
      <p class="page-header__subheading appear">Hay vida más allá de las dos Marías; concretamente más de una docena. Las del equipo que, según el proyecto, se contrae o se expande como un acordeón para lograr el equilibrio perfecto entre agilidad, eficiencia y expertise.</p>
    </div>
  </section>


  <section class="members members--two">
    <div class="member">
    <img class="member__image" src="/img/team/maria_blasi.jpg" alt="" />
      <h2 class="member__name">Maria Blasi</h2>
      <h3 class="member__role">Fundadora y Directora de Operaciones</h3>
    </div>

    <div class="member__separator"></div>

    <div class="member">
      <img class="member__image" src="/img/team/maria_sala.jpg" alt="" />
      <h2 class="member__name">Maria Sala</h2>
      <h3 class="member__role">Socia y Directora General</h3>
    </div>
  </section>

  <section class="team-intro">
    <div class="team-intro--left"><strong>Las dos Marias</strong> se conocieron tomando un café malo en el pasillo de una agencia londinense. Además de ser tocayas, compartían una larga experiencia en el business digital y una misma ética de trabajo. Tanto, que decidieron asociarse en the Two Marias y durante los últimos 11 años han producido juntas xx proyectos con amor de madre.</div>
    <div class="team-intro--right">A lo largo de los años hemos tenido la suerte de ir sumando a la mesa perfiles senior de distintas disciplinas y con experiencia internacional. Gracias a su talento y energía aseguramos calidad y velocidad,
y las cenas de Navidad son más divertidas.</div>

  </section>

  <section class="members members--four">

  <div class="member">
      <img class="member__image" src="/img/team/paula_aguilar.jpg" alt="" />
      <h2 class="member__name">Paula</h2>
      <h3 class="member__role">Production Manager</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/alex_lopez.jpg" alt="" />
      <h2 class="member__name">Alex</h2>
      <h3 class="member__role">Front-End Developer</h3>
    </div>

    <div class="member">
    <img class="member__image" src="/img/team/xavi_gort.jpg" alt="" />
      <h2 class="member__name">Xavi</h2>
      <h3 class="member__role">Senior Developer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/ale_bica.jpg" alt="" />
      <h2 class="member__name">Ale</h2>
      <h3 class="member__role">Senior Creative</h3>
    </div>
  </section>

  <section class="team-intro team-intro--freelancers background--pink">
    <div class="team-intro--left">
      Cada proyecto que entra por la puerta es una oportunidad para trabajar con gente que nos encanta. Nuestros colaboradores freelance acumulan experiencia, seriedad y ganas. Lo mejor de cada casa.
    </div>
</section>

  <section class="members members--four members--freelancers background--transparent">


  <div class="members__wrapper">

    <div class="member">
    <img class="member__image" src="/img/team/albert.jpg" alt="" />
      <h2 class="member__name">Albert</h2>
      <h3 class="member__role">Developer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/cecilia.jpg" alt="" />
      <h2 class="member__name">Cecilia</h2>
      <h3 class="member__role">Art Director</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/saul.jpg" alt="" />
      <h2 class="member__name">Saül</h2>
      <h3 class="member__role">Designer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/cata.jpg" alt="" />
      <h2 class="member__name">Cata</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

  </div>

  <div class="members__wrapper">


    <div class="member">
      <img class="member__image" src="/img/team/barbara.jpg" alt="" />
      <h2 class="member__name">Barbara</h2>
      <h3 class="member__role">UX Designer</h3>
    </div>

    <div class="member">
    <img class="member__image" src="/img/team/cesc.jpg" alt="" />
      <h2 class="member__name">Cesc</h2>
      <h3 class="member__role">Designer</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/dirk.jpg" alt="" />
      <h2 class="member__name">Dirk</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

    <div class="member">
      <img class="member__image" src="/img/team/jonas.jpg" alt="" />
      <h2 class="member__name">Jonas</h2>
      <h3 class="member__role">Motion Graphics</h3>
    </div>

  </div>

  <img class="background--absolute left-0 bottom-20 minus-z" src="/img/backgrounds/corner-pink.svg" alt=""/>

  </section>

  <section class="team-bottom bottom-block">


    <div class="bottom-block__wrapper">

      <div class="bottom-block__content">

        <div class="bottom-block__text">
          <h6 class="bottom-block__heading">Únete a la <span class="italic">troupe</span></h6>
          <p class="bottom-block__desc">Programadores, diseñadores, producers, technologists; siempre buscamos nuevo talento con el que colaborar. <br/>Escríbenos a  <a href="mailto:jobs@thetwomarias.com">jobs@thetwomarias.com</a><br/></p>
        </div>

        <div class="bottom-block__image">
          <object type="image/svg+xml" data="/img/puzzle.svg" preserveAspectRatio="xMidYMid meet">
            <img src="/img/home/home-bottom-bag.png" alt="Presupuesto en menos de 24 horas" title="Presupuesto en menos de 24 horas" />
          </object>

        </div>

      </div>

    </div>

  </section>


</main>

<?php include 'templates/layout/footer.php'; ?>
