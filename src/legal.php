<?php
  // Variables
  $bodyClass = 'legal';
?>

<?php include 'templates/layout/head.php'; ?>
<?php include 'templates/layout/header.php'; ?>

<main>

  <section class="page-header">
    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title appear">Privacy Policy</h1>
    </div>
  </section>

  <section class="legal-content appear">
    <h2 class="legal__heading">About Us</h2>
    <p class="legal__desc">We are trading as The Two Marias Studio SL, offices based in Barcelona, 08013 in Spain (and we refer to ourselves as <strong>“we”</strong>, <strong>“us”</strong> or <strong>“our”</strong> in this document). Our <strong>“Website”</strong> means the The Two Marias website, URLs are thetwomarias.com or any relevant subdomains. We own and operate this <strong>“Website”</strong> on our own behalf.</p>
  
    <h2 class="legal__heading">About this Privacy Policy</h2>
    <p class="legal__desc">In this Privacy Policy, references to <strong>“you”</strong> means any person submitting any data to us or to this Website about himself or herself or about any other living individual in respect of any use of the Website or the services available through the Website (each being a <strong>“Service”</strong>)</p>
    <p class="legal__desc">When you use different aspects of the Website, we may ask you to supply us with various information about you or anyone whom you represent. This Privacy and Cookies Policy sets out the way in which we may use such information.</p>
    <p class="legal__desc">We are committed to protecting and respecting your privacy. This Privacy Policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us and never transferred to other individuals or companies.</p>
    <p class="legal__desc">Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. For the purpose of the Data Protection Act 1998 (the Act), the data controller is The Two Marias.</p>

    <h2 class="legal__heading">Contacting us</h2>
    <p class="legal__desc">Questions, comments, requests or suggestions regarding this privacy policy are welcomed and should be addressed to <a href="mailto:info@thetwomarias.com"><span>info@thetwomarias.com</span></a>.</p>

    <h2 class="legal__heading">Your Privacy</h2>
    <p class="legal__desc">Your privacy is very important to us. We shall therefore only use your name and other information which relates to you in the manner set out in this Privacy and Cookies Policy. We will only use your personal data in a way that is fair to you. We will only collect information where it is necessary for us to do so and we will only collect information if it is relevant to our dealings with you.</p>
    <p class="legal__desc">We will only keep your information for as long as we are either required to by law or as is relevant for the purposes for which it was collected.</p>   

    <h2 class="legal__heading">Cookie Policy</h2>
    <p class="legal__desc">This Cookie Policy applies to any websites, branded pages on third-party platforms (such as Facebook) and applications accessed or used through such websites or third-party platforms ("The Two Marias Websites") that are operated by, or on behalf of, The Two Marias.</p>
    <p class="legal__desc">By using the The Two Marias Website, you are consenting to our use of cookies in accordance with this Cookie Policy. If you do not agree to our use of cookies in this way, you should set your browser settings accordingly or not use the The Two Marias Website.</p>
    <p class="legal__desc">The table below summarises the different types of cookie we use on the The Two Marias Website, together with their respective purpose and duration (i.e. how long each cookie will remain on your device).</p>

    <h2 class="legal__heading">What are cookies?</h2>
    <p class="legal__desc">Cookies are files or pieces of information that may be stored on your computer (or other internet-enabled devices, such as smartphones and tablets) when you visit the The Two Marias website. A cookie will usually contain the name of the website from which the cook ie has come, the "lifetime" of the cookie (i.e. how long it will remain on your device) and a value, which is usually a randomly generated unique number.</p>
    
    <h2 class="legal__heading">What do we use cookies for?</h2>
    <p class="legal__desc">We only use cookies to compile anonymous, aggregated statistics that allow us to understand how people use the The Two Marias Website and to help us improve their structure and content. We cannot identify you personally from this information.</p>
    
    <h2 class="legal__heading">What types of cookies do we use?</h2>
    <p class="legal__desc">Two types of cookies may be used on the The Two Marias Website: “session cookies” and “persistent cookies”. Session cookies are temporary cookies that remain on your device until you leave the The Two Marias Website. A persistent cookie remains on your device for much longer, or until you manually delete it (how long the cookie remains on your device will depend on the duration, or “lifetime”, of the specific cookie and on your browser settings).</p>

    <h2 class="legal__heading">How can I control or delete cookies?</h2>
    <p class="legal__desc">Most internet browsers are initially set up to automatically accept cookies. You can change the settings to block cookies, or to alert you when cookies are being sent to your device. There is a number of ways to manage cookies. Please refer to your browser instructions, or help screen, to learn more about how to adjust or modify your browser settings.</p>
    <p class="legal__desc">If you use different devices to view and access the The Two Marias Website (e.g. your computer, smartphone, tablet etc.) you will need to ensure that each browser on each device is adjusted to suit your cookie preferences.</p>
    <p class="legal__desc"><a href="http://www.allaboutcookies.org/manage-cookies/" target="_blank"><span>Click here for more information</span></a></p>

    <h2 class="legal__heading">More detailed information:</h2>
    <p class="legal__desc">Session / temporary – These cookies are deleted from your device after you leave a website.</p>
    <p class="legal__desc">Permanent / persistent – These cookies are not deleted from your device after you leave a website. They remain on your device for a longer period of time.</p>


    <table border="1" class="table__cookies">
      <tbody>
        <tr>
          <td>Name</td>
          <td>Purpose</td>
        </tr>
        <tr>
          <td>_utma, _utmb,<br/>_utmc,_utmz</td>
          <td>These cookies enable Google Analytics functionality.
          <ul>
            <li>Determine which domain to measure</li>
            <li>Distinguish unique users</li>
            <li>Remember the number and time of previous visits</li>
            <li>Remember traffic source information</li>
            <li>Determine the start and end of a session</li>
            <li>Remember the value of visitor-level custom variables</li>
          </ul>
          </td>
        </tr>
      </tbody>
    </table>

</div>

  </section>

  


</main>

<?php include 'templates/layout/footer.php'; ?>
