<?php
  // Variables
  $bodyClass = 'case-study honda';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Honda Miimo <br/>Lawn Planner</h1>
      <img class="page-header__image" src="/img/case-studies/honda/header-honda-miimo.jpg" alt="Honda Miimo Lawn Planner"/>
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industry</h3>
          <h4 class="page-header__specs--desc">Robotic Engineering</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Technology</h3>
          <h4 class="page-header__specs--desc">HTML, CSS, Javascript, Google Maps API</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Development</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">UX/UI</li>
            <li class="page-header__specs--item">Design</li>
            <li class="page-header__specs--item">Art Direction</li>
            <li class="page-header__specs--item">Front-end Development</li>
            <li class="page-header__specs--item">Integration in honda.com</li>
            <li class="page-header__specs--item">Localisation (19 languages)</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">Honda Europe asked us to develop an online lawnmower simulator, to help potential customers understand how their new robotic Lawn Planner actually worked, and thus increase consideration and conversion.
          <br/><br/>
          In order to do this, we lifted the hood on the new Honda Miimo robot, studied its operating rules, and translated them into algorithms that simulated the product’s behaviour in an online visualisation — and against the user’s own garden space. We used the Google Maps API to enable people to find their own garden, and get a tangible sense of how Miimo might go about taking care of their lawn for real.
          <br/><br/>
          The simulator works in 19 languages, ​​and be warned: the whole thing has some seriously hypnotic power! Just like the Miimo itself…
        </div>
        <a href="https://www.honda.co.uk/lawn-and-garden/products/miimo/lawn-planner.html" target="_blank" class="link--cta"><span>Visit the website</span></a>
      </div>

    </div>


  </section>

  <section class="case-study__video background--purple">

    <div class="case-study__video--macbook appear hide-sm show-lg">
        <video src="/videos/honda.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" class="appear" playsinline loop muted autoplay></video>
    </div>

    <div class="case-study__video--macbook appear hide-lg show-md show-sm appear">
      <video id="miimo-video" src="/videos/honda-mobile.mp4" width="100%" height="100%" style="width:100%; display:block; margin-bottom:-1px;" poster='/img/projects/honda/honda-poster.jpg' class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" controls preload="none" muted>
        <source src="/videos/honda.mp4"  type='video/mp4' media="all and (max-width:576px)" />
        <source src="/videos/honda.mp4" type='video/mp4' />
        <source src="/videos/honda.webm" type='video/webm' />
      </video>
    </div>

  </section>

  <section class="case-study__images case-study__images--two">
    <img class="case-study__image--left" src="/img/case-studies/honda/tablet-left.png" alt="Honda Miimo Lawn Planner"/>
    <img class="case-study__image--right" src="/img/case-studies/honda/tablet-right.png" alt="Honda Miimo Lawn Planner"/>
    <img class="background--absolute left-0 top-0 minus-z" style="width:100%" src="/img/backgrounds/corner-purple-inverted.svg" alt=""/>
  </section>

  <section class="case-study__image background--pink">
    <img src="/img/case-studies/honda/honda-miimo-image.jpg" alt="" />
  </section>



  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Next case study</h4>
      <h5 class="case-study-next__heading">Accelya</h5>
      <p class="case-study-next__desc">
      This leading provider of technological services to airlines asked us to produce a series of corporate and product videos to be showcased at international trade events in Dallas, Dubai and Monaco. We carried out the production of the videos from first idea to final edit. And the time just flew by!
      </p>
      <a href="/case-studies/accelya.php" class="link--cta"><span>Read more</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
      <a href="/case-studies/accelya.php" class="link--inner"></a>
      <img src="/img/case-studies/next-case-study-accelya.jpg" alt="" />
    </div>
  </section>

</main>

<?php include '../templates/layout/footer.php'; ?>
