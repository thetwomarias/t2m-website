<?php
  // Variables
  $bodyClass = 'case-study simon-coll';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Simón Coll <br/>Website</h1>
      <img class="page-header__image" src="/img/case-studies/simon-coll/header-simon-coll.jpg" alt="Chocolates Simon Coll"/>
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industry</h3>
          <h4 class="page-header__specs--desc">Food</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Technology</h3>
          <h4 class="page-header__specs--desc">Wordpress, Woocommerce, HTML, PHP, Javascript, CSS</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Development</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">UX/UI</li>
            <li class="page-header__specs--item">Responsive design</li>
            <li class="page-header__specs--item">Icon design</li>
            <li class="page-header__specs--item">Content writing and translations</li>
            <li class="page-header__specs--item">Front-end and Back-end development</li>
            <li class="page-header__specs--item">Integration with CMS (Wordpress) and ERP (Navision)</li>
            <li class="page-header__specs--item">Quality Assurance</li>
            <li class="page-header__specs--item">Email design and production</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">Simón Coll, a well-known Catalan company master chocolatier, wanted to redo its brand website, and at the same time make the leap into e-commerce. The Two Marias were privileged to be their chosen partner, and proud to hold their hand throughout the process.
          <br/><br/>
          Our challenge was to take 175 years of tradition and expertise, and weave it through a brand new sales channel that could be integrated into their ERP system. The site elegantly combines editorial with sales functionality, enabling visitors to buy chocolate or book a visit to their factory, but also immerse themselves in the unique history of the brand. 
          <br/><br/>
          We crafted a clean and elegant user experience, brought to life through a design that matched their premium values; and developed their online store from the ground up, with over 400 products to choose from. Suffice it to say, we now know almost as much about chocolate as the experts at Simón Coll - but we’ll be sticking to what we do best for now ;)
        </div>
        <a href="#" class="link--cta"><span>Visit website</span></a>
      </div>

    </div>


  </section>

  <section class="case-study__video background--purple appear hide-sm show-lg">
    <div class="video__background" data-vide-bg="mp4: /videos/simon-coll-home, webm: /videos/simon-coll-home,  ogv: /videos/simon-coll-home, poster: /img/case-studies/simon-coll/online-shop-simon-coll.jpg'" data-vide-options="posterType: 'jpg', loop: true, muted: true, volume: 0, autoplay: true, position: '0% 0%', resizing: true">
  </div>
  </section>

  <section class="case-study__video background--purple appear hide-lg show-sm padding-sm left-35 right-35">
    <video src="/videos/simon-coll-home-mobile-new.mp4" width="100%" style="width:100%; display:block; margin-bottom:-1px;" playsinline loop muted autoplay></video>
  </section>


  <section class="case-study__block background--white" >

    <div class="case-study__images case-study__images--three case-study__slider">
      <img src="/img/case-studies/simon-coll/mobile-image-left.png" class="move-up" alt="Chocolates Simon Coll"/>
      <img src="/img/case-studies/simon-coll/mobile-image-middle.png" alt="Chocolates Simon Coll"/>
      <img src="/img/case-studies/simon-coll/mobile-image-right.png" class="move-up" alt="Chocolates Simon Coll"/>
    </div>

  </section>

  <section class="case-study__block background--pink padding-sm bottom-20">

    <div class="case-study__images case-study__images--two">
      <img class="back-z" src="/img/case-studies/simon-coll/desktop-image-left.jpg" alt="Chocolates Simon Coll"/>
      <img class="front-z" src="/img/case-studies/simon-coll/desktop-image-right.jpg" alt="Chocolates Simon Coll"/>
    </div>

    <img class="background--absolute left-0 bottom-0 hide-sm" src="/img/backgrounds/corner-purple.svg" alt=""/>

  </section>

  <section class="case-study__icons background--purple">

    <div class="case-study__icons--list">
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-house.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-train.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-clock.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-chat.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-three.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-girl.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-parking.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-calendar.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-gluten.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-wheelchair.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-marker.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-wine-clock.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-ticket.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-flower.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-bottles.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-gift.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-pay.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-thumbs.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-print.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-tray.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-chocolate.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-grape.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-picnic.svg" alt="" title=""/></div>
      <div class="case-study__icons--item"><img src="/img/case-studies/simon-coll/iconography/icon-car.svg" alt="" title=""/></div>
    </div>

  </section>

  <section class="case-study__image">
    <img src="/img/case-studies/simon-coll/simon-coll-image-01.jpg" alt="" />
  </section>

  <div class="case-study__images case-study__images--two case-study__images--diff background--pink">
    <img class="case-study__image--left" src="/img/case-studies/simon-coll/tablet-left.png" alt="Simon Coll, procès de reserves"/>
    <img class="case-study__image--right" src="/img/case-studies/simon-coll/tablet-right.png" alt="Simon Coll, procès de reserves"/>
  </div>

  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Next Case Study</h4>
      <h5 class="case-study-next__heading">Honda Miimo <br/>Lawn Planner</h5>
      <p class="case-study-next__desc">
      Honda Europe asked us to develop an online lawnmower simulator, to help potential customers understand how their new robotic Lawn Planner actually worked, and thus increase consideration and conversion.
      </p>
      <a href="/case-studies/honda-miimo.php" class="link--cta"><span>Read more</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
      <a href="/case-studies/honda-miimo.php" class="link--inner"></a>
      <img src="/img/case-studies/catalana-occidente/next-case-study-honda-miimo.jpg" alt="" />
    </div>
  </section>

</main>

<?php include '../templates/layout/footer.php'; ?>
