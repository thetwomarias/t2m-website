<?php
  // Variables
  $bodyClass = 'case-study amatller';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Chocolate <br/>Amatller <br/>Website</h1>
      <img class="page-header__image" src="/img/case-studies/amatller/header-amatller.jpg" alt="Chocolate Amatller"/>
      <!-- <picture>
        <source class="page-header__image" srcset="/img/case-studies/amatller/header-amatller.jpg" media="(min-width: 991px)" />
        <source class="page-header__image" srcset="/img/case-studies/amatller/header-amatller.jpg" media="(min-width: 576px)" />
        <img class="page-header__image" src="/img/case-studies/amatller/header-amatller-mobile.png" />
      </picture> -->
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industry</h3>
          <h4 class="page-header__specs--desc">Food</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Technology</h3>
          <h4 class="page-header__specs--desc">Wordpress, Woocommerce, HTML, PHP, Javascript, CSS</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Development</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">UX/UI</li>
            <li class="page-header__specs--item">Photography and Image retouching</li>
            <li class="page-header__specs--item">Responsive design</li>
            <li class="page-header__specs--item">Icon design</li>
            <li class="page-header__specs--item">Art Direction</li>
            <li class="page-header__specs--item">E-mail design and integration with Mailchimp</li>
            <li class="page-header__specs--item">Content writing and translations</li>
            <li class="page-header__specs--item">Front-end and Back-end development</li>
            <li class="page-header__specs--item">Integration with CMS (Wordpress) and ERP (Navision)</li>
            <li class="page-header__specs--item">Quality Assurance</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">With more than 200 years of history, we were honoured that Chocolate Amatller chose us to design their new online presence. The brand is known and recognized around the world for the beauty of its packaging, which often features original illustrations by none other than Alfons Mucha, the Czech artist at the forefront of the Art Nouveau movement.
          <br/><br/>
          Sold in gourmet stores and duty-free concessions, their chocolate is typically bought as either a personal indulgence or a gift for someone special - and either way, it looks every inch the part. We opted, therefore, for an elegant and contemporary site design that let the brand’s iconic product design do the talking. We organised a still-life and product photography session to capture the packaging in all its glory; while other content, such as the History section, showcases the brand’s graphic legacy, enhancing the brand image by creating an emotional connection with the customer.
          <br/><br/>
          In addition to presenting the brand and its history, customers can also buy its products, of course. We host everything in an e-commerce platform shared with its sister brand, Simón Coll. Wherever possible and appropriate, both sites are synchronised with one another across both websites. When adding a new product to the inventory, the editor or admin chooses whether it will appear only in the Simón Coll shop, or also on the Amatller website, thus saving time and simplifying management overall.
        </div>
        <a href="https://chocolateamatller.com" target="_blank" class="link--cta"><span>Visit website</span></a>
      </div>

    </div>


  </section>

  
  <section class="case-study__block case-study__image background--white">
    <img src="/img/case-studies/amatller/amatller-image.jpg" alt="" />
  </section>

  <section class="case-study__image background--white">
    <img src="/img/case-studies/amatller/amatller-wide.jpg" alt="" />
  </section>

  <section class="case-study__block case-study__video background--pink">
    <img src="/img/case-studies/amatller/amatller-video.jpg" alt="" />
  </section>
  
  <section class="case-study__block background--white" >

    <div class="case-study__images case-study__images--three case-study__slider">
      <img src="/img/case-studies/amatller/mobile-image-left.png" class="move-up" alt="Chocolates Simon Coll"/>
      <img src="/img/case-studies/amatller/mobile-image-middle.png" alt="Chocolates Simon Coll"/>
      <img src="/img/case-studies/amatller/mobile-image-right.png" class="move-up" alt="Chocolates Simon Coll"/>
    </div>

  </section>

  <section class="case-study__block background--pink">

    <div class="case-study__images case-study__images--two">
      <img class="back-z" src="/img/case-studies/amatller/desktop-image-left.jpg" alt="Chocolates Simon Coll"/>
      <img class="front-z" src="/img/case-studies/amatller/desktop-image-right.jpg" alt="Chocolates Simon Coll"/>
    </div>

    <img class="background--absolute left-0 bottom-0" src="/img/backgrounds/corner-white-cs.svg" alt=""/>

  </section>

  <section class="case-study__block case-study__image background--white">
    <img src="/img/case-studies/amatller/amatller-image-2.jpg" alt="" />
  </section>

  <section class="case-study__image">
  <img src="/img/case-studies/amatller/amatller-image-3.jpg" alt="" />
  </section>


  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Next Case Study</h4>
      <h5 class="case-study-next__heading">Catalana Occidente <br/>Videos</h5>
      <p class="case-study-next__desc">
      We worked with Spanish insurance company Catalana Occidente to promote the launch of their new Connected safety car service. We produced two online videos from start to finish.
      </p>
      <a href="/case-studies/catalana-occidente.php" class="link--cta"><span>Read more</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
    <a href="/case-studies/catalana-occidente.php" class="link--inner"></a>
      <img src="/img/case-studies/simon-coll/next-case-study-catalana.png" alt="" />
    </div>
  </section>

</main>

<?php include '../templates/layout/footer.php'; ?>
