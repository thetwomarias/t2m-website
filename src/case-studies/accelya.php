<?php
  // Variables
  $bodyClass = 'case-study accelya';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Accelya <br/>Videos</h1>
      <img class="page-header__image" src="/img/case-studies/accelya/header-accelya.jpg" alt="Accelya Videos"/>
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industry</h3>
          <h4 class="page-header__specs--desc">Airlines</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Technology</h3>
          <h4 class="page-header__specs--desc">Illustration, After Effects, Premiere</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Development</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">Script</li>
            <li class="page-header__specs--item">Storyboarding</li>
            <li class="page-header__specs--item">Illustration</li>
            <li class="page-header__specs--item">Motion graphics</li>
            <li class="page-header__specs--item">Shooting</li>
            <li class="page-header__specs--item">Editing</li>
            <li class="page-header__specs--item">Sound recording</li>
            <li class="page-header__specs--item">Sound editing</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">Accelya, with 400+ airlines, travel agents and shippers as clients, is one of the world’s leading providers of technology products and services to the air passenger and cargo industry.
          <br/><br/>We’ve created so far three videos for them. The first video was showcased at a IATA summit in Dallas, where Accelya was the main sponsor, and included a brief shoot in Dubai. The second was an animation-based video that was used to communicate the launch of an accounting software product at an event in Dubai. The third video presented a newly rebranded sales software release to an audience of more than 500 people from the aviation industry at an event in Monaco.
          <br/><br/>The main challenges of these videos were to try and produce pieces that could entertain an audience for a minute, while at the same delivering complex messages, like the rebranding of a software solution. The client’s budget was also limited, so we had to be very creative at finding the right balance between new and existing content.
        </div>
      </div>

    </div>


  </section>

  <section class="case-study__video background--purple">

    <div class="case-study__video--macbook">
      <video id="video-accelya-one" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-1.jpg' muted>
        <source src="/videos/accelya-one-mobile.mp4"  type='video/mp4' media="all and (max-width:576px)" />
        <source src="/videos/accelya-one.mp4" type='video/mp4' />
        <source src="/videos/accelya-one.webm" type='video/webm' />
      </video>
    </div>

  </section>

  <section class="case-study__video case-study__video--full background--purple">
    <video id="video-accelya-three" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-3.jpg' muted>
      <source src="/videos/accelya-three-mobile.mp4"  type='video/mp4' media="all and (max-width:576px)" />
      <source src="/videos/accelya-three.mp4" type='video/mp4' />
      <source src="/videos/accelya-three.webm" type='video/webm' />
    </video>
  </section>

  <section class="case-study__block case-study__image background--transparent">
    <img src="/img/case-studies/accelya/story-board.jpg" alt="" />
  </section>

  <section class="case-study__video" id="accelya-motion">
    <img class="background--absolute right-0 top-0 minus-z" src="/img/backgrounds/corner-purple.svg" alt="" />
    <div class="case-study__video--macbook">
      <video id="video-accelya-two" class="video-js vjs-big-play-centered vjs-16-9 vjs-show-big-play-button-on-pause" width="100%" height="100%" controls preload="none" poster='/img/projects/accelya/poster-2.jpg' muted>
        <source src="/videos/accelya-two-mobile.mp4"  type='video/mp4' media="all and (max-width:576px)" />
        <source src="/videos/accelya-two.mp4" type='video/mp4' />
        <source src="/videos/accelya-two.webm" type='video/webm' />
      </video>
    </div>
  </section>

  <section class="case-study__block case-study__images background--purple">
    <img src="/img/case-studies/accelya/tablet-left.png" class="image--left front-z" alt="" />
    <img src="/img/case-studies/accelya/tablet-right.png" class="image--right back-z" alt="" />
  </section>


  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Next case study</h4>
      <h5 class="case-study-next__heading">Simón Coll<br/>Website</h5>
      <p class="case-study-next__desc">
      Simón Coll, a well-known Catalan company master chocolatier, wanted to redo its brand website, and at the same time make the leap into e-commerce. The Two Marias were privileged to be their chosen partner, and proud to hold their hand throughout the process.
      </p>
      <a href="/case-studies/simon-coll.php" class="link--cta"><span>Read more</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
      <a href="/case-studies/simon-coll.php" class="link--inner"></a>
      <img src="/img/case-studies/next-case-study-simon-coll.jpg" alt="" />
    </div>
  </section>


</main>

<?php include '../templates/layout/footer.php'; ?>
