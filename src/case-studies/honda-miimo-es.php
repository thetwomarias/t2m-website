<?php
  // Variables
  $bodyClass = 'case-study honda';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Simulador Web <br/>Honda Miimo</h1>
      <img class="page-header__image" src="/img/case-studies/honda/header-honda-miimo.jpg" alt="Honda Miimo Lawn Planner"/>
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industria</h3>
          <h4 class="page-header__specs--desc">Ingeniería Robótica</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Tecnología</h3>
          <h4 class="page-header__specs--desc">HTML, CSS, Javascript, Google Maps API</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Desarrollo</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">UX/UI</li>
            <li class="page-header__specs--item">Diseño</li>
            <li class="page-header__specs--item">Dirección de Arte</li>
            <li class="page-header__specs--item">Desarrollo front-end</li>
            <li class="page-header__specs--item">Integración en honda.com</li>
            <li class="page-header__specs--item">Localización (19 idiomas)</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">Honda Europe nos pidió que desarrolláramos un simulador de cortacésped online para ayudar a potenciales clientes a entender el funcionamiento de la máquina, y así aumentar las ventas.
          <br/><br/>
          Para ello, estudiamos las reglas operativas del robot y las tradujimos a algoritmos que simularan el comportamiento en una visualización online.  Integramos la API de Google Maps para que el cliente pudiera hacer la demo sobre su propio jardín y hacerse una mejor idea. El simulador funciona en 19 idiomas y ojo, ¡que tiene cierto poder hipnótico!
        </div>
        <a href="#" class="link--cta"><span>Ir al website</span></a>
      </div>

    </div>


  </section>

  <section class="case-study__video background--purple">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1052.65 606.99"><defs><style>.cls-1{fill:none;}.cls-2{fill:#f7ddda;}.cls-3{fill:#fff;}.cls-4{clip-path:url(#clip-path);}.cls-5{fill:#d5b9cf;}</style><clipPath id="clip-path"><rect class="cls-1" x="129.14" y="37.7" width="794.37" height="496.99"/></clipPath></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="contenido"><rect class="cls-2" x="100" width="852.64" height="598.33" rx="48" ry="48"/><path class="cls-3" d="M0,571v6.81C0,593.85,12.94,607,28.76,607h995.13c15.82,0,28.76-13.14,28.76-29.19V571Z"/><path class="cls-2" d="M441.5,571v2.45a10.43,10.43,0,0,0,10.33,10.47h149a10.43,10.43,0,0,0,10.33-10.47V571Z"/><g class="cls-4"><image width="1384" height="944" transform="translate(95.46 7.05) scale(0.62)" xlink:href="http://new.thetwomarias.com/img/case-studies/honda/honda-video.jpg"/></g><circle class="cls-5" cx="526.32" cy="17.28" r="5.92"/></g></g></svg>
  </section>

  <div class="case-study__images case-study__images--two">
    <img class="case-study__image--left" src="/img/case-studies/honda/tablet-left.png" alt="Honda Miimo Lawn Planner"/>
    <img class="case-study__image--right" src="/img/case-studies/honda/tablet-right.png" alt="Honda Miimo Lawn Planner"/>
    <img class="background--absolute left-0 top-0 minus-z" style="width:100%" src="/img/backgrounds/corner-purple-inverted.svg" alt=""/>

  </div>

  <section class="case-study__image background--pink">
    <img src="/img/case-studies/honda/honda-miimo-image.jpg" alt="" />
  </section>



  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Próximo Caso</h4>
      <h5 class="case-study-next__heading">Chocolates <br/>Simón Coll</h5>
      <p class="case-study-next__desc">
        Lorem ipsum dolor sit amet, consectetur <br/>adipiscing elit. Cras pulvinar elementum urna, <br/>a ultrices nisl congue in.
      </p>
      <a href="#" class="link--cta"><span>Ver más</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
      <img src="/img/case-studies/catalana-occidente/next-case-study-simon-coll.jpg" alt="" />
    </div>
  </section>

</main>

<?php include '../templates/layout/footer.php'; ?>
