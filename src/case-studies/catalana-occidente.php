<?php
  // Variables
  $bodyClass = 'case-study catalana-occidente';
?>

<?php include '../templates/layout/head.php'; ?>
<?php include '../templates/layout/header.php'; ?>

<main>

  <section class="page-header">

    <div class="scroll-btn scroll fixed appear appear-only"><span>Scroll</span></div>
    <div class="page-header__container">
      <h1 class="page-header__title">Catalana <br/>Occidente <br/>Videos</h1>
        <div class="page-header__video" 
        data-vide-bg="mp4: /videos/catalana-occidente, webm: /videos/catalana-occidente,  ogv: /videos/catalana-occidente, poster: /img/case-studies/catalana-occidente/header-catalana-occidente.png'" 
        data-vide-options="posterType: 'jpg', loop: true, position: '0% 0%', resizing: true">
        </div>
    </div>

    <div class="page-header__container">

      <div class="page-header__specs">

        <div>
          <h3 class="page-header__specs--title">Industry</h3>
          <h4 class="page-header__specs--desc">Insurance</h4>
        </div>

        <div>
          <h3 class="page-header__specs--title">Technology</h3>
          <h4 class="page-header__specs--desc">llustrator, After Effects</h3>
        </div>

        <div>
          <h3 class="page-header__specs--title">Development</h3>
          <ul class="page-header__specs--list">
            <li class="page-header__specs--item">Script</li>
            <li class="page-header__specs--item">Art Direction</li>
            <li class="page-header__specs--item">Storyboarding</li>
            <li class="page-header__specs--item">Illustration</li>
            <li class="page-header__specs--item">Motion graphics</li>
            <li class="page-header__specs--item">Editing</li>
            <li class="page-header__specs--item">Sound recording</li>
            <li class="page-header__specs--item">Sound editing</li>
          </ul>
        </div>

      </div>

      <div class="page-header__brief">
        <div class="brief__desc">We worked with Spanish insurance company Catalana Occidente to promote the launch of their new Connected safety car — a service that turns any car into a connected car. 
          Simply by installing a small, discreet device within the car itself, and then pairing it with a mobile app on the customer’s phone, they’re suddenly afforded all the safety and security that digital technology allows: from being able to geolocate the car, to receiving an alert if the car unexpectedly leaves the area, to automatically calling the emergency services in the event of a crash.
          <br/><br/>For launch, we put together an informative but entertaining animation to highlight the benefits of the service. With a contemporary, social-friendly visual style and a tongue-in-cheek narrative, we followed a day-in-the-life of Roberto — a driver who, to be honest, has some pretty rotten luck, but who manages to come out unscathed, thanks to Catalana Occidente. And then, having hooked people in with some humour, we followed this up with a more educational video that demonstrated the deeper functionality of the mobile app itself.</div>
        <a href="#" class="link--cta"><span>Watch the videos</span></a>
      </div>

    </div>


  </section>

  <section class="background--purple">
    <img src="/img/case-studies/catalana-occidente/characters.svg" alt="" />
  </section>

  <section class="case-study__image background--white">
    <img src="/img/case-studies/catalana-occidente/car-illustration.png" alt="" />
  </section>

  <section class="case-study__block case-study__image background--transparent">
    <img src="/img/case-studies/catalana-occidente/story-board.jpg" alt="" />
  </section>

  <section class="case-study__video background--transparent">
    <img src="/img/case-studies/catalana-occidente/catalana-video.svg" alt="" />
    <img class="background--absolute left-0 bottom-0 minus-z" src="/img/backgrounds/corner-purple.svg" alt=""/>
  </section>

  <section class="case-study__video background--purple">
    <img src="/img/case-studies/catalana-occidente/catalana-tablet.svg" alt="" />
  </section>

  <section class="case-study__spacer background--purple" style="height:200px;">
  </section>


  <section class="background--purple padding-0">
    <div class="case-study__images case-study__images--two">
      <img class="case-study__image--left" src="/img/case-studies/catalana-occidente/car-image-left.svg" alt="Chocolates Simon Coll"/>
      <img class="case-study__image--right" src="/img/case-studies/catalana-occidente/car-image-right.svg" alt="Chocolates Simon Coll"/>
    </div>
  </section>
  

  <section class="case-study-next">
    <div class="case-study-next__text">
      <h4 class="case-study-next__title">Next Case Study</h4>
      <h5 class="case-study-next__heading">Honda Miimo <br/>Lawn Planner</h5>
      <p class="case-study-next__desc">
      Honda Europe asked us to develop an online lawnmower simulator, to help potential customers understand how their new robotic Lawn Planner actually worked, and thus increase consideration and conversion.
      </p>
      <a href="/case-studies/honda-miimo.php" class="link--cta"><span>Read more</span></a>
    </div>
    <div class="case-study-next__image case-study-next__image--boxed">
      <a href="/case-studies/honda-miimo.php" class="link--inner"></a>
      <img src="/img/case-studies/next-case-study-honda-miimo.jpg" alt="" />
    </div>
  </section>

</main>

<?php include '../templates/layout/footer.php'; ?>
